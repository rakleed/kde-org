---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE publie les applications de KDE en version 15.12.2
layout: application
title: KDE publie les applications de KDE en version 15.12.2
version: 15.12.2
---
16 Septembre 2016. Aujourd'hui, KDE a publié la seconde mise à jour de stabilisation des <a href='../15.12.0'>applications 15.12 de KDE</a>. Cette mise à jour ne contient que des corrections de bogues et des mises à jour de traduction, elle sera sûre et appréciable pour tout le monde.

Plus de 30 corrections de bogues apportent des améliorations à kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark and umbrello et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.17 de KDE.
