---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE publica a versión 18.04.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.04.2 das aplicacións de KDE
version: 18.04.2
---
June 7, 2018. Today KDE released the second stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 25 correccións de erros inclúen melloras en, entre outros, Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize e Okular.

Entre as melloras están:

- Agora as operacións en imaxes en Gwenview poden facerse de novo tras desfacerse
- KGpg xa non falla ao descifrar mensaxes sen cabeceira de versión
- Corrixiuse a exportación de follas de cálculo de Cantor a LaTeX para matrices de Maxima
