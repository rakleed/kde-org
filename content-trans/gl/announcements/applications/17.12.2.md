---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE publica a versión 17.12.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.12.2 das aplicacións de KDE
version: 17.12.2
---
February 8, 2018. Today KDE released the second stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Dolphin, Gwenview, KGet e Okular.
