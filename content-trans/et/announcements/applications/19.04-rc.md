---
aliases:
- ../announce-applications-19.04-rc
date: 2019-04-05
description: KDE Ships Applications 19.04 Release Candidate.
layout: application
release: applications-19.03.90
title: KDE toob välja rakenduste 19.04 väljalaskekandidaadi
version_number: 19.03.90
version_text: 19.04 Release Candidate
---
April 5, 2019. Today KDE released the Release Candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/19.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

KDE rakendused 19.04 vajavad põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda meeskonnaga väljalaskekandidaati paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.
