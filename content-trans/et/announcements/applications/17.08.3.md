---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE toob välja KDE rakendused 17.08.3
layout: application
title: KDE toob välja KDE rakendused 17.08.3
version: 17.08.3
---
November 9, 2017. Today KDE released the third stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Tosina teadaoleva veaparanduse hulka kuuluvad Kontacti, Arki, Gwenview, KGpg, KWave'i, Okulari, Spectacle'i ja teiste rakenduste täiustused.

This release also includes the last version of KDE Development Platform 4.14.38.

Täiustused sisaldavad muu hulgas:

- Püüti üle saada Samba 4.7 tagasilangusest parooliga kaitstud SMB jagatud ressursside käitlemisel
- Okulari ei taba enam teatavate pööramistööde korral krahh
- Ark säilitab ZIP-arhiive lahti pakkides failide muutmise kuupäevad
