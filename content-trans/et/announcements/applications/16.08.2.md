---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE toob välja KDE rakendused 16.08.2
layout: application
title: KDE toob välja KDE rakendused 16.08.2
version: 16.08.2
---
October 13, 2016. Today KDE released the second stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 30 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Arki, Dolphini, Kgpg, Kolourpainti, Okulari ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.25.
