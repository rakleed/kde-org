---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE toob välja KDE rakendused 17.08.1
layout: application
title: KDE toob välja KDE rakendused 17.08.1
version: 17.08.1
---
September 7, 2017. Today KDE released the first stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad Kontacti, Gwenview, Kdenlive'i, Konsooli, KDE turvalaeka, Okulari, Umbrello, KDE mängude ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.36.
