---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE Ships Applications 15.08.0.
layout: application
release: applications-15.08.0
title: KDE toob välja KDE rakendused 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphini uus välimus - nüüd KDE Frameworks 5 peal` class="text-center" width="600px" caption=`Dolphini uus välimus - nüüd KDE Frameworks 5 peal`>}}

19. august 2015. KDE laskis täna välja KDE rakendused 15.08

With this release a total of 107 applications have been ported to <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.

With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>the Kontact Suite</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Kontacti komplekti tehniline eelvaade

Over the past several months the KDE PIM team did put a lot of effort into porting Kontact to Qt 5 and KDE Frameworks 5. Additionally the data access performance got improved considerably by an optimized communication layer. The KDE PIM team is working hard on further polishing the Kontact Suite and is looking forward to your feedback. For more and detailed information about what changed in KDE PIM see <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels blog</a>.

### Kdenlive ja Okular

This release of Kdenlive includes lots of fixes in the DVD wizard, along with a large number of bug-fixes and other features which includes the integration of some bigger refactorings. More information about Kdenlive's changes can be seen in its <a href='https://kdenlive.org/discover/15.08.0'>extensive changelog</a>. And Okular now supports Fade transition in the presentation mode.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku tähtmõistatusega` class="text-center" width="600px" caption=`KSudoku tähtmõistatusega`>}}

### Dolphin, õpitarkvara ja mängud

Dolphin was as well ported to KDE Frameworks 5. Marble got improved <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> support as well as better support for annotations, editing and KML overlays.

Arki sissekannete arv oli hämmastavalt suur, nende seas rohkelt pisikesi parandusi. KStarsi sissekannete arv oli samuti suur, sealhulgas täiustati ADU algoritmi, piirväärtuste kontrollimist, gideerimist, HFR-i piirangu automaatset fokuseerimist ning teleskoobi liigutamiskiiruse ja fikseeritud positsioonist vabastamise toetust. KSudoku muutus veel paremaks. Muu hulgas lisati graafiline kasutajaliides Mathdoku ja Killer Sudoku ülesannete sisestamiseks ning uus lahendaja, mille aluseks on Donald Knuthi Dancing Linksi (DLC) algoritm.

### Muud väljalasked

Ühes selle väljalaskega toodi viimast korda välja Plasma 4 pikaajalise toetusega versioon 4.11.22.
