---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### 추가 CMake 모듈

- ecm_add_tests()의 새로운 인자.(버그 345797)

### 프레임워크 통합

- KDirSelectDialog의 올바른 initialDirectory 사용
- 시작 url 값을 수정할 때 올바른 프로토콜 지정 여부 확인
- FileMode::Directory 모드에서 존재하는 디렉터리만 지정할 수 있도록 수정

### KActivities

(변경 기록 없음)

### KAuth

- 모든 KAuth 사용자가 KAUTH_HELPER_INSTALL_ABSOLUTE_DIR 값을 사용할 수 있도록 수정

### KCodecs

- KEmailAddress: extractEmailAddress와 firstEmailAddress에서 오류 메시지를 반환하는 재정의 추가.

### KCompletion

- 파일 대화 상자에서 파일 이름을 편집할 때 원하지 않게 선택되는 문제 수정(버그 344525)

### KConfig

- QWindow::screen()이 null일 때 충돌 수정
- KConfigGui::setSessionConfig() 추가(버그 346768)

### KCoreAddons

- KPluginLoader::findPluginById() 편의 API 추가

### KDeclarative

- KPluginMetdata에서 ConfigModule 생성 추가
- pressAndhold 이벤트 수정

### KDELibs 4 지원

- 임시 파일을 하드코딩하지 않고 QTemporaryFile을 사용합니다.

### KDocTools

- 번역 업데이트
- customization/ru 업데이트
- 잘못된 링크가 있는 항목 수정

### KEmoticons

- 통합 플러그인에서 테마 캐시

### KGlobalAccel

- [runtime] 플랫폼별 코드를 플러그인으로 이동

### KIconThemes

- KIconEngine::availableSizes() 최적화

### KIO

- 사용자를 자동으로 완성하지 않고 접두사가 비어 있지 않을 때 조건을 추가합니다(버그 346920).
- KIO::DndPopupMenuPlugin을 불러올 때 KPluginLoader::factory() 사용
- 네트워크 프록시를 사용할 때 데드록 수정(버그 346214)
- KIO::suggestName 함수에서 파일 확장자를 유지하도록 수정
- sycoca5를 업데이트할 때 kbuildsycoca4를 자동으로 호출합니다.
- KFileWidget: 디렉터리만 선택 모드에서 파일을 받아들이지 않음
- KIO::AccessManager: 순차적 QIODevice를 비동기 처리할 수 있도록 변경

### KNewStuff

- 새 메서드: fillMenuFromGroupingNames
- KMoreTools: 새 그룹 추가
- KMoreToolsMenuFactory: "git-clients-and-actions" 처리
- createMenuFromGroupingNames: url 인자를 선택 사항으로 변경

### KNotification

- 위젯을 설정하지 않았을 때 NotifyByExecute 충돌 수정(버그 348510)
- 알림 닫기 처리 개선(버그 342752)
- QDesktopWidget 사용을 QScreen으로 대체
- 비 GUI 스레드에서도 KNotification을 호출할 수 있도록 변경

### 패키지 프레임워크

- 구조체 QPointer 접근 방지(버그 347231)

### KPeople

- /tmp 하드코딩 대신 QTemporaryFile 사용.

### KPty

- 사용 가능한 경우 tcgetattr 및 tcsetattr 사용

### Kross

- "forms" 및 "kdetranslation" Kross 모듈 불러오기 수정

### KService

- 루트로 실행할 때 기존 캐시 파일의 소유자 보존(버그 342438)
- 스트림을 열 수 없을 때에 대비(버그 342438)
- 파일에 기록할 때 잘못된 권한 검사(버그 342438)
- x-scheme-handler/* 의사 MIME 형식을 처리할 때 ksycoca 질의 수정(버그 347353)

### KTextEditor

- KDE 4.x 시기처럼 제 3자 앱이나 플러그인에서 katepart5/syntax에 구문 강조 XML 파일을 설치하도록 허용
- KTextEditor::Document::searchText() 추가
- KEncodingFileDialog를 다시 사용(버그 343255)

### KTextWidgets

- 데코레이터를 지우는 메서드 추가
- 사용자 정의 Sonnet 데코레이터 사용 허용
- KTextEdit의 "이전 찾기" 기능 개선.
- 텍스트 음성 합성 다시 추가

### KWidgetsAddons

- KAssistantDialog: KDELibs4 버전에 있었던 도움말 단추 다시 추가

### KXMLGUI

- KMainWindow 세션 관리 추가(버그 346768)

### NetworkManagerQt

- NetworkManager 1.2.0+에서 WiMAX 지원 삭제

### Plasma 프레임워크

- 달력 구성 요소에서 주 번호 표시 가능(버그 338195)
- 암호 필드에서 글꼴에 QtRendering 사용
- MIME 형식에서 값을 지정했을 때 AssociatedApplicationManager 찾아보기 수정(버그 340326)
- 패널 배경색 수정(버그 347143)
- "애플릿을 불러올 수 없음" 메시지 삭제
- Plasmoid 설정 창에서 QML KCM 불러오기 기능
- 애플릿에 DataEngineStructure 사용하지 않음
- libplasma를 최대한 sycoca를 사용하지 않도록 이식
- [plasmacomponents] SectionScroller를 ListView.section.criteria를 따르도록 수정
- 터치스크린이 있을 때 스크롤 바를 자동으로 숨기지 않음(버그 347254)

### Sonnet

- SpellerPlugins의 단일 중앙 캐시를 사용합니다.
- 임시 할당을 최소화했습니다.
- 최적화: speller 객체를 복사할 때 dict 캐시를 지우지 않습니다.
- save() 호출을 필요할 때 한 번만 호출하도록 최적화했습니다.

<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot 기사</a>에서 릴리스에 대해 토론할 수 있습니다.
