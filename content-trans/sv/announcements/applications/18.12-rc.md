---
aliases:
- ../announce-applications-18.12-rc
date: 2018-11-30
description: KDE levererar Program 18.12 leveranskandidat.
layout: application
release: applications-18.11.90
title: KDE levererar leveranskandidat av KDE-program 18.12
version_number: 18.11.90
version_text: 18.12 Release Candidate
---
30:e november, 2018. Idag ger KDE ut leveranskandidaten av de nya versionerna av KDE-program. Med beroenden och funktioner frysta, fokuserar KDE-grupperna nu på att rätta fel och ytterligare finputsning.

Titta i <a href='https://community.kde.org/Applications/18.12_Release_Notes'>gemenskapens versionsfakta</a> för information om komprimerade arkiv och kända problem. Ett fullständigare meddelande kommer att vara tillgängligt för den slutliga utgåvan.

Utgåva 18.12 av KDE Program behöver en omfattande utprovning för att behålla och förbättra kvaliteten och användarupplevelsen. Verkliga användare är väsentliga för att upprätthålla hög kvalitet i KDE, eftersom utvecklare helt enkelt inte kan prova varje möjlig konfiguration. Vi räknar med dig för att hjälpa oss hitta fel tidigt, så att de kan krossas innan den slutliga utgåvan. Överväg gärna att gå med i gruppen genom att installera betaversionen och <a href='https://bugs.kde.org/'>rapportera eventuella fel</a>.
