---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE levererar Program 15.12.
layout: application
release: applications-15.12.0
title: KDE levererar KDE-program 15.12.0
version: 15.12.0
---
15:e december, 2015. Idag ger KDE ut KDE-program 15.12.

KDE är glada över att tillkännage utgivningen av KDE-program 15.12, decemberuppdateringen 2015 av KDE-program. Utgåvan innehåller ett nytt program samt funktionstillägg och felrättningar på alla områden av befintliga program. Gruppen strävar efter att ge skrivbordet och dessa program den bästa möjliga kvalitet. Alltså räknar vi med att du skickar oss dina kommentarer.

I den här utgåvan har vi uppdaterat en mängd program att använda det nya<a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Ramverk 5</a>, inklusive <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>Ksystemlog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> och ytterligare KDE-spel, förutom KDE:s gränssnitt för bildinsticksprogram och dess stödbibliotek. Det gör att det totala antalet program som använder KDE Ramverk 5 nu är 126.

### Ett spektakulärt nytt tillägg

Efter 14 år som en del av KDE, har Ksnapshot gått i pension och ersatts av ett nytt skärmbildsprogram, Spectacle.

Med nya funktioner och ett helt nytt användargränssnitt, Spectacle gör det så enkelt och diskret som det kan vara att ta skärmbilder. Förutom vad man kunde göra med Ksnapshot, går det nu att ta sammansatta skärmbilder av menyer tillsammans med deras tillhörande fönster, eller ta skärmbilder av hela skärmen (eller det aktiva fönstret) utan att ens starta Spectacle, genom att helt enkelt använda respektive snabbtangenterna Skift+Print Screen och Meta+Print Screen.

Vi har också gjort omfattande optimering av programstarttiden, för att helt minimera fördröjningen från när man startar programmet till när bilden tas.

### Finputsning överallt

Många program har genomgått omfattande finputsning denna omgången, förutom stabilitetsförbättringar och felrättningar.

Större förbättringar har gjorts av användargränssnittet i <a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, den icke-linjära videoeditorn. Det går nu att kopiera och klistra in objekt i tidslinjen, och enkelt ändra genomskinlighet för ett givet spår. Ikonfärger justeras automatiskt enligt huvudtemat i användargränssnittet, vilket gör dem lättare att se.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark visar nu ZIP-kommentarer`>}}

Arkivhanteraren <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> kan nu visa kommentarer inbäddade i ZIP- och RAR-arkiv. Vi har förbättrat stöd för Unicode-tecken för filnamn i ZIP-arkiv, och det går nu att detektera skadade arkiv och återställa så mycket data som möjligt från dem. Det går också att öppna arkiverade filer i sina standardprogram, och vi har rättat drag och släpp på skrivbordet samt förhandsgranskning av XML-filer.

### Bara lek och inget arbete

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Ksudoku har en ny välkomstskärm (på Mac OS X)`>}}

Utvecklarna av KDE-spel har arbetat hårt under de senaste månaderna för att optimera spelen för en smidigare och rikare upplevelse, och vi har en mängd nyheter på området som användare kan njuta av.

Vi har lagt till två nya nivåuppsättningar i <a href='https://www.kde.org/applications/games/kgoldrunner/'>Kgoldrunner</a>, en som tillåter att man gräver medan man faller och en som inte gör det. Vi har lagt till lösningar för flera befintliga nivåuppsättningar. Som en ytterligare utmaning tillåter vi inte att man gräver medan man faller i några äldre nivåuppsättningar.

Man kan nu skriva ut Mattedoku- och Mördarsudoku-spel i <a href='https://www.kde.org/applications/games/ksudoku/'>Ksudoku</a>. Den nya layouten med flera kolumner på Ksudokus välkomstskärm gör det enklare att se fler av de olika tillgängliga speltyperna, och kolumnerna justeras automatiskt när fönsterstorleken ändras. Vi har gjort samma sak i Palapeli, vilket gör det enklare att se mer av pusselsamlingen på en gång.

Vi har också inkluderat stabilitetsförbättringar av spel som Sänka fartyg, Klickety <a href='https://www.kde.org/applications/games/kshisen/'>Kshisen</a> och <a href='https://www.kde.org/applications/games/ktuberling/'>Ktuberling</a> och totalupplevelsen är mycket förbättrad. Potatismannen, Klickety och Sänka fartyg har också uppdaterats för att använda det nya KDE Ramverk 5.

### Viktiga felrättningar

Hatar du inte när favoritprogram kraschar vid de mest olämpliga tillfällen? Det gör vi också, och för att råda bot på det har vi arbetat mycket hårt med att rätta ett stort antal fel åt dig, men troligtvis har vi ändå låtit några bli kvar, så kom ihåg att <a href='https://bugs.kde.org'>rapportera dem</a>!

Vi har inkluderat diverse stabilitetsförbättringar i vår filhanterare, <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, och några förbättringar för att göra panorering jämnare. Vi har rättat ett besvärligt problem i <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a> med vit text på vit bakgrund. I <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> har vi försökt rätta en krasch som uppstod vid avstängning, förutom att snygga till användargränssnittet och lägga till en ny cachetömning.

<a href='https://userbase.kde.org/Kontact'>Kontact-sviten</a> har fått massor med nya funktioner, stora förbättringar och prestandaoptimeringar. I själva verket har det varit så mycket utveckling i den här omgången att vi har ökat versionsnumret till 5.1. Gruppen arbetar hårt, och ser fram emot alla kommentarer.

### Fortsätta framåt

Som en del av arbetet att modernisera det vi erbjuder, har vi tagit bort några program från KDE-program, och ger inte längre ut dem från och med KDE-program 15.12.

Vi har tagit bort fyra program från utgåvan: Amor, Ktux, Ksnapshot och Superkaramba. Såsom angivits ovan har Ksnapshot ersatts av Spectacle, och Plasma ersätter i huvudsak Superkaramba som grafiskt komponentgränssnitt. Vi har tagit bort fristående skärmsläckare, eftersom skärmlåsning hanteras helt annorlunda i moderna skrivbord.

Vi har också tagit bort tre grafikpaket (kde-base-artwork, kde-wallpapers och kdeartwork), vars innehåll inte hade ändrats sedan länge.

### Fullständig ändringslogg
