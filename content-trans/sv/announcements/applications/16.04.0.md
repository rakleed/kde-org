---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE levererar KDE-program 16.04.0
layout: application
title: KDE levererar KDE-program 16.04.0
version: 16.04.0
---
20:e april, 2016. KDE introducerar idag KDE Program 16.04 med en imponerande samling uppgraderingar när det gäller bättre åtkomst, tillägg av mycket användbara funktioner, och att bli av med de där småproblemen, vilket leder till att KDE Program nu är ett steg närmare att erbjuda dig den perfekta lösningen för ditt system.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>Färgväljaren</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>Kfloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>Kmahjongg</a> och <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> har nu konverterats till KDE Ramverk 5, och vi ser fram emot dina kommentarer och din insikt i de nyaste funktionerna som introduceras i den här utgåvan. Vi uppmuntrar dig också att stödja <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a>, som vi välkomnar till KDE Program, och ge dina synpunkter på vad mer du vill se.

### KDE:s nyaste tillägg

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Ett nytt program har lagts till i KDE:s utbildningssvit. <a href='https://minuet.kde.org'>Minuet</a> är ett musikutbildningsprogram som innehåller fullständigt stöd för MIDI, med kontroll av tempo, tonhöjd och volym, vilket gör det lämpligt för både nybörjare och erfarna musiker.

Minuet innehåller 44 gehörsövningar avseende skalor, ackord, intervall och rytm, samt gör det möjligt att åskådliggöra musikmaterial på ett pianotangentbord och tillåter smidig integrering av egna övningar.

### Mer hjälp för dig

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

Hjälpcentralen, som tidigare distribuerades i Plasma, distribueras nu som en del av KDE Program.

En omfattande felsöknings- och upprensningskampanj utförd av Hjälpcentral-gruppen gav som resultat att 49 fel rättades, varav en stor del var relaterade till att förbättra och återställa den tidigare icke fungerade sökfunktionen.

Den interna dokumentsökningen som förlitade sig på den föråldrade programvaran ht::/dig har ersatts med ett nytt indexerings- och söksystem baserat på Xapian, vilket ger som resultat att funktioner som att söka i manualsidor, infosidor och i dokumentation för KDE-program har återställts, med tillägg av bokmärken för dokumentationssidor.

Underhåll av koden har också blivit ordentligt försett, med borttagning av stöd för KDELIbs4, ytterligare städning av koden samt några andra mindre fel, för att ge dig en Hjälpcentral i nytt glänsande skick.

### Offensiv skadedjursbekämpning

Kontact-sviten har fått hela 55 fel rättade. Vissa av dem hade att göra med problem att ställa in alarm, import av brev från Thunderbird, förminskning av ikoner för Skype och Google talk i Kontact-panelen, samt åtgärder relaterade till Kmail som korgimport, visitkortsimport, öppna ODF-bilagor i brev, infoga webbadresser från Chromium, skillnader i verktygsmenyer när programmet startas som en del av Kontact i motsats till fristående användning, saknat menyalternativ för 'Skicka' och ytterligare några få. Stöd för RSS-kanaler på brasiliansk portugisiska har lagts till, samt rättning av adresser för ungerska och spanska kanaler.

De nya funktionerna omfattar en omkonstruktion av adressbokens kontakteditor, ett nytt standardtema för brevhuvuden i Kmail, förbättringar av inställningsexport och en rättning av stödet för favoritikoner i Akregator. Brevgränssnittet i Kmail har städats upp tillsammans med introduktionen av den nya standardtemat för brevhuvuden i Kmail, med Grantlee-temat använd för sidan 'Om' i Kmail samt i Kontact och Akregator. Akregator använder nu QtWebKit som webbåtergivningsverktyg, ett av de viktigare verktygen för att återge webbsidor och köra Javascript-kod, och processen att implementera stöd för QtWebEngine i Akregator och andra program i Kontact-sviten har redan påbörjats. Medan flera funktioner flyttades till insticksprogram i kdepim-addons, delades pim-biblioteken i ett stort antal olika paket.

### Arkivering på djupet

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, arkivhanteraren, har fått två viktigare fel rättade så att Ark nu varnar användaren om uppackning misslyckas på grund av otillräckligt utrymme i målkatalogen och att den nu inte fyller minnet under uppackning av mycket stora filer via dra och släpp.

Ark inkluderar nu också en egenskapsdialogruta som visar information som arkivtyp, komprimerad och okomprimerad storlek, och MD5/SHA-1/SHA-256 kryptografiska kondensat för arkivet som för närvarande är öppet.

Ark kan nu också öppna och packa upp RAR-arkiv utan att utnyttja den icke-fria verktyget rar, samt kan öppna, packa upp och skapa TAR-arkiv komprimerade med formaten lzop, lzip och lrzip.

Användargränssnittet i Ark har snyggats till genom att organisera om menyraden och verktygsraden så att användbarheten förbättras, och för att ta bort tvetydigheter, samt för att spara vertikalt utrymme tack vare att statusraden nu normalt är dold.

### Större precision i videoredigering

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, den icke-linjära videoeditorn, har fått mängder av nya funktioner implementerade. Titelverktyget har nu en rutnätsfunktion, toningar, tillägg av textskuggor och justering av bokstavs- och radmellanrum.

En integrerad visning av ljudnivåer tillåter enkel övervakning av ljudet i projektet tillsammans med nya överlagrade videomonitorer som visas uppspelningens bildhastighet, säkra zoner och ljudvågformer, och en verktygsrad för att söka till markörer och en zoomningsmonitor.

En ny biblioteksfunktion som tillåter att sekvenser kopieras och klistras in mellan projekt, och en delad visning i tidsraden för att jämföra och åskådliggöra effekter inlagda i klippet med den utan dem har också lagts till.

Återgivningsdialogrutan har skrivits om med ett ytterligare alternativ för att få snabbare kodning, som sålunda skapar stora filer, och hastighetseffekten har gjorts om för att också fungera med ljud.

Kurvor har introducerats för nyckelbildrutor i ett fåtal effekter, och de effekter som oftast väljes kan snabbt kommas åt via den grafiska favoriteffektkomponenten. När skärverktyget används visar den vertikala linjen på tidslinjen den exakta bildram där skäret utförs, och de nytillagda klippgeneratorerna gör det möjligt att skapa färglinjeklipp och räknare. Förutom det har klippanvändningsräkning återintroducerats i projektkorgen och ljudminiatyrerna har också skrivits om för att göra dem mycket snabbare.

De större felrättningarna inkluderar kraschen när titlar används (vilket kräver den senaste versionen av MLT), rättning av korruption som inträffade när projektets bildrutor/s var något annat än 25, kraschen när spår togs bort, det problematiska överskrivningsläget i tidslinjen och korruption eller förlorade effekter när en landsinställning med kommatecken som separator användes. Förutom dessa, har gruppen arbetat konsekvent för att göra stora förbättringar av stabilitet, arbetsflöde och de små användarvänliga funktionerna.

### Med mera

Dokumentvisaren <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> har lagt till nya funktioner i form av anpassningsbar bredd för inlagda kommentarer, möjlighet att direkt öppna inbäddade filer istället för att bara spara dem, och dessutom visning av en innehållsförteckningsmarkör när underliggande länkar är hopdragna.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> har introducerat förbättrat stöd för GnuPG 2.1 med rättning av självtestfel och de besvärliga nyckeluppdateringarna orsakade av den nya katalogstrukturen i GnuPG (GNU Privacy Guard). ECC-nyckelgenerering har lagts till, nu med visning av kurvdetaljer för ECC-certifikat. Kleopatra ges nu ut som ett separat paket, och stöd för .pfx- och .crt-filer har infogats. För att lösa skillnaden i beteende för importerade hemliga nycklar och genererade nycklar, tillåter Kleopatra nu att ägarens pålitlighet kan ställas in till fullständig för importerade hemliga nycklar.
