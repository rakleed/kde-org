---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Lägg till Qt5Network som ett öppet beroende

### BluezQt

- Rätta deklarationskatalog i pri-fil

### Breeze-ikoner

- Lägg till saknade prefixdefinitioner för namnrymd
- Kontrollera att SVG-ikoner är välformade
- Rätta alla ikoner för edit-clear-location-ltr (fel 366519)
- Lägg till stöd för Kwin-effektikon
- Byt namn på caps-on till input-caps-on
- Lägg till caps-ikoner för textinmatning
- Lägg till några Gnome-specifika ikoner från Sadi58
- Lägg till programikoner från gnastyle
- Dolphin-, Konsole- och Umbrello-ikoner optimerade för 16 bildpunkter, 22 bildpunkter och 32 bildpunkter
- Uppdaterade VLC-ikon för 22 bildpunkter, 32 bildpunkter och 48 bildpunkter
- Lägg till programikon för Subtitle Composer
- Rätta Kleopatras nya ikon
- Lägg till programikon för Kleopatra
- Lägg till ikoner för Wine och Wine-qt
- Rätta fel för presentation av ord tack vara Sadi58 (fel 358495)
- Lägg till ikon för system-log-out med 32 bildpunkter
- Lägg till systemikoner med 32 bildpunkter, ta bort färgade systemikoner
- Lägg till stöd för pidgin- och banshee-ikoner
- Ta bort VLC-programikon på grund av licensproblem, lägg till ny VLC-ikon (fel 366490)
- Lägg till stöd för gthumb-ikon
- Använd HighlightedText för katalogikoner
- Ikoner för katalogplatser använder nu stilmall (färgläggningsfärg)

### Extra CMake-moduler

- ecm_process_po_files_as_qm: Hoppa över inexakta översättningar
- Standardnivån för loggkategorier ska vara Info istället för Varning
- Dokumentera variabeln ARGS i create-apk-* targets
- Skapa en test som validerar projektens appstream-information

### KDE Doxygen-verktyg

- Lägg till villkor om gruppens plattform inte är definierad
- Mall: Sortera plattformar alfabetiskt

### KCodecs

- Hämta filen som används för att skapa kentities.c från kdelibs

### KConfig

- Lägg till posten Ge bidrag till KStandardShortcut

### KConfigWidgets

- Lägg till standardåtgärden Ge bidrag

### KDeclarative

- [kpackagelauncherqml] Anta att skrivbordsfilens namn är samma som pluginId
- Läs in QtQuick återgivningsinställningar från en inställningsfil och ställ in förval
- icondialog.cpp - Riktig kompileringsrättning som inte skuggar m_dialog
- Rätta krasch när ingen QApplication är tillgänglig
- Exponera översättningsdomän

### Stöd för KDELibs 4

- Rätta kompileringsfel på Windows i kstyle.h

### KDocTools

- Lägg till sökvägar för inställning, cache + data för general.entities
- Uppdaterad med den engelska versionen
- Lägg till poster för mellanslags- och Meta-tangent i src/customization/en/user.entities

### KFileMetaData

- Kräv bara Xattr om operativsystemet är Linux
- Återställ bygge på Windows

### KIdleTime

- [xsync] XFlush i simulateUserActivity

### KIO

- KPropertiesDialog: Ta bort varning från dokumentationen, felet är borta
- [test program] Lös upp relativa sökvägar genom att använda QUrl::fromUserInput
- KUrlRequester: Rätta felruta när en fil väljes och fildialogrutan öppnas igen
- Tillhandahåll reserv om slavar inte listar posten . (fel 366795)
- Rätta skapa symbolisk länk via "skrivbordsprotokollet"
- KNewFileMenu: Använd KIO::linkAs istället för KIO::link när symboliska länkar skapas
- KFileWidget: Rätta dubbla '/' i sökväg
- KUrlRequester: Använd statisk syntax för connect(), var icke konsistent
- KUrlRequester: Skicka window() som överliggande objekt för QFileDialog
- Undvik att anropa connect(null, .....) från KUrlComboRequester

### KNewStuff

- Packa upp arkiv i underkataloger
- Tillåt inte längre installation i generell datakatalog på grund av potentiellt säkerhetshål

### KNotification

- Hämta egenskapen ProtocolVersion i StatusNotifierWatcher på ett asynkront sätt

### Paketet Framework

- Tysta varningar som avråder från användning av contentHash

### Kross

- Återställ "Ta bort oanvända KF5-beroenden"

### KTextEditor

- Ta bort acceleratorkonflikt (fel 363738)
- Rätta färgläggning av e-postadress i doxygen (fel 363186)
- Detektera några fler json-filer, såsom våra egna projekt
- Förbättra detektering av Mime-typ (fel 357902)
- Fel 363280, färgläggning, C++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (fel 363280)
- Fel 363280, färgläggning, C++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Fel 351496: Python kodvikning fungerar inte under initial inmatning (fel 351496)
- Fel 365171: Python syntaxfärgläggning fungerar inte riktigt för escape-sekvenser (fel 365171)
- Fel 344276: PHP nowdoc kodvikning ej korrekt (fel 344276)
- Fel 359613: Vissa CSS3-egenskaper stöds inte av syntaxfärgläggning (fel 359613)
- Fel 367821: wineHQ-syntax: Sektionen i en reg-fil färgläggs inte korrekt (fel 367821)
- Förbättra hantering av växlingsfil om växlingskatalog angiven
- Rätta krasch när dokument läses in igen med automatisk radbrytning på grund av radlängdsbegränsning (fel 366493)
- Rätta ständiga krascher relaterade till vi-kommandoraden (fel 367786)
- Rätta så att radnummer i utskrivna dokument nu börjar med 1 (fel 366579)
- Säkerhetskopiera fjärrfiler: Hantera också monterade filer som fjärrfiler
- Städa logik för att skapa sökrad
- Lägg till färgläggning för Magma
- Tillåt bara en nivå rekursion
- Rätta felaktig växlingsfil på Windows
- Programfix: Lägg till stöd för bitbake i gränssnittet för syntaxfärgläggning
- autobrace: Titta på attribut för stavningskontroll där tecknet skrevs in (fel 367539)
- Färglägg QMAKE_CFLAGS
- Lämna inte huvudkontext
- Lägg till några namn på körbara filer som ofta används

### KUnitConversion

- Lägg till engelska massenheten "stone"

### Ramverket KWallet

- Flytta kwallet-query docbook till rätt underkatalog
- Rätta ordval an -&gt; one

### Kwayland

- Gör linux/input.h vid kompileringstillfälle valbar

### KWidgetsAddons

- Rätta bakgrund för tecken som inte ingår i BMP
- Lägg till oktal C-undantagen UTF-8 sökning
- Gör att förvald KMessageBoxDontAskAgainMemoryStorage sparar i QSettings

### KXMLGUI

- Konvertera till standardåtgärden Ge bidrag
- Konvertera från authorizeKAction som avråds från

### Plasma ramverk

- Rätta enhetsikon 22 bildpunkters ikonen fungerade inte i den gamla filen
- WindowThumbnail: Gör GL-anrop i rätt tråd (fel 368066)
- Gör så att plasma_install_package fungerar med KDE_INSTALL_DIRS_NO_DEPRECATED
- Lägg till marginal och vaddering i ikonen start.svgz
- Rätta stilmallsgrejer i datorikon
- Lägg till ikoner för dator och bärbar dator i kicker (fel 367816)
- Rätta dubbel varning om att inte kunna tilldela odefinierad i DayDelegate
- Rätta att stylesheed.svgz filer inte gillar mig
- Byt namn på 22 bildpunkters ikoner till 22-22x och 32 bildpunkters ikoner till x i kicker
- [PlasmaComponents TextField] Strunta att läsa in ikoner för oanvända knappar
- Extra kontroll i Containment::corona för specialfallet systembricka
- När en omgivning markeras som borttagen, markera också alla underminiprogram som borttagna: Rättar att omgivningsinställningar i systembrickan inte tas bort
- Rätta ikon för enhetsunderrättelse
- Lägg till systemsökning till system med 32 och 22 bildpunkters storlek
- Lägg till svartvita ikoner för kicker
- Ställ in färgschema för systemsökningsikon
- Flytta systemsökning till system.svgz
- Rätta felaktiga eller saknade "X-KDE-ParentApp" i skrivbordsfildefinitioner
- Rätta dokumentation av programmeringsgränssnitt för Plasma::PluginLoader: Förväxling av applets/dataengine/services/..
- Lägg till systemsökningsikon för SDDM-temat
- Lägg till 32 bildpunkters ikon för nepomuk
- Uppdatera ikonen i systembrickan för tryckplatta
- Ta bort kod som aldrig kan köras
- [ContainmentView] Visa paneler när användargränssnittet är redo
- Deklarera inte om egenskapen implicitHeight
- Använd QQuickViewSharedEngine::setTranslationDomain (fel 361513)
- Lägg till stöd för 22 och 32 bildpunkters Plasma Breeze-ikoner
- Ta bort färgade systemikoner och lägg till 32 bildpunkters svartvita
- Lägg till en valfri knapp för att visa lösenord i textfält
- Standardverktygstipsen speglas nu i ett höger-till-vänster språk
- Prestanda vid byte av månad i kalendern har förbättrats avsevärt

### Sonnet

- Ändra inte språknamnen till små bokstäver vid tolkning av trigram
- Rätta omedelbar krasch vid start på grund av null insticksprogrampekare
- Hantera ordlistor utan riktiga namn
- Ersätt handplockad lista över skriptspråksavbildningar, använd riktiga namn på språk
- Lägg till verktyg för att skapa trigram
- Laga språkdetektering något
- Använd valt språk som förslag vid detektering
- Använd lagrade stavningskontrollverktyg vid språkdetektering, förbättrar prestanda något
- Förbättra språkdetektering
- Filtrera listan över förslag enligt tillgängliga ordlistor, ta bort dubbletter
- Kom ihåg att lägga till den sista trigram-matchningen
- Kontrollera om något trigram faktiskt matchar
- Hantera flera språk med samma poäng i matchning av trigram
- Kontrollera inte minsta storlek två gånger
- Beskär språklista enligt tillgängliga språk
- Använd samma minsta längd överallt i språkdetektering
- Rimlighetskontrollera att den inlästa modellen har rätt antal trigram för varje språk

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
