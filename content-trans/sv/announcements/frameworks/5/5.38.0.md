---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Saknades i förra månades tillkännagivande: KF5 inkluderar ett nytt ramverk, Kirigami, en uppsättning QtQuick-komponenter för att skapa användargränssnitt baserade på KDE UX-riktlinjerna.

### Baloo

- Rätta katalogbaserad sökning

### Extra CMake-moduler

- Ställ in CMAKE_*_OUTPUT_5.38 för att köra tester utan att installera
- Inkludera en modul för att hitta QML-importer som körtidsberoenden

### Integrering med ramverk

- Returnera högupplöst rensningsikon för radeditor
- Rätta att dialogrutor accepteras med Ctrl+Retur när namn på knappar byts

### KActivitiesStats

- Strukturera om frågan som kombinerar länkade och använda resurser
- Läs in modellen igen när resursens länk tas bort
- Rättade frågan när länkade och använda resurser sammanfogas

### KConfig

- Rätta beteckningar för åtgärderna DeleteFile/RenameFile (fel 382450)
- kconfigini: Avlägsna inledande blanktecken vid läsning av postvärden (fel 310674)

### KConfigWidgets

- Avråd från användning av KStandardAction::Help och KStandardAction::SaveOptions
- Rätta beteckningar för åtgärderna DeleteFile/RenameFile (fel 382450)
- Använd "document-close" som ikon för KStandardAction::close

### KCoreAddons

- DesktopFileParser: Lägg till reservuppslagning ":/kservicetypes5/*"
- Lägg till stöd för oinstallerade insticksprogram i kcoreaddons_add_plugin
- desktopfileparser: Rätta tolkning av icke-kompatibla nyckel/värden (fel 310674)

### KDED

- Stöd X-KDE-OnlyShowOnQtPlatforms

### KDocTools

- CMake: Rätta avkortning av målnamn när byggkatalogen har specialtecken (fel 377573)
- Lägg till CC BY-SA 4.0 International och använd som standardvärde

### KGlobalAccel

- KGlobalAccel: Konvertera till nya metoden symXModXToKeyQt i KKeyServer, för att rätta numeriska tangenter (fel 183458)

### KInit

- klauncher: Rätta matchning av appId för flatpak-program

### KIO

- Konvertera IM för webbgenvägar från KServiceTypeTrader till KPluginLoader::findPlugins
- [KFilePropsPlugin] Formatera totalSize enligt landsinställningar under beräkning
- KIO: Rätta långvarig minnesläcka vid avslutning
- Lägg till möjlighet att filtrera Mime-typer i KUrlCompletion
- KIO: Konvertera till URI-filterinsticksprogram från KServiceTypeTrader till json+KPluginMetaData
- [KUrlNavigator] Skicka tabRequested när plats i menyn mittenklickas (fel 304589)
- [KUrlNavigator] Skicka tabRequested när platsväljare mittenklickas (fel 304589)
- [KACLEditWidget] Tillåt dubbelklick för att redigera post
- [kiocore] Rätta logikfel i föregående incheckning
- [kiocore] Kontrollera om klauncher kör eller inte
- Begränsa verkligen takt av meddelandet INF_PROCESSED_SIZE (fel 383843)
- Rensa inte Qt:s SSL certifikatutfärdares certifikatlagring
- [KDesktopPropsPlugin] Skapa målkatalog om den inte finns
- [Fil I/O-slav] Rätta tilldelning av särskilda filegenskaper (fel 365795)
- Ta bort kontroll av upptagensnurra i TransferJobPrivate::slotDataReqFromDevice
- Gör kiod5 till en "agent" på Mac
- Rätta att proxy IM inte läser in manuell proxy riktigt

### Kirigami

- Dölj rullningslister när de är oanvändbara
- Lägg till grundläggande exempel för att justera kolumnbredd med dragbara grepp
- Placering av ider-lager i grepp
- Rätta placering av grepp när de överlappar sista sidan
- Visa inte falska grepp på sista kolumnen
- Lagra inte saker i delegaterna (fel 383741)
- Eftersom vi redan ställer in keyNavigationEnabled, ställ också in radbrytningar
- Bättre vänsterjustering för bakåtknappen (fel 383751)
- Ta inte hänsyn till rubriken två gånger vid rullning (fel 383725)
- Radbryt aldrig rubrikbeteckningarna
- Utför FIXME: ta bort resetTimer (fel 383772)
- Rulla inte bort programrubrik för icke-mobil
- Lägg till en egenskap för att dölja avskiljaren PageRow som matchar AbstractListItem
- Rätta rullning med flödena originY och bottomtotop
- Ta bort varningar om inställning av både bildpunkter och punktstorlekar
- Utlös inte nåbarhetsläge för inverterade vyer
- Ta hänsyn till sidfot
- Lägg till ett något mer komplext exempel på ett chattprogram
- Mer felsäkert sätt att hitta rätt sidfot
- Kontrollera objekts giltighet innan det används
- Ta hänsyn till lagerposition för isCurrentPage
- Använd animation istället för animator (fel 383761)
- Lämna nödvändigt utrymma för sidfoten, om möjligt
- Bättre dämpning för applicationitem-lådor
- Bakgrundsdämpning för applicationitem
- Rätta marginaler för bakåtknapp på ett riktigt sätt
- Riktiga marginaler för bakåtknapp
- Färre varningar i ApplicationHeader
- Använd inte Plasma-skalning för ikonstorlekar
- Nytt utseende för grepp

### KItemViews

### KJobWidgets

- Initiera knapptillstånd för "Paus" i den grafiska komponentföljaren

### KNotification

- Blockera inte tjänst för startunderrättelser (fel 382444)

### Ramverket KPackage

- Strukturera om kpackagetool bort från strängliknande väljare

### Kör program

- Rensa tidigare åtgärder vid uppdatering
- Lägg till fjärrkörprogram via D-Bus

### KTextEditor

- Konvertera Document/View skriptprogrammeringsgränssnitt till lösning baserad på QJSValue
- Visa ikoner i ikonkantens sammanhangsberoende meny
- Ersätt KStandardAction::PasteText med KStandardAction::Paste
- Stöd bråkdelsskalning när förhandsgranskning av sidoraden genereras
- Byt från QtScript till QtQml

### Kwayland

- Behandla RGB-inmatningsbuffertar som förmultiplicerade
- Uppdatera utmatning med SurfaceInterface när en global utmatning förstörs
- KWayland::Client::Surface följ utmatningsdestruktion
- Undvik att skicka dataerbjudanden från en ogiltig källa (fel 383054)

### KWidgetsAddons

- Förenkla setContents genom att låta Qt göra mer av arbetet
- KSqueezedTextLabel: Lägg till isSqueezed() av bekvämlighetsskäl
- KSqueezedTextLabel: Mindre förbättringar av dokumentation av programmeringsgränssnitt
- [KPasswordLineEdit] Ställ in fokusproxy till radeditor (fel 383653)
- [KPasswordDialog] Nollställ geometriegenskap

### KWindowSystem

- KKeyServer: Rätta hantering av KeypadModifier (fel 183458)

### KXMLGUI

- Spara ett antal anrop till stat() vid programstart
- Rätta position för KHelpMenu på Wayland (fel 384193)
- Bli av med felaktig klickhantering med mittenknapp (fel 383162)
- KUndoActions: Använd actionCollection för att ställa in genvägen

### Plasma ramverk

- [ConfigModel] Skydda mot tillägg av null ConfigCategory
- [ConfigModel] Tillåt att ConfigCategory läggs till och tas bort med programkod (fel 372090)
- [EventPluginsManager] Exponera pluginPath i modell
- [Icon Item] Ta inte bor inställning av imagePath i onödan
- [FrameSvg] Använd QPixmap::mask() istället för invecklad väg via alphaChannel() som avråds från
- [FrameSvgItem] Skapa objekten margins och fixedMargins på begäran
- Rätta markerat tillstånd för menyalternativ
- Tvinga Plasma-stil för QQC2 i miniprogram
- Installera katalogen PlasmaComponents.3/private
- Bli av med återstående "locolor" teman
- [Theme] Använd KConfig SimpleConfig
- Undvik några onödiga uppslagningar av temainnehåll
- Ignorera falska storleksändringshändelser för tomma storlekar (fel 382340)

### Syntaxfärgläggning

- Lägg till syntaxdefinition för Adblock Plus filterlistor
- Skriv om syntaxdefinitionen för Sieve
- Lägg till färgläggning för QDoc-inställningsfiler
- Lägg till färgläggningsdefinition för Tiger
- Undanta bindestreck i rest reguljära XML-uttryck (fel 383632)
- Rättning: vanlig text färgläggs som powershell
- Lägg till syntaxfärgläggning för Metamath
- Basera om Less syntaxfärgläggning på SCSS ett (fel 369277)
- Lägg till Pony färgläggning
- Skriv om syntaxdefinitionen för e-post

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
