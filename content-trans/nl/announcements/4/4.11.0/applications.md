---
date: 2013-08-14
hidden: true
title: KDE Applications 4.11 biedt een geweldige stap voorwaarts in Persoonlijk informatiebeheer
  en overal verbeteringen
---
De bestandsbeheerder Dolphin brengt vele kleinere reparaties en optimalisaties in deze uitgave. Laden van grote mappen is versnelt en vereist tot 30&#37; minder geheugen. Zware activiteit van schijf en CPU wordt voorkomen door alleen voorbeelden rond de zichtbare items te laden. Er zijn nog veel meer verbeteringen: bijvoorbeeld, veel bugs in uitvouwen van mappen in detailweergave zijn gerepareerd, geen pictogram voor plaatshouder &quot;onbekend&quot; zullen nog getoond worden bij binnengaan van een map en middenklikken op een archief opent nu een nieuw tabblad met de inhoud van het archief, waarmee een meer consistent gevoel verkregen wordt.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`De nieuwe later verzenden werken in Kontact` width="600px">}}

## Kontact-suite verbeteringen

De Kontact-suite laat weer een significante focus zien op stabiliteit, prestatie en geheugengebruik. Mappen importeren, schakelen tussen mappen, ophalen van e-mail, markeren of verplaatsten van grote aantallen berichten en opstarttijd zijn allen in de laatste 6 maanden verbeterd. Zie <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>deze blog</a> voor details. De <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>archiveringsfunctionaliteit heeft vele reparaties van bugs ondergaan</a> en er zijn ook verbeteringen in de ImportWizard aangebracht, waarmee het importeren van instellingen uit de Trojitá e-mailclient en beter importeren uit verschillende andere toepassingen mogelijk zijn. Meer informatie is <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>hier</a> te vinden.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`De e-mailagent voor archiveren kan e-mailberichten in gecomprimeerde vorm opslaan` width="600px">}}

Deze uitgave komt ook met enige belangrijke nieuwe mogelijkheden. Er is een <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>nieuwe themabewerker voor berichtkoppen</a> en e-mailafbeeldingen kunnen gemakkelijk een andere grootte krijgen. De mogelijkheid <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Later verzenden</a> stelt u in staat om het verzenden van e-mailberichten te plannen op een specifieke datum en tijd, met de toegevoegde mogelijkheid van herhaald verzenden met een gespecificeerd interval. Ondersteuning van KMail Sieve filter (een IMAP-functie die filteren op de server toestaat) is verbeterd, gebruikers kunnen sieve-filtering-scripts maken <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>met een gemakkelijk te gebruiken interface</a>. Op het gebied van beveiliging introduceeert KMail automatische 'scam-detectie', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>met het tonen van een waarschuwing</a> wanneer e-mailberichten typische phishing-tricks bevatten. U ontvangt nu een <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informatieve melding</a> wanneer nieuwe e-mail arriveert. En, tenslotte, komt de Blogilo blogmaker met een zeer verbeterde op QtWebKit gebaseerde HTML-bewerker.

## Uitgebreide ondersteuning van talen door Kate

De geavanceerde tekstbewerker Kate introduceert nieuwe plug-ins: Python (2 en 3), JavaScript & JQuery, Django en XML. Ze introduceren functies als statische en dynamische automatische aanvulling, controle van syntaxis, invoegen van codefragmenten en de mogelijkheid om automatisch XML te in te laten springen met een sneltoets. Maar er is meer voor vrienden van Python: een console voor python die diepgaande informatie levert over een geopend broncodebestand. Enkele kleine verbeteringen aan de UI zijn ook aangebracht, inclusief <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>nieuwe passieve melding voor de zoekfunctie</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimalisaties aan de VIM modus</a> en <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>nieuwe functionaliteit voor het invouwen van tekst</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars toont interessante komende gebeurtenissen zichtbaar vanaf uw locatie` width="600px">}}

## Verbeteringen aan andere programma's

Op het gebied van spellen en educatie zijn verschillende kleinere en grotere nieuwe mogelijkheden en optimalisaties gearriveerd. Aankomende typisten kunnen genieten van de ondersteuning voor rechts-naar-links in KTouch terwijl de vriend van sterrenkijkers, KStars, nu een hulpmiddel hebben dat interessante aankomende gebeurtenissen in uw omgeving toont. Hulpmiddelen voor wiskunde Rocs, Kig, Cantor en KAlgebra kregen aandacht en ondersteunen meer backends en berekeningen. En het spel KJumpingCube heeft nu mogelijkheden voor grotere afmetingen van het bord, nieuwe niveaus van skills, snellere respons en een verbeterd gebruikersinterface.

De eenvoudige toepassing voor tekenen, Kolourpaint, kan overweg met het WebP afbeeldingsformat en de universele documentviewer Okular heeft instelbare hulpmiddeln voor nakijken en introduceert ondersteuning voor ongedaan maken / opnieuw doen in formulieren en annotaties. De audio-tagger/speler JuK ondersteunt afspelen en bewerken van metagegevens van het nieuwe audio-format Ogg Opus (dit vereist echter dat het audiostuurprogramma en TagLib ook Ogg Opus ondersteunen).

#### KDE-toepassingen installeren

KDE software, inclusief al zijn bibliotheken en toepassingen, zijn vrij beschikbaar onder Open-Source licenties. KDE software werkt op verschillende hardware configuraties en CPU architecturen zoals ARM- en x86-besturingssystemen en werkt met elk soort windowmanager of bureaubladomgeving. Naast Linux en andere op UNIX gebaseerde besturingssystemen kunt u Microsoft Windows versies van de meeste KDE-toepassingen vinden op de site <a href='http://windows.kde.org'>KDE-software op Windows</a> en Apple Mac OS X versies op de site <a href='http://mac.kde.org/'>KDE software op Mac</a>. Experimentele bouwsels van KDE-toepassingen voor verschillende mobiele platforms zoals MeeGo, MS Windows Mobile en Symbian zijn te vinden op het web maar worden nu niet onderssteund. <a href='http://plasma-active.org'>Plasma Active</a> is een gebruikservaring voor een breder spectrum van apparaten, zoals tabletcomputers en andere mobiele hardware.

KDE software is verkrijgbaar in broncode en verschillende binaire formaten uit<a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> en kan ook verkregen worden op <a href='/download'>CD-ROM</a> of met elk van de <a href='/distributions'>belangrijke GNU/Linux en UNIX systemen</a> van vandaag.

##### Pakketten

Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van 4.11.0 voor sommige versies van hun distributie te leveren en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. <br />

##### Locaties van pakketten

Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki van de gemeenschap</a>.

De complete broncode voor 4.11.0 kan <a href='/info/4/4.11.0'>vrij gedownload worden</a>. Instructies over compileren en installeren van KDE software 4.11.0 zijn beschikbaar vanaf de <a href='/info/4/4.11.0#binary'>4.11.0 informatiepagina</a>.

#### Systeemvereisten

Om het meeste uit deze uitgaven te halen, bevelen we aan om een recente versie van Qt, zoals 4.8.4, te gebruiken. Dit is noodzakelijk om een stabiele ervaring met hoge prestaties te verzekeren omdat sommige verbeteringen, die zijn gemaakt in KDE software, eigenlijk zijn gedaan in het onderliggende Qt-framework.<br /> Om volledige gebruik te maken van de mogelijkheden van de software van KDE, bevelen we ook aan de laatste grafische stuurprogramma's voor uw systeem te gebruiken, omdat dit de gebruikerservaring aanzienlijk verhoogt, beiden in optionele functionaliteit en in algehele prestaties en stabiliteit.

## Eveneens vandaag geannonceerd:

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 gaat door met verfijnen van gebruikservaring</a>

Versnellen voor onderhoud op lange termijn, levert Plasma Workspaces verdere verbeteringen aan de basis functionaliteit met een soepelere taakbalk, slimmere widget voor de batterij en verbeterde soundmixer. De introductie van KScreen brengt intelligentere behandeling van meerdere monitoren naar de Werkruimten en verbeteringen van de prestaties op grote schaal, gecombineerd met kleine verbeteringen aan bruikbaarheid, maken dat over het geheel de ervaring plezieriger is.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 levert betere prestaties</a>

Deze uitgave van KDE Platform 4.11 gaat door met focus op stabiliteit. Nieuwe functies zullen geïmplementeerd worden in onze toekomstige uitgave KDE Frameworks 5.0, maar voor de stabiele uitgave is het gelukt om optimalisatie samen te ballen in ons Nepomuk framework.
