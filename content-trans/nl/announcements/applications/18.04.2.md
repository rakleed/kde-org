---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE stelt KDE Applicaties 18.04.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.04.2 beschikbaar
version: 18.04.2
---
7 juni 2018. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../18.04.0'>KDE Applicaties 18.04</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 25 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular, naast andere.

Verbeteringen bevatten:

- Bewerkingen aan afbeeldingen in Gwenview kunnen nu opnieuw gedaan worden nadat ze ongedaan zijn gemaakt
- KGpg faalt niet langer om berichten zonder een versie in de kop te ontcijferen
- Exporteren van Cantor werkbladen naar LaTeX is gerepareerd voor Maxima matrices
