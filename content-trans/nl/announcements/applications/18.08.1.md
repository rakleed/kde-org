---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE stelt KDE Applicaties 18.08.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.08.1 beschikbaar
version: 18.08.1
---
6 september 2018. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../18.08.0'>KDE Applicaties 18.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan een dozijn aangegeven reparaties van bugs, die verbeteringen aan Kontact, Cantor, Gwenview, Okular, Umbrello, naast andere.

Verbeteringen bevatten:

- De KIO-MTP component crasht niet langer wanneer het apparaat al in gebruik is door een andere toepassing
- E-mails verzenden in KMail gebruikt nu het wachtwoord wanneer dat gespecificeerd wordt via de wachtwoordprompt
- Okular onthoud nu de modus van de zijbalk na opslaan van PDF documenten
