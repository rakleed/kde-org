---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nieuwe modulen

KHolidays: bibliotheek voor berekenen van vakantiedagen

Deze bibliotheek levert een C++ API die vakantiedagen en andere speciale gebeurtenissen voor een geografisch gebied bepaalt.

Doel: biedt beschikbare acties voor een specifiek doel

Dit framework biedt de mogelijkheid om services en acties aan te maken en te integreren in elke toepassing zonder ze specifiek te moeten implementeren. Het doel is ze mechanismen te bieden om een lijst te maken van de verschillende alternatieven om ze uit te voeren gegeven het gevraagde type actie en zal componenten faciliteren zodat alle plug-ins alle informatie kunnen ontvangen die ze nodig hebben.

### Baloo

- balooctl-status: produceer te ontleden uitvoer
- KIO Slave tagged folder deep copies repareren. Dit breekt het maken van een lijst met van tag voorziene mappen in de tagboomstructuur, maar het is beter dan gebroken kopieën
- Sla het in de wachtrij zetten van nieuwe niet te indexeren bestanden en verwijder ze direct uit de index
- Verwijder niet te indexeren verplaatste bestanden uit de index

### Breeze pictogrammen

- Ontbrekende pictogrammen van Krusader toevoegen voor synchronisatie van mappen (bug 384473)
- Pictogram lijst-verwijderen bijwerken met - in plaats van pictogram annuleren (bug 382650)
- pictogrammen voor pulsaudio-plasmoid toegevoegd (bug 357334)
- overal dezelfde dekking 0,5 gebruiken
- Nieuw pictogram voor virtualbox (bug 384357)
- weer-mist dag/nacht neutraal maken (bug 388865)
- installeer echt de nieuwe context voor animaties
- QML-bestand-mime ziet er nu hetzelfde uit in alle groottes (bug 376757)
- Animatiepictogrammen bijwerken (bug 368833)
- embleem gedeeld gekleurd pictogram toevoegen
- Gebroken index.theme bestanden repareren, "Context=Status" ontbrak in status/64
- Executierechten verwijderen uit .svg-bestanden
- Download van actiepictogram is gekoppeld aan download bewerken (bug 382935)
- Dropbox systeemvakpictogramthema bijwerken (bug 383477)
- Ontbrekende emblem-default-symbolic (bug 382234)
- Type in MIME-type bestandsnaam (bug 386144)
- Een meer specifieke octave-logo gebruiken (bug 385048)
- kluispictogrammen toevoegen (bug 386587)
- schaal px-statuspictogrammen (bug 386895)

### Extra CMake-modules

- FindQtWaylandScanner.cmake: qmake-query voor HINT gebruiken
- Ga na dat gezocht wordt naar Qt5-based qmlplugindump
- ECMToolchainAndroidTest bestaat niet meer (bug 389519)
- De parameter LD_LIBRARY_PATH niet in prefix.sh instellen
- FindSeccomp aan zoekmodulen toevoegen
- Terugvallen naar de taalnaam voor opzoeken van vertalingen als locale-naam mislukt
- Android: meer includes toevoegen

### KAuth

- Regressie met koppeling geïntroduceerd in 5.42 repareren.

### KCMUtils

- Voegt tekstballonnen toe aan de twee knoppen bij elk item

### KCompletion

- Onjuiste emissie van textEdited() door KLineEdit repareren (bug 373004)

### KConfig

- Ctrl+Shift+ gebruiken, als de standaard sneltoets voor "&lt;Program&gt; configureren"

### KCoreAddons

- Laat ook spdx toetsen LGPL-2.1 &amp; LGPL-2.1+ overeenkomen
- De veel snellere urls() methode gebruiken uit QMimeData (bug 342056)
- inotify KDirWatch backend optimaliseren: koppel inotify wd aan Entry
- Optimize: QMetaObject::invokeMethod gebruiken met functie

### KDeclarative

- [ConfigModule] QML context en engine opnieuw gebruiken indien aanwezig (bug 388766)
- [ConfigPropertyMap] ontbrekende include toevoegen
- [ConfigPropertyMap] geen valueChanged uitsturen op initiële creatie

### KDED

- kded5 niet exporteren als een CMake-doel

### Ondersteuning van KDELibs 4

- Maak Solid::NetworkingPrivate opnieuw om een gedeelde en platform specifieke implementatie te krijgen
- mingw compileerfout "src/kdeui/kapplication_win.cpp:212:22: repareren fout: 'kill' was niet gedeclareerd in deze scope"
- kded dbus-naam in solid-networking howto repareren

### KDesignerPlugin

- Afhankelijkheid van doctools optioneel maken

### KDESU

- Zorg dat KDESU_USE_SUDO_DEFAULT modus opnieuw bouwt
- Zorg dat kdesu werkt wanneer PWD is /usr/bin

### KGlobalAccel

- cmake-functie gebruiken in 'kdbusaddons_generate_dbus_service_file' uit kdbusaddons om dbus-service-bestand te genereren (bug 382460)

### KDE GUI-addons

- Koppeling maken van aangemaakte QCH-bestand in QtGui docs repareren

### KI18n

- Zoeken naar libintl bij "cross"-compiling native Yocto pakketten repareren

### KInit

- Cross-compileren met MinGW (MXE) repareren

### KIO

- Kopiëren van bestand naar VFAT zonder waarschuwingen repareren
- kio_file: foutbehandeling voor initiële perms gedurende kopiëren van bestand overslaan
- kio_ftp: stuur geen foutsignaal voordat we alle commando's voor een lijst hebben geprobeerd (bug 387634)
- Prestaties: het bestemmingsobject van KFileItem gebruiken om uit te zoeken of het is te schrijven in plaats van een KFileItemListProperties aan te maken
- Prestaties: de KFileItemListProperties kopieerconstructor gebruiken in plaats van de conversie van KFileItemList naar KFileItemListProperties. dit bespaart alle items opnieuw te bekijken
- Foutbehandeling in bestand ioslave verbeteren
- PrivilegeExecution job-vlag verwijderen
- KRun: uitvoeren "netwerkmap toevoegen" toestaan zonder vraag om bevestiging
- Filteren van plaatsen toestaan gebaseerd op alternatieve toepassingsnaam
- [Uri Filter Search Provider] dubbel verwijderen vermijden (bug 388983)
- Overlap van het eerste item in KFilePlacesView repareren
- Schakel de ondersteuning voor KAuth in KIO tijdelijk uit
- previewtest: de ingeschakelde plug-ins specificeren toestaan
- [KFileItem] "emblem-shared" voor gedeelde bestanden gebruiken
- [DropJob] slepen en loslaten in een alleen-lezen map inschakelen
- [FileUndoManager] wijzigingen ongedaan maken inschakelen in alleen-lezen mappen
- Ondersteuning toevoegen voor uitvoeren met privileges in KIO-jobs (tijdelijk uitgeschakeld in deze uitgave)
- Ondersteuning toevoegen voor delen van bestandsdescriptor tussen file-KIO-slave en zijn KAuth-helper
- KFilePreviewGenerator::LayoutBlocker repareren (bug 352776)
- KonqPopupMenu/Plugin kan nu de X-KDE-RequiredNumberOfUrls-sleutel gebruiken om een bepaald aantal bestanden te verkrijgen om geselecteerd te worden alvorens ze te tonen
- [KPropertiesDialog] woordafbreking inschakelen voor beschrijving controlesom
- cmake-functie gebruiken in 'kdbusaddons_generate_dbus_service_file' uit kdbusaddons om dbus-service-bestand te genereren (bug 388063)

### Kirigami

- ondersteuning voor ColorGroups
- geen terugkoppeling met een klik als het item geen muisgebeurtenissen wil
- omweg voor apps die listitems onjuist gebruiken
- ruimte voor de schuifbalk (bug 389602)
- Een tekstballon bieden voor de hoofdactie
- cmake: de officiële CMake variabele gebruiken voor bouwen als een statische plug-in
- Leesbaar voor de mens van tier aanduiding in API dox
- [ScrollView] Schuif één pagina met Shift+wieltje
- [PageRow] navigeer tussen niveaus met muisknoppen terug/verder
- Maak zeker dat DesktopIcon wordt weergegeven met de juiste beeldverhouding (bug 388737)

### KItemModels

- KRearrangeColumnsProxyModel: crash repareren wanneer er geen bronmodel is
- KRearrangeColumnsProxyModel: sibling() opnieuw implementeren zodat het werkt zoals verwacht

### KJobWidgets

- Duplicatie van code in byteSize(dubbele grootte) verwijderd (bug 384561)

### KJS

- Afhankelijkheid van doctools optioneel maken

### KJSEmbed

- kjscmd niet exporteren
- Afhankelijkheid van doctools optioneel maken

### KNotification

- De melding van de actie "Commando uitvoeren" is gerepareerd (bug 389284)

### KTextEditor

- Repareren: weergave springt bij schuiven verder dan het eind van het document is ingeschakeld (bug 306745)
- Gebruik minstens de gevraagde breedte voor het argument hint-boomstructuur
- ExpandingWidgetModel: zoek de meest rechtse kolom gebaseerd op locatie

### KWidgetsAddons

- KDateComboBox: dateChanged() die niets uitzendt na typen van een datum repareren (bug 364200)
- KMultiTabBar: regressie in conversie naar nieuwe stijl connect() repareren

### Plasma Framework

- Eigenschap in Units.qml definiëren voor de Plasma stijlen
- windowthumbnail: de GLXFBConfig selectiecode repareren
- [Default Tooltip] grootte aanpassen repareren (bug 389371)
- [Plasma Dialog] Roep venstereffects alleen aan indien zichtbaar
- Eén bron van logspam gerefereerd in Bug 388389 repareren (lege bestandsnaam doorgegeven aan functie)
- [Calendar] Pas de agenda werkbalkankers aan
- [ConfigModel] Stel QML-context in op ConfigModule (bug 388766)
- [Icon Item] Behandel broncode beginnend met een slash als lokaal bestand
- RnL uiterlijk voor keuzelijst repareren (bug 387558)

### QQC2StyleBridge

- BusyIndicator toevoegen aan de gestijlde besturingslijst
- flikkering verwijderen bij zweven boven de schuifbalk

### Solid

- [UDisks] Negeer alleen non-user achterliggend bestand als het bekend is (bug 389358)
- Opslag apparaten aangekoppeld buiten /media, /run/media en $HOME worden nu genegeerd, evenals loop-apparaten (bug 319998)
- [UDisks-apparaat] loop-apparaat tonen met hun onderliggende bestandsnaam en pictogram

### Sonnet

- Aspell woordenboeken op Windows zoeken

### Accentuering van syntaxis

- C# var regex repareren
- Ondersteuning voor underscores in numerieke constanten (Python 3.6) (bug 385422)
- Khronos Collada en glTF bestanden accentueren
- Ini-accentuering van waarden die ; of # tekens bevatten repareren
- AppArmor: nieuwe trefwoorden, verbeteringen &amp; reparaties

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
