---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Limiet/offset behandeling in SearchStore::exec repareren
- Opnieuw de baloo-index aanmaken
- balooctl config: opties toevoegen om onlyBasicIndexing in te stellen/te bekijken
- balooctl controle overzetten om te werken met de nieuwe architectuur (bug 353011)
- FileContentIndexer: repareer dubbel emitterende filePath
- UnindexedFileIterator: mtime is quint32 geen quint64
- Transactie: nog een Dbi-typo repareren
- Transactie: documentMTime() en documentCTime() met verkeerde Dbis repareren.
- Transaction::checkPostingDbInTermsDb: code optimaliseren
- Dbus-waarschuwingen repareren
- Balooctl: checkDb commando toevoegen
- balooctl config: "exclude filter" toevoegen
- KF5Baloo: ga na dat D-Bus interfaces gegenereerd zijn voordat ze worden gebruikt. (bug 353308)
- Gebruik van QByteArray::fromRawData vermijden
- baloo-monitor uit baloo verwijderen
- TagListJob: Geeft foutmelding bij mislukken van het openen van de database
- Geen subterms negeren indien niet gevonden
- Nettere code bij mislukken van Baloo::File::load() bij mislukken van DB openen.
- Laat balooctl IndexerConfig gebruiken in plaats van baloofilerc direct te manipuleren
- i18n voor balooshow verbeteren
- Laat balooshow netjes mislukken als de database niet geopend kan worden.
- Laat Baloo::File::load() mislukken als de database niet geopend kan worden. (bug 353049)
- IndexerConfig: refresh() methode toevoegen
- inotify: geen closedWrite gebeurtenis simuleren na verplaatsing zonder cookie
- ExtractorProcess: de extra / verwijderen aan het eind van het filePath
- baloo_file_extractor: roep QProcess::close aan voor vernietiging van het QProcess
- baloomonitorplugin/balooctl: i18nize indexerstatus.
- BalooCtl: en 'config' optie toevoegen
- Maak baloosearch meer presentabel
- Lege EventMonitor-bestanden verwijderen
- BalooShow: toon meer informatie wanneer de id's niet overeenkomen
- BalooShow: wanneer aangeroepen met een id controle als de id correct is
- Een FileInfo klasse toevoegen
- Foutcontrole toevoegen op verschillende plaatsen zodat Baloo niet crasht indien uitgeschakeld. (bug 352454)
- Baloo repareren bij het niet respecteren van "basic indexing only" als configuratie-optie
- Monitor: resterende tijd ophalen bij opstarten
- Actuale methodeaanroepen in MainAdaptor in plaats van QMetaObject::invokeMethod
- org.kde.baloo-interface toevoegen aan root-object voor achterwaartse compatibiliteit
- Datumtekst repareren getoond in de adresbalk vanwege overzetten naar QDate
- Vertraging toevoegen na elk bestand in plaats van elke batch
- Qt::Widgets afhankelijkheid van baloo_file verwijderen
- Ongebruikte code uit baloo_file_extractor verwijderen
- Baloo-monitor toevoegen of experimentele qml-plug-in
- Maak "querying for remaining time" veilig voor thread's
- kioslaves: ontbrekend overschrijven voor virtuele functies toegevoegd
- Extractor: de applicationData instellen na constructie van de app
- Query: ondersteuning voor 'offset' implementeren
- Balooctl: --version en --help toevoegen (bug 351645)
- Ondersteuning van KAuth verwijderen om max inotify watches te vergroten als telling te laag is (bug 351602)

### BluezQt

- Crash van fakebluez repareren in obexmanagertest met ASAN
- Voorwaarts declareren van alle geëxporteerde klassen in types.h
- ObexTransfer: fout instellen wanneer transfersessie is verwijderd
- Utils: pointers behouden naar exemplaren van managers
- ObexTransfer: fout instellen wanneer org.bluez.obex crasht

### Extra CMake-modules

- GTK pictogramcache bijwerken bij installeren van pictogrammen.
- Workaround verwijderen om executie te vertragen op Android
- ECMEnableSanitizers: de ongedefinieerde sanitizer wordt ondersteund door gcc 4.9
- Detectie van X11,XCB etc. uitschakelen op OS X
- Zoek naar de bestanden in de geïnstalleerde prefix in plaats van het prefixpad
- Qt5 gebruiken om te specificeren wat de installatieprefix is van Qt5
- Definitie van ANDROID toevoegen zoals nodig is in qsystemdetection.h.

### Frameworkintegratie

- Probleem met niet tonen van willekeurige bestandsdialoog repareren. (bug 350758)

### KActivities

- Een eigen functie voor overeenkomst gebruiken in plaats van glob van sqlite. (bug 352574)
- Probleem met toevoegen van een nieuwe hulpbron aan het model gerepareerd

### KCodecs

- Crash in UnicodeGroupProber::HandleData gerepareerd met korte tekenreeksen

### KConfig

- kconfig-compiler markeren als niet-gui hulpmiddel

### KCoreAddons

- KShell::splitArgs: alleen ASCII spatie is een scheidingsteken, niet unicode spatie U+3000 (bug 345140)
- KDirWatch: crash wanneer een globale statische destructor KDirWatch::self() gebruikt repareren (bug 353080)
- Crash wanneer KDirWatch wordt gebruikt in Q_GLOBAL_STATIC repareren.
- KDirWatch: thread-beveiliging repareren
- Hoe KAboutData constructor argumenten in te stellen uitleggen.

### KCrash

- KCrash: cwd aan kdeinit overdragen bij auto-herstart van de app via kdeinit. (bug 337760)
- KCrash::initialize() toevoegen zodat apps en de platform-plug-in expliciet KCrash inschakelen.
- ASAN uitschakelen indien ingeschakeld

### KDeclarative

- Kleine verbeteringen in ColumnProxyModel
- Het mogelijk maken voor applicaties om het pad naar homeDir te kennen
- EventForge verplaatsen uit de bureaubladcontainer
- Ingeschakelde eigenschap voor QIconItem leveren.

### KDED

- kded: logica rond sycoca vereenvoudigen; gewoon ensureCacheValid aanroepen.

### Ondersteuning van KDELibs 4

- newInstance aanroepen vanuit de dochter bij eerste keer uitvoeren
- kdewin definieert gebruiken.
- X11 niet proberen te zoeken op WIN32
- cmake: taglib versiecontrole in FindTaglib.cmake repareren.

### KDesignerPlugin

- Qt moc kan geen macro's behandelen (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- windows usermetadata implementeren

### KDE GUI-addons

- Niet naar X11/XCB zoeken is zinvol ook voor WIN32

### KHTML

- std::auto_ptr vervangen door std::unique_ptr
- khtml-filter: rules met speciale adblock-functies die we nog niet behandelen verwerpen.
- khtml-filter: broncode herordenen, geen functionele wijzigingen.
- khtml-filter: regexp met opties negeren omdat we ze niet ondersteunen.
- khtml-filter: detectie van scheidingsteken in adblock-opties repareren.
- khtml-filter: opschonen vanaf witruimte aan het eind van de regel.
- khtml-filter: Geen regels verwerpen die beginnen met '&amp;' omdat het geen speciaal adblock-teken is.

### KI18n

- strikte iterators verwijderen voor msvc om ki18n te laten bouwen

### KIO

- KFileWidget: argument van ouder moet standaard 0 zijn zoals in alle widgets.
- Ga na dat de grootte van het byte-array dat we zojuist hebben gedumpt in de struct groot genoeg is alvorens de targetInfo te berekenen, anders gebruiken we geheugen dat van ons is
- Gebruik van Qurl repareren bij aanroepen van QFileDialog::getExistingDirectory()
- Lijst met apparaten van Solid verversen alvorens kio_trash te zoeken
- Trash toestaan: naast trash:/ als url voor listDir (roept listRoot aan) (bug 353181)
- KProtocolManager: deadlock bij gebruikt van EnvVarProxy gerepareerd. (bug 350890)
- X11 niet proberen te zoeken op WIN32
- KBuildSycocaProgressDialog: ingebouwde bezigindicator van Qt gebruiken. (bug 158672)
- KBuildSycocaProgressDialog: kbuildsycoca5 uitvoeren met QProcess.
- KPropertiesDialog: reparatie voor ~/.local als een symbolische koppeling, vergelijk canonieke paden
- Ondersteuning voor netwerk-shares in kio_trash toevoegen (bug 177023)
- Verbinden met de signalen van QDialogButtonBox, niet van QDialog (bug 352770)
- Cookies KCM: DBus-namen bijwerken voor kded5
- JSON-bestanden direct gebruiken in plaats van kcoreaddons_desktop_to_json()

### KNotification

- Geen dubbele melding verzenden van bijwerksignaal
- Configuratie voor notificatie repareren alleen opnieuw ontleden wanneer deze is gewijzigd
- X11 niet proberen te zoeken op WIN32

### KNotifyConfig

- Methode voor laden van standaarden wijzigen
- De appnaam sturen waarvan de config is bijgewerkt samen met het DBus-signaal
- Methode toevoegen om kconfigwidget terug te zetten naar de standaarden
- De config niet n-keer synchroniseren bij opslaan

### KService

- De grootste tijdaanduiding gebruiken in submap als tijdaanduiding van hulpbronmap.
- KSycoca: mtime opslaan voor elke bronmap, om wijzigingen te detecteren. (bug 353036)
- KServiceTypeProfile: onnodige factorycreatie verwijderen. (bug 353360)
- KServiceTest::initTestCase vereenvoudigen en versnellen.
- maak installatienaam van bestand toepassing.menu een gecachte cmake-variabele
- KSycoca: ensureCacheValid() zou de db moeten aanmaken als deze niet bestaat
- KSycoca: laat globale database werken na de recente checkcode op tijdsaanduiding
- KSycoca: DB-bestandsnaam wijzigen om taal en sha1 van de mappen waaruit het is gebouwd in te voegen.
- KSycoca: maak ensureCacheValid() onderdeel van de publieke API.
- KSycoca: een q-pointer toevoegen om meer singleton-gebruik te verwijderen
- KSycoca: alle self() methoden voor factories verwijderen, sla ze in plaats daarvan op in KSycoca.
- KBuildSycoca: schrijven het bestand ksycoca5stamp verwijderen.
- KBuildSycoca: qCWarning gebruiken in plaats van fprintf(stderr, ...) of qWarning
- KSycoca: ksycoca opnieuw bouwen in proces in plaats van kbuildsycoca5 uit te voeren
- KSycoca: alles van de code van kbuildsycoca verplaatsen naar de lib, behalve voor main().
- Optimalisatie van KSycoca: bewaak het bestand als de app verbinding maakt met databaseChanged()
- Geheugenlekken in de klasse KBuildSycoca repareren
- KSycoca: DBus-notificatie vervangen door bewaken van bestande met KDirWatch.
- kbuildsycoca: maak optie --nosignal verouderd.
- KBuildSycoca: op dbus gebaseerde vergrendeling door een lock-bestand vervangen.
- Niet crashen bij ontdekken van ongeldige plug-in-informatie.
- Headers hernoemen naar _p.h in voorbereiding voor verplaatsen naar kservice-bibliotheek.
- Verplaats checkGlobalHeader() binnen KBuildSycoca::recreate().
- Code voor --checkstamps en --nocheckfiles verwijderen.

### KTextEditor

- meer regexp valideren
- regexps in HL bestanden repareren (bug 352662)
- ocaml HL synchroniseren met status van https://code.google.com/p/vincent-hugot-projects/ voordat google-code down is, enige kleine reparaties van bugs
- woord afbreken toevoegen (bug 352258)
- Regel valideren voordat zaken met invouwen worden aangeroepen (bug 339894)
- Probelemen met aantal woorden in Kate repareren door te luisteren naar DocumentPrivate in plaats van Document (bug 353258)
- Kconfig-syntaxisaccentuering bijwerken: nieuwe operators uit Linux 4.2 toevoegen
- sync w/ KDE/4.14 kate branch
- minimap: scrollbar-handle niet getekend met scrollmarks off repareren. (bug 352641)
- syntax: git-user-optie toevoegen voor kdesrc-buildrc

### KWallet Framework

- Niet langer automatisch sluiten bij laatst gebruik

### KWidgetsAddons

- Waarschuwing C4138 (MSVC) repareren: '*/' gevonden buiten commentaar

### KWindowSystem

- Doe een diepe kopie van QByteArray get_stringlist_reply
- Sta interactie met meerdere X-servers toe in de NETWM klassen.
- [xcb] Overweeg mods in KKeyServer zoals geïnitialiseerd op platform != x11
- KKeyserver (x11) wijzigen om logging te categoriseren

### KXMLGUI

- Maak het mogelijk om sneltoetsschema's symmetrisch te im-/exporteren

### NetworkManagerQt

- Introspecties repareren, LastSeen zou in AccessPoint moeten zitten en niet in ActiveConnection

### Plasma Framework

- Verberg tekstballondialoog wanneer de cursor het inactieve ToolTipArea ingaat
- als het desktop-bestand Icon=/foo.svgz heeft gebruik dat bestand uit het pakket
- voeg een bestandstype "schermafdruk" toe in pakketten
- overweeg devicepixelration in standalone schuifbalk
- geen hover-effect bij touchscreen+mobile
- lineedit svg marges in sizeHint berekeningen gebruiken
- Laat animatie-pictogram in plasma tekstballonnen niet uitvagen
- Repareer afgekapte tekst op knop
- Contextmenu's van applets binnen een paneel overlappen niet langer het applet
- Vereenvoudig ophalen van lijst met geassocieerde apps in AssociatedApplicationManager

### Sonnet

- hunspell-plug-in-ID voor juist laden repareren
- statische compilatie op windows ondersteunen, voeg pad voor windows libreoffice hunspell dict toe
- Neem niet aan dat er UTF-8 gecodeerde Hunspell woordenboeken zijn. (bug 353133)
- Highlighter::setCurrentLanguage() repareren voor het geval dat de eerdere taal ongeldig was (bug 349151)
- /usr/share/hunspell ondesteunen als dict-locatie
- Op NSSpellChecker gebaseerde plug-in

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
