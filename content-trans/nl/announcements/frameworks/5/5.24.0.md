---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Algemene wijzigingen

- De lijst met ondersteunde platforms voor elk framework is nu meer expliciet. Android is toegevoegd aan de lijst met ondersteunde platforms in alle frameworks waar dat het geval is.

### Baloo

- DocumentUrlDB::del Ken alleen toe wanneer kinderen van dir echt bestaan
- Slecht geformuleerde Queries negeren die een binaire operator hebben zonder eerste argument

### Breeze pictogrammen

- Veel nieuwe en verbeterde pictogrammen
- bug 364931 repareren pictogram voor niet actieve gebruiker was niet zichtbaar (bug 364931)
- Een programma toevoegen om symbolisch gekoppelde bestanden te converteren naar qrc-aliases

### Extra CMake-modules

- Relatieve bibliotheekpaden aan APK integreren
- "${BIN_INSTALL_DIR}/data" voor DATAROOTDIR onder Windows gebruiken

### KArchive

- Ga na dat uitpakken van een archief, om reden van beveiliging, geen bestanden installeert buiten de uitpakmap. Pak zulke bestanden uit naar de basismap van de uitpakmap.

### KBookmarks

- KBookmarkManagerList opschonen voordat qApp eindigt, om dodelijke omarming met de DBus-thread te vermijden

### KConfig

- Maak authorizeKAction() verouderd ten faveure van authorizeAction()
- Reproduceerbaarheid in bouwen repareren door na te gaan dat utf-8 codering wordt gebruikt

### KConfigWidgets

- KStandardAction::showStatusbar: geef de bedoelde actie terug

### KDeclarative

- Maak epoxy optioneel

### KDED

- [OS X] kded5 een agent maken en het bouwen als een reguliere toepassing

### Ondersteuning van KDELibs 4

- KDETranslator klasse verwijderen, er is geen kdeqt.po meer
- Documenteer de vervanging voor use12Clock()

### KDesignerPlugin

- Toevoegen van ondersteuning voor KNewPasswordWidget

### KDocTools

- KDocTools toestaan om altijd minstens zijn eigen geïnstalleerde zaken te lokaliseren
- CMAKE_INSTALL_DATAROOTDIR gebruiken om naar docbook te zoeken in plaats van share
- qt5options manpagina-docbook bijwerken naar qt 5.4
- kf5options manpagina-docbook bijwerken

### KEmoticons

- glass-thema verplaatsen naar kde-look

### KGlobalAccel

- QGuiApplication gebruiken in plaats van QApplication

### KHTML

- Geërfde waarde voor outline shorthand eigenschap toepassen repareren
- Behandel initiële en geërfde voor randstraal
- Laat eigenschap weg als we een ongeldige lengte|percentage als achtergrond-grootte krijgen
- cssText moet komma-gescheiden waarden voor deze eigenschappen uitvoeren
- Ontleden van background-clip in verkleining repareren
- Ontleden van background-grootte in verkleining repareren
- Markeer eigenschappen zoals ingesteld bij herhalende patronen
- Geërfde achtergrondeigenschappen repareren
- Initiële en geërfde waarde voor eigenschap achtergrond-grootte repareren
- Pas het khtml kxmlgui bestand toe in een Qt hulpbronbestand

### KI18n

- Doorzoek catalogi ook voor gestripte varianten van waarden in env-var LANGUAGE
- Ontleden van env-var waarden van WRT-modifier en codeset, gedaan in verkeerde volgorde, repareren

### KIconThemes

- Ondersteuning voor het automatisch laden en gebruiken van een pictogramthema in een RCC-bestand
- Documenteer toepassing van toepassen van pictogramthema onder MacOS en Windows, zie https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Sta timeout toe in reset_oom_protection bij wachten op SIGUSR1

### KIO

- KIO: SlaveBase::openPasswordDialogV2 toevoegen voor betere foutcontrole, maak uw kioslaves er voor geschikt
- KUrlRequester die de bestandsdialoog opent in de verkeerde map (bug 364719)
- Onveilige KDirModelDirNode* casts repareren
- Optie KIO_FORK_SLAVES toevoegen aan cmake om standaard waarde in te stellen
- ShortUri-filter: filteren van mailto:user@host repareren
- OpenFileManagerWindowJob toevoegen aan bestand accentueren binnen een map
- KRun: runApplication-methode toevoegen
- Leverancier van zoekmachine soundcloud toevoegen
- Een probleem met uitlijnen repareren met de eigen OS X "macintosh"-stijl

### KItemModels

- KExtraColumnsProxyModel::removeExtraColumn toevoegen, is nodig voor StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake - HAVE_SYS_PARAM_H op de juiste manier instellen

### KNewStuff

- Ga na dat we een afmeting hebben te bieden (bug 346188)
- "Dialoog voor downloaden mislukt wanneer alle categorieën ontbreken"

### KNotification

- Melden via de taakbalk repareren

### KNotifyConfig

- KNotifyConfigWidget: disableAllSounds() methode toevoegen (bug 157272)

### KParts

- Switch toevoegen om behandeling van KParts van venstertitels uit te schakelen
- Menu-item voor donate toevoegen aan het helpmenu van onze apps

### Kross

- Naam van enumerator "StandardButtons" van QDialogButtonBox repareren
- De eerste poging om bibliotheek te laden verwijderen omdat we libraryPaths in elk geval zullen proberen
- Crash repareren wanneer een methode getoond aan Kross QVariant teruggeeft met niet te herplaatsen gegevens
- Geen C-style casts gebruiken in void* (bug 325055)

### KRunner

- [QueryMatch] iconName toevoegen

### KTextEditor

- Schuifbalk tekstvoorbeeld tonen na een vertraging van 250ms
- voorbeeld en zaken verbergen bij scrollen van beeld van inhoud
- parent + toolview instellen, dit is mogelijk nodig om het item in de taakomschakelaar in Win10 te vermijden
- "KDE-Standaard" verwijderen uit coderingsvak
- Voorbeeld invouwen standaard aan
- Onderbroken onderstrepen vermijden voor voorbeel &amp; vergiftigen van cache voor regelindeling vermijden
- Altijd optie "Voorbeeld tonen van ingevouwen tekst" inschakelen
- TextPreview: de grooveRect-height aanpassen wanneer scrollPastEnd is ingeschakeld
- Voorbeeld op schuifbalk: groove rect gebruiken als deze niet de volledige hoogte gebruikt
- KTE::MovingRange::numberOfLines() toevoegen zoals ook KTE::Range heeft
- Voorbeeld bij code-invouwing: pop-up-hoogte instellen zodat alle verborgen regel passen
- Optie om voorbeeld van ingevouwen tekst uit te schakelen toevoegen
- Modeline 'voorbeeld invouwen' van type bool toevoegen
- ConfigInterface bekijken: ondersteuning van 'voorbeeld invouwen' van type bool
- Bool KateViewConfig::foldingPreview() en setFoldingPreview(bool) toevoegen
- Functie: tekstvoorbeeld tonen bij bewegen boven een ingevouwen blok code
- KateTextPreview: setShowFoldedLines() en showFoldedLines() toevoegen
- Modelines 'schuifbalk-minimap' [bool] en 'schuifbalk-voorbeeld' [bool] toevoegen
- Mini-map schuifbalk standaard inschakelen
- Nieuwe functie: tekstvoorbeeld tonen bij zweven boven de schuifbalk
- KateUndoGroup::editEnd(): KTE::Range doorgeven met const ref
- vim-modus behandeling van sneltoets repareren, na wijziging van gedrag in Qt 5.5 (bug 353332)
- Automatisch haakje: voeg geen ' teken in in de tekst
- ConfigInterface: scrollbar-minimap-config-key toevoegen aan in-/uitschakelen van mini-map-schuifbalk
- KTE::View::cursorToCoordinate() repareren wanneer top-bericht-widget zichtbaar is
- Refactoring van de geëmuleerde commandobalk
- Tekenen van artifacts repareren bij schuiven terwijl meldingen zichtbaar zijn (bug 363220)

### KWayland

- Een parent_window-event toevoegen aan het Plasma Window interface
- Op de juiste manier behandelen van vernietiging van een Pointer/Keyboard/Touch hulpbron
- [server] Dode code verwijderen: KeyboardInterface::Private::sendKeymap
- [server] Ondersteuning toevoegen voor handmatig instellen van de klembordselectie DataDeviceInterface
- [server] Maak zeker dat Resource::Private::get een nullptr teruggeeft als een nullptr wordt doorgegeven
- [server] Controle op hulpbron QtExtendedSurfaceInterface::close toevoegen
- [server] SurfaceInterface pointer weghalen in gerefereerde objecten bij vernietiging
- [server] Foutbericht repareren in QtSurfaceExtension Interface
- [server] Introduceer een Resource::unbound signaal uitgezonden vanuit de unbind-handler
- [server] Ken niet toe bij vernietiging van een nog steeds gerefereerd BufferInterface
- Vernietigingsverzoek toevoegen aan org_kde_kwin_shadow en org_kde_kwin_shadow_manager

### KWidgetsAddons

- Lezen van Unihan gegevens repareren
- Minimumgrootte repareren van KNewPasswordDialog (bug 342523)
- Dubbelzinnige contructor repareren op MSVC 2015
- Een probleem met uitlijnen repareren met de eigen OS X "macintosh"-stijl (bug 296810)

### KXMLGUI

- KXMLGui: Samenvoegen van indices repareren bij verwijderen van xmlgui-clients met acties in groepen (bug 64754)
- Niet waarschuwen over "bestand gevonden in compat-locatie" als het helemaal niet was gevonden
- Menu-item voor donate toevoegen aan het helpmenu van onze apps

### NetworkManagerQt

- Peap-label niet instellen gebaseerd op peap-versie
- Controle op Networkmanagerversie tijdens uitvoeren doen (vermijdt compileermoment versus uitvoermoment (bug 362736)

### Plasma Framework

- [Agenda] Pijltjestoetsen omdraaien bij talen met rechts-naar-links
- Plasma::Service::operationDescription() zou een QVariantMap terug moeten geven
- Geen ingebedde containers invoegen in containmentAt(pos) (bug 361777)
- Het kleurthema voor het systeempictogram voor opnieuw starten repareren (aanmeldscherm) (bug 364454)
- Taakbalkminiaturen met llvmpipe uitschakelen (bug 363371)
- tegen niet niet geldige applets waken (bug 364281)
- PluginLoader::loadApplet: herstel compatibiliteit voor fout geïnstalleerde applets
- corrigeer map voor PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: verbeter foutmelding over versiecompatibiliteit van plug-in
- Controle om QMenu op het scherm te houden voor multischermindelingen repareren
- Nieuw type container voor het systeemvak

### Solid

- Controle dat CPU geldig is repareren
- Behandel lezen van /proc/cpuinfo voor Arm-processors
- Zoek naar CPU's via subsysteem in plaats van stuurprogramma

### Sonnet

- Marker helper exe als non-gui app
- Sta nsspellcheck toe om standaard te worden gecompileerd op mac

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
