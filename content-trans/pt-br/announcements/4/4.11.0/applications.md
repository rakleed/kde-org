---
date: 2013-08-14
hidden: true
title: Os Aplicativos do KDE 4.11 trazem um grande avanço no gerenciamento de informações
  pessoais e outras melhorias gerais
---
O gestor de ficheiros Dolphin traz muitas pequenas correcções de erros e optimizações nesta versão. O carregamento de pastas grandes foi acelerado e precisa de cerca de 30&#37; de memória a menos. A actividade intensa no disco e no CPU é evitada através do carregamento das antevisões apenas para os itens visíveis. Ocorreram muitas mais melhorias: por exemplo, muitos erros que afectavam as pastas expandidas na vista de Detalhes foram corrigidos, sem que apareçam mais ícones de substituição &quot;desconhecido&quot; quando entra numa pasta; por outro lado, se carregar com o botão do meio do rato sobre um pacote irá abrir agora uma página nova com o conteúdo do pacote, criando uma experiência global mais consistente.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`O novo fluxo de envio posterior no Kontact` width="600px">}}

## Melhorias na Suíte Kontact

A Suíte Kontact novamente teve melhorias significativas de estabilidade, desempenho e uso de memória. A importação de pastas, mudança de mapas, obtenção de e-mails, marcação ou movimentação de grande quantidade de mensagens e o tempo de início foram melhorados nos últimos 6 meses. Veja <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>este blog</a> para mais detalhes. A <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>funcionalidade de arquivamento teve muitas correções de erros</a> e também ocorreram melhorias no Assistente de Importação, permitindo a importação das configurações do cliente de e-mail Trojitá e uma melhor importação a partir de diversos outros aplicativos. Encontre mais informações <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>aqui</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`O agente de arquivamento gerencia o armazenamento de e-mails de modo compactado` width="600px">}}

Esta versão também vem com diversas funcionalidades novas. Existe um <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>novo editor de temas para os cabeçalhos das mensagens de e-mail</a> e as imagens das mensagens podem ser dimensionadas na hora. A funcionalidade de <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Envio Posterior</a> permite a calendarização do envio das mensagens numa data e hora específicas, com a possibilidade adicional de repetir o envio com base num intervalo específico. O suporte de filtros do Sieve para o KMail (uma funcionalidade de IMAP que permite a filtragem no servidor) foi melhorado, podendo os utilizadores gerar programas de filtragem do Sieve <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>com uma interface simples de utilizar</a>. Na área da segurança, o KMail introduz a 'detecção automática de burlas', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>mostrando um aviso</a> quando as mensagens tiverem alguns dos truques típicos de 'phishing' (burla). Irá receber agora uma <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>notificação informativa</a> quando chegar correio novo e, por último, o escritor de 'blogs' Blogilo vem com um editor de HTML extremamente melhorado e baseado no QtWebKit.

## Suporte ampliado para linguagens de programação no Kate

O editor de texto avançado Kate disponibiliza novos plugins: Python (2 e 3), JavaScript e JQuery, Django e XML. Eles introduzem funcionalidades como completação automática estática e dinâmica, verificações de sintaxe, inserção de trechos de código e capacidade de recuar automaticamente o XML com teclas de atalho. Mas ainda existe mais para os fãs do Python: um console Python que oferece informações aprofundadas sobre um arquivo de código aberto. Algumas pequenas melhorias na interface também foram feitas, incluindo as <a href="http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/">novas notificações passivas para a funcionalidade de pesquisa</a>, <a href="http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/">otimizações no modo VIM</a> e <a href="http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/">novas funcionalidades de desdobramento de texto</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`O KStars mostra os próximos eventos interessantes que serão visíveis na sua localização` width="600px">}}

## Outras melhorias nos aplicativos

Na área de jogos e educação ocorreram várias pequenas e grandes novas funcionalidades e otimizações. Os datilógrafos poderão gostar do suporte à digitação da direita para a esquerda do KTouch, enquanto o amigo do observador de estrelas, o KStars, tem agora uma ferramenta que mostra os eventos interessantes que irão ocorrer na sua localização. As ferramentas matemáticas Rocs, Kig, Cantor e KAlgebra também tiveram alguma atenção dos desenvolvedores, passando a ter suporte a mais infraestruturas e cálculos. Além disso, o jogo KJumpingCube tem agora tamanhos de tabuleiros maiores, novos níveis de experiência, respostas mais rápidas e uma interface melhorada.

O aplicativo de desenho Kolourpaint consegue lidar com o formato de imagens WebP e o visualizador de documentos universal Okular tem ferramentas de revisão configuráveis e introduz suporte para desfazer/refazer alterações em formulários e anotações. O reprodutor de áudio JuK tem suporte a reprodução e edição de metadados do novo formato de áudio Ogg Opus (é necessário que o driver de áudio e a TagLib também tenham suporte ao Ogg Opus).

#### Instalando aplicativos do KDE

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. <br />

##### Locais dos pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.11.0'>transferir à vontade</a> o código-fonte completo de 4.11.0. As instruções de compilação e instalação da aplicação do KDE 4.11.0 está disponível na <a href='/info/4/4.11.0#binary'>Página de Informações do 4.11.0</a>.

#### Requisitos do sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.<br />Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.

## Também anunciado hoje:

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="A Área de Trabalho Plasma do KDE 4.11" width="64" height="64" /> A Área de Trabalho Plasma 4.11 Continua a Afinar a Experiência do Utilizador</a>

Preparando-se para uma manutenção a longo prazo, o Espaço de Trabalho Plasma oferece melhorias para as funcionalidades básicas com uma barra de tarefas mais suave, um widget de bateria mais inteligente e um mixer som melhorado. A inclusão do KScreen traz um tratamento inteligente para vários monitores do ambiente de trabalho, assim como algumas melhorias de desempenho em grande escala, combinadas com pequenos ajustes de usabilidade, para proporcionar uma experiência geral mais agradável.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento do KDE 4.11"/> A Plataforma do KDE 4.11 Oferece uma Melhor Performance</a>

Esta versão da Plataforma do KDE 4.11 continua com foco na estabilidade. As novas funcionalidades serão implementadas na nossa futura versão do KDE Frameworks 5.0, mas para a versão estável continuaremos com a otimização da plataforma Nepomuk.
