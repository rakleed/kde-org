---
aliases:
- ../announce-applications-14.12-beta2
custom_spread_install: true
date: '2014-11-13'
description: O KDE Lança as Aplicações 14.12 Beta 2.
layout: application
title: O KDE disponibiliza a segunda versão Beta do KDE Applications 14.12
---
13 de novembro de 2014. Hoje o KDE disponibilizou o beta da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 14.12 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalando os pacotes binários do KDE Applications 14.12 Beta 2

<em>Pacotes</em>. Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do 14.12 Beta2 (internamente 14.11.90) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilando o KDE Applications 14.12 Beta 2

Poderá <a href='http://download.kde.org/unstable/applications/14.11.90/src/'>transferir à vontade</a> o código-fonte completo das Aplicações do KDE 14.12 Beta2. As instruções de compilação e instalação estão disponíveis na <a href='/info/applications/applications-14.11.90'>Página de Informações das Aplicações do KDE Beta 2</a>.
