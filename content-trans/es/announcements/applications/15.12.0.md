---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE lanza las Aplicaciones 15.12.
layout: application
release: applications-15.12.0
title: KDE lanza las Aplicaciones de KDE 15.12.0
version: 15.12.0
---
Hoy, 16 de diciembre de 2015, KDE ha lanzado las Aplicaciones de KDE 15.12.

Este mes, diciembre de 2015, KDE se complace en anunciar el lanzamiento de las Aplicaciones de KDE 15.12. Esta versión incorpora una nueva aplicación y nuevas funcionalidades y correcciones de errores para las aplicaciones ya existentes. El equipo se está esforzando en proporcionar a los usuarios un escritorio y unas aplicaciones de la mejor calidad, así que contamos con ustedes para que nos envíen sus comentarios.

En esta versión, hemos actualizado un conjunto completo de aplicaciones para que utilicen el nuevo <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, que incluye <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> y más juegos de KDE, aparte de la interfaz del complemento Image de KDE y sus bibliotecas. Esto hace que el número total de aplicaciones que utilizan KDE Frameworks 5 ascienda a 126.

### Una nueva adición espectacular

Tras catorce años siendo parte de KDE, se ha jubilado a KSnapshot, que ha sido sustituida por una nueva aplicación para hacer capturas de pantalla, Spectacle.

Con las nuevas funcionalidades y una interfaz de usuario completamente nueva, Spectacle consigue la máxima sencillez y discreción al hacer capturas de pantalla. Además de lo que se podía hacer con KSnapshot, Spectacle también puede realizar capturas de pantalla compuestas de los menús emergentes de sus pantallas padre o realizar capturas de pantalla de la pantalla completa (o de la ventana activa en el momento) sin ni siquiera iniciar Spectacle, simplemente utilizando los accesos rápidos de teclado Mays+Impr Pant y Meta+Impr Pant, respectivamente.

También hemos optimizado drásticamente el tiempo de inicio de las aplicaciones para reducir al mínimo imprescindible el intervalo de tiempo que transcurre desde que el usuario inicia la aplicación hasta que se captura la pantalla.

### Ajustes en todas partes

En este ciclo, muchas de las aplicaciones se han sometido a un significativo ajuste además de a una estabilización y solución de errores.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, el editor de vídeo no lineal, ha experimentado importantes correcciones de errores en su interfaz de usuario. Ahora se pueden copiar y pegar elementos en la línea del tiempo y alternar la transparencia fácilmente en una pista dada. Los colores de los iconos se ajustan automáticamente al tema de la interfaz de usuario principal, lo que permite visualizarlos mejor.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark puede mostrar ahora comentarios de ZIP`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, el administrador de archivos comprimidos ahora puede mostrar los comentarios integrados en los archivos comprimidos ZIP y RAR. Hemos mejorado la compatibilidad con los caracteres Unicode en los nombres de archivo ZIP y ahora se puede detectar archivos corruptos y recuperar de ellos la mayor cantidad de datos posible. También se pueden abrir archivos comprimidos en su aplicación original y hemos corregido las funcionalidades de arrastrar y soltar en el escritorio y de vista previa de archivos XML.

### Jugar y no trabajar

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nueva pantalla de bienvenida de KSudoku (en Mac OS X)`>}}

Los desarrolladores de los juegos de KDE han estado trabajando intensamente durante los pasados meses para optimizar nuestros juegos de manera que ofrezcan una experiencia más fluida y sofisticada. Como resultado, tenemos muchas cosas nuevas para que disfruten nuestros usuarios.

En <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, hemos añadido dos nuevos conjuntos de niveles, uno que permite cavar mientras se cae y otro en el que no. También hemos añadido soluciones para varios niveles existentes. Y para aumentar la dificultad, ahora no se permite cavar mientras se cae en algunos de los conjuntos de niveles existentes.

En <a href='https://www.kde.org/applications/games/ksudoku/'>Sudoku</a>, ahora se puede imprimir los rompecabezas Mathdoku y Killer Sudoku. La nueva presentación de varias columnas en la pantalla de bienvenida de Sudoku facilita la visualización de la variedad de rompecabezas disponible y las columnas se ajustan de manera automática cuando se cambia el tamaño de la pantalla. Hemos aplicado esto mismo a Palapeli, lo que facilita ver la colección de puzzles de un vistazo.

También hemos mejorado la estabilidad de juegos como KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> y <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> y la experiencia general se ha mejorado notablemente. KTuberling, Klickety y KNavalBattle también se han actualizado para que usen Plasma %.

### Correcciones de errores importantes

¿No resulta odioso que su aplicación favorita falle en el momento más inoportuno? Para solucionar esto, hemos trabajado intensamente en la solución de numerosos errores, pero probablemente nos hayamos dejado alguno, así que no olvide <a href='https://bugs.kde.org'>avisarnos cuando detecte alguno</a>.

En nuestro administrador de archivos, <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, se ha mejorado la estabilidad y se han solucionado algunos errores que hacen más fluido el deslizamiento de la pantalla. En <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, se ha solucionado un problema muy molesto que ocurría con el texto en segundo plano. En <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, hemos intentado solucionar un error que ocurría cuando se apagaba el equipo, además de despejar la interfaz gráfica y de añadir un nuevo componente para limpiar la caché.

En la <a href='https://userbase.kde.org/Kontact'>Suite Kontact</a>, se han añadido numerosas funcionalidades, se han solucionado errores graves y se ha optimizado el rendimiento. De hecho, se ha realizado tanto desarrollo en este ciclo que hemos avanzado hasta la versión 5.1. El equipo está trabajando intensamente y espera con ilusión los comentarios de los usuarios.

### Avanzando

Como parte del esfuerzo para modernizar nuestra oferta, hemos eliminado varias aplicaciones, las cuales ya no se van a lanzar como parte de las Aplicaciones de KDE 15.12

Hemos eliminado 4 aplicaciones de esta versión: Amor, KTux, KSnapshot and SuperKaramba. Como ya se ha explicado, KSnapshot se ha sustituido por Spectacle y, básicamente, Plasma sustituye a SuperKaramba como motor de elementos gráficos. Hemos eliminado los salvapantallas independientes porque el bloqueo de pantalla se gestiona de manera muy diferente en los escritorios actuales.

También hemos eliminado 3 paquetes de material gráfico y visual (kde-base-artwork, kde-wallpapers y kdeartwork), ya que su contenido no había cambiado desde hace mucho tiempo.

### Registro de cambios completo
