---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---
### Baloo

- [baloo_file] Esperar que se inicie el proceso del extractor.
- [balooctl] Se ha añadido una orden para mostrar los archivos que no se han podido indexar (error 406116).
- Se ha añadido QML a los tipos de código fuente.
- [balooctl] Capturar la constante «totalsize» en la lambda.
- [balooctl] Cambiar la salida multilínea a una nueva aplicación auxiliar.
- [balooctl] Usar nueva aplicación auxiliar en la salida de JSON.
- [balooctl] Usar nueva aplicación auxiliar para salida de formato sencillo.
- [balooctl] Factor out file index status collection from output
- Keep empty Json metadata docs out of DocumentData DB
- [balooshow] Allow referencing files by URL from hardlink
- [balooshow] Suppress warning when URL refers to unindexed file
- [MTimeDB] Allow timestamp newer than the newest document in range match
- [MTimeDB] Use exact match when exact match is requested
- [balooctl] Cleanup handling of different positional arguments
- [balooctl] Extend options help text, improve error checking
- [balooctl] Use more understandable names for size in status output
- [balooctl] clear command: Remove bogus check for documentData, cleanup
- [kio_search] Fix warning, add UDSEntry for "." in listDir
- Use hex notation for DocumentOperation flag enum
- Calculate total DB size correctly
- Postpone term parsing until needed, do not set both term and searchstring
- Don't add default valued date filters to json
- Usar el formato JSON compacto al convertir URL de consulta.
- [balooshow] Do not print a bogus warning for a non-indexed file

### Iconos Brisa

- Add non-symbolic 16px versions of find-location and mark-location
- Symlink preferences-system-windows-effect-flipswitch to preferences-system-tabbox
- Add "edit-delete-remove" icon symlink and add 22px version of "paint-none" and "edit-none"
- Use consistent default Kickoff user icon
- Add an icon for Thunderbolt KCM
- Sharpen Z's in system-suspend* icons
- Improve "widget-alternatives" icon
- Add go-up/down/next/previous-skip
- Update KDE logo to be closer to original
- Se ha añadido el icono para alternativas.

### Módulos CMake adicionales

- Bug fix: find c++ stl using regex
- Unconditionally enable -DQT_STRICT_ITERATORS, not just in debug mode

### KArchive

- KTar: Proteger contra tamaños de «longlink» negativos.
- Fix invalid memory write on malformed tar files
- Corregir una fuga de memoria al leer ciertos archivos «tar».
- Corregir el uso de memoria sin inicializar al leer archivos «tar» mal formados.
- Corregir un desbordamiento de búfer al leer archivos mal formados.
- Fix null-dereference on malformed tar files
- Instalar el archivo de cabecera «krcc.h».
- Corregir el borrado doble de archivos dañados.
- Disallow copy of KArchiveDirectoryPrivate and KArchivePrivate
- Fix KArchive::findOrCreate running out of stack on VERY LONG paths
- Introduce and use KArchiveDirectory::addEntryV2
- removeEntry can fail so it's good to know if it did
- KZip: Corregir el uso de memoria asignada tras haberla liberado en archivos dañados.

### KAuth

- Force KAuth helpers to have UTF-8 support (bug 384294)

### KBookmarks

- Add support for KBookmarkOwner to communicate if it has tabs open

### KCMUtils

- Use size hints from the ApplicationItem itself
- Fix Oxygen background gradient for QML modules

### KConfig

- Add Notify capability to KConfigXT

### KCoreAddons

- Fix wrong "Unable to find service type" warnings
- New class KOSRelease - a parser for os-release files

### KDeclarative

- [KeySequenceItem] Make the clear button have the same height as shortcut button
- Plotter: Scope GL Program to lifespan of scenegraph node (bug 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: Don't show english text to the user
- [ConfigModule] Pass initial properties in push()
- Enable glGetGraphicsResetStatus support by default on Qt &gt;= 5.13 (bug 364766)

### KDED

- Install .desktop file for kded5 (bug 387556)

### KFileMetaData

- [TagLibExtractor] Fix crash on invalid Speex files (bug 403902)
- Fix exivextractor crash with malformed files (bug 405210)
- Declarar la propiedades como tipo meta.
- Change properties attributes for consistency
- Handle variant list in formatting functions
- Fix for Windows' LARGE_INTEGER type
- Fix (compilation) errors for Windows UserMetaData implementation
- Añadir el tipo MIME que faltaba al escritor de «taglib».
- [UserMetaData] Handle changes in attribute data size correctly
- [UserMetaData] Untangle Windows, Linux/BSD/Mac and stub code

### KGlobalAccel

- Copy container in Component::cleanUp before iterating
- Don't use qAsConst over a temporary variable (bug 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb: se han añadido las festividades de Zambia.
- holidays/plan2/holiday_lv_lv: se ha corregido el día de san Juan.
- holiday_mu_en: Festividades de 2019 en Mauricio.
- holiday_th_en-gb: Actualización para 2019 (fallo 402277).
- Actualización de las festividades japonesas.
- Se han añadido las festividades públicas de Baja Sajonia (Alemania).

### KImageFormats

- tga: don't try to read more than max_palette_size into palette
- tga: memset dst if read fails
- tga: memset the whole palette array, not only the palette_size
- Initialize the unread bits of _starttab
- xcf: corregir el uso de memoria sin inicializar en documentos dañados.
- ras: Don't overread input on malformed files
- xcf: layer is const in copy and merge, mark it as such

### KIO

- [FileWidget] Replace "Filter:" with "File type:" when saving with a limited list of mimetypes (bug 79903)
- Newly created 'Link to Application' files have a generic icon
- [Properties dialog] Use the string "Free space" instead of "Disk usage" (bug 406630)
- Fill UDSEntry::UDS_CREATION_TIME under linux when glibc &gt;= 2.28
- [KUrlNavigator] Fix URL navigation when exiting archive with krarc and Dolphin (bug 386448)
- [KDynamicJobTracker] When kuiserver isn't available, also fall back to widget dialog (bug 379887)

### Kirigami

- [aboutpage] hide Authors header if there are no authors
- Update qrc.in to match .qrc (missing ActionMenuItem)
- Make sure we don't squeeze out the ActionButton (bug 406678)
- Pages: export correct contentHeight/implicit sizes
- [ColumnView] Also check for index in child filter..
- [ColumnView] Don't let mouse back button go back beyond first page
- header has immediately the proper size

### KConfigWidgets

- [KUiServerJobTracker] Track kuiserver service life time and re-register jobs if needed

### KNewStuff

- Remove pixelated border (bug 391108)

### KNotification

- [Notify by Portal] Support default action and priority hints
- [KNotification] Se ha añadido «HighUrgency».
- [KNotifications] Update when flags, urls, or urgency change
- Permitir definir la urgencia de las notificaciones.

### Framework KPackage

- Se han añadido las propiedades que faltaban en «kpackage-generic.desktop».
- kpackagetool: Leer «kpackage-generic.desktop» de «qrc».
- AppStream generation: make sure we look up for the package structure on packages that have metadata.desktop/json too

### KTextEditor

- Review kate config pages to improve maintenance friendliness
- Allow to change the Mode, after changing the Highlighting
- ViewConfig: Use new generic config interface
- Fix bookmark pixmap painting on icon bar
- Ensure the left border miss no change of the count of line number digits
- Fix to show folding preview when move the mouse from bottom to top
- Review IconBorder
- Add input methods to input method status bar button
- Pintar el marcador de plegado con un color apropiado y hacerlo más visible.
- remove default shortcut F6 to show icon border
- Add action to toggle folding of child ranges (bug 344414)
- Retitle button "Close" to "Close file" when a file has been removed on disk (bug 406305)
- up copy-right, perhaps that should be a define, too
- avoid conflicting shortcuts for switching tabs
- KateIconBorder: Fix folding popup width and height
- avoid view jump to bottom on folding changes
- DocumentPrivate: Respect indention mode when block selection (bug 395430)
- ViewInternal: Fix makeVisible(..) (bug 306745)
- DocumentPrivate: Make bracket handling smart (bug 368580)
- ViewInternal: Review drop event
- Allow to close a document whose file was deleted on disk
- KateIconBorder: Use UTF-8 char instead of special pixmap as dyn wrap indicator
- KateIconBorder: Ensure Dyn Wrap Marker are shown
- KateIconBorder: Code cosmetic
- DocumentPrivate: Support auto bracket in block selection mode (bug 382213)

### KUnitConversion

- Fix l/100 km to MPG conversion (bug 378967)

### Framework KWallet

- Set correct kwalletd_bin_path
- Export path of kwalletd binary for kwallet_pam

### KWayland

- Add CriticalNotification window type to PlasmaShellSurface protocol
- Implement wl_eglstream_controller Server Interface

### KWidgetsAddons

- Se ha actualizado «kcharselect-data» a Unicode 12.1.
- KCharSelect's internal model: ensure rowCount() is 0 for valid indexes

### KWindowSystem

- Introduce CriticalNotificationType
- Support NET_WM_STATE_FOCUSED
- Document that modToStringUser and stringUserToMod only deal with english strings

### KXMLGUI

- KKeySequenceWidget: Don't show English strings to the user

### NetworkManagerQt

- WireGuard: Do not require 'private-key' to be non-empty for 'private-key-flags'
- WireGuard: workaround wrong secret flag type
- WireGuard: private-key and preshared-keys can be requested together

### Framework de Plasma

- PlatformComponentsPlugin: fix plugin iid to QQmlExtensionInterface
- IconItem: remove remaining &amp; unused smooth property bits
- [Dialog] Add CriticalNotification type
- Fix wrong group names for 22, 32 px in audio.svg
- make the mobile text toolbar appear only on press
- use the new Kirigami.WheelHandler
- Add more icon sizes for audio, configure, distribute
- [FrameSvgItem] Update filtering on smooth changes
- Air/Oxygen desktoptheme: fix progressbar height using "hint-bar-size"
- Fix stylesheet support for audio-volume-medium
- Update audio, drive, edit, go, list, media, plasmavault icons to match breeze-icons
- Align z's to pixel grid in system.svg
- use the mobiletextcursor from proper namespace
- [FrameSvgItem] Respect smooth property
- Oxygen desktoptheme: add padding to hands, against jagged outline on rotation
- SvgItem, IconItem: drop "smooth" property override, update node on change
- Support gzipping of svgz also on windows, using 7z
- Air/Oxygen desktoptheme: fix hand offsets with hint-*-rotation-center-offset
- Add invokable public API for emitting contextualActionsAboutToShow
- Breeze desktoptheme clock: support hand shadow offset hint of Plasma 5.16
- Keep desktoptheme SVG files uncompressed in repo, install svgz
- separate mobile text selection to avoid recursive imports
- Use more appropriate "Alternatives" icon and text
- FrameSvgItem: add "mask" property

### Prison

- Aztec: Fix padding if the last partial codeword is all one bits

### QQC2StyleBridge

- Avoid nesting Controls in TextField (bug 406851)
- make the mobile text toolbar appear only on press
- [TabBar] Update height when TabButtons are added dynamically
- use the new Kirigami.WheelHandler
- Permitir el tamaño de iconos personalizado en ToolButton.
- It compile fine without foreach

### Solid

- [Fstab] Add support for non-network filesystems
- [FsTab] Add cache for device file system type
- [Fstab] Preparatory work for enabling filesystems beyond NFS/SMB
- Fix no member named 'setTime_t' in 'QDateTime' error while building (bug 405554)

### Resaltado de sintaxis

- Se ha añadido resaltado de sintaxis el intérprete «fish».
- AppArmor: don't highlight variable assignments and alias rules within profiles

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
