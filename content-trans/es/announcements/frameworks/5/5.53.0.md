---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Se han corregido las búsquedas de puntuación 10 (5 estrellas) (error 357960).
- Evitar la escritura de datos sin modificar en bases de datos de términos.
- No añadir Type::Document/Presentation/Spreadsheet dos veces para documentos de MS Office.
- Inicializar kcrash correctamente.
- Asegurar que solo existe un MTime por documento en la base de datos de MTime.
- [Extractor] Usar la serialización QDataStream en lugar de otra amañada.
- [Extractor] Sustituir el manejador de E/S de cosecha propia por QDataStream, capturar HUP.

### Iconos Brisa

- Añadir iconos para application-vnd.appimage/x-iso9660-appimage.
- Añadir el icono de 22 píxeles dialog-warning (error 397983).
- Fix angle and margin of 32px dialog-ok-apply (bug 393608)
- Change primary monochrome icon colors to match new HIG colors
- Change archive-* action icons to represent archives (bug 399253)
- Add help-browser symlink to 16px and 22px directories (bug 400852)
- Add new generic sorting icons; rename existing sorting icons
- Add root version of drive-harddisk (bug 399307)

### Módulos CMake adicionales

- New module: FindLibExiv2.cmake

### Integración con Frameworks

- Add option BUILD_KPACKAGE_INSTALL_HANDLERS to skip building install handlers

### Herramientas KDE Doxygen

- Add busy indicator during research and make the research asynchronous (bug 379281)
- Normalize all input paths with the os.path.normpath function (bug 392428)

### KCMUtils

- Perfect alignment between QML and QWidget KCM titles
- Add context to kcmodule connection to lambdas (bug 397894)

### KCoreAddons

- Permitir que se pueda usar «KAboutData/License/Person» desde QML.
- Fix crash if XDG_CACHE_HOME directory is too small or out of space (bug 339829)

### KDE DNS-SD

- now installs kdnssd_version.h to check the version of the lib
- do not leak resolver in remoteservice
- prevent avahi signal racing
- Corregir para macOS.

### KFileMetaData

- Revive 'Description' property for DublinCore metadata
- add a description property to KFileMetaData
- [KFileMetaData] Add extractor for DSC conforming (Encapsulated) Postscript
- [ExtractorCollection] Avoid dependency of kcoreaddons for CI tests
- Permitir el uso de archivos «speex» en «taglibextractor».
- add two more internet sources regarding tagging information
- simplify handling of id3 tags
- [XmlExtractor] Use QXmlStreamReader for better performance

### KIO

- Fix assert when cleaning up symlinks in PreviewJob
- Add the possibility to have a keyboard shortcut to create a file
- [KUrlNavigator] Re-activate on mouse middle button click (bug 386453)
- Se han eliminado los proveedores de búsqueda obsoletos.
- Port more search providers to HTTPS
- Export again KFilePlaceEditDialog (bug 376619)
- Restore sendfile support
- [ioslaves/trash] Handle broken symlinks in deleted subdirectories (bug 400990)
- [RenameDialog] Fix layout when using the NoRename flag
- Se ha añadido el «@since» que faltaba para KFilePlacesModel::TagsType.
- [KDirOperator] Use the new <code>view-sort</code> icon for the sort order chooser
- Use HTTPS for all search providers that support it
- Disable unmount option for / or /home (bug 399659)
- [KFilePlaceEditDialog] Fix include guard
- [Places panel] Use new <code>folder-root</code> icon for Root item
- [KSambaShare] Make "net usershare info" parser testable
- Give the file dialogs a "Sort by" menu button on the toolbar

### Kirigami

- DelegateRecycler: Don't create a new propertiesTracker for every delegate
- Move the about page from Discover to Kirigami
- Ocultar el cajón de contexto cuando existe una barra de herramientas global.
- ensure all items are laid out (bug 400671)
- change index on pressed, not on clicked (bug 400518)
- new text sizes for Headings
- Los cajones laterales no mueven cabeceras/pies globales.

### KNewStuff

- Add programmaticaly useful error signalling

### KNotification

- Rename NotifyByFlatpak to NotifyByPortal
- Notification portal: support pixmaps in notifications

### Framework KPackage

- Don't generate appstream data for files that lack a description (bug 400431)
- Capture package metadata before install start

### KRunner

- When re-using runners when reloading, reload their configuration (bug 399621)

### KTextEditor

- Permitir prioridades negativas de definición de sintaxis.
- Expose "Toggle Comment" feature through tools menu and default shortcut (bug 387654)
- Fix hidden languages in the mode menu
- SpellCheckBar: Use DictionaryComboBox instead of plain QComboBox
- KTextEditor::ViewPrivate: Avoid warning "Text requested for invalid range"
- Android: No need to define log2 anymore
- disconnect contextmenu from all aboutToXXContextMenu receivers (bug 401069)
- Introduce AbstractAnnotationItemDelegate for more control by consumer

### KUnitConversion

- Updated with petroleum industry units (bug 388074)

### KWayland

- Autogenerate logging file + fix categories file
- Add VirtualDesktops to PlasmaWindowModel
- Update PlasmaWindowModel test to reflect VirtualDesktop changes
- Cleanup windowInterface in tests before windowManagement is destroyed
- Delete the correct item in removeDesktop
- Cleanup Virtual Desktop Manager list entry in PlasmaVirtualDesktop destructor
- Correct version of newly added PlasmaVirtualDesktop interface
- [server] Text input content hint and purpose per protocol version
- [server] Put text-input (de-)activate, en-/disable callbacks in child classes
- [server] Put set surrounding text callback with uint in v0 class
- [server] Put some text-input v0 exclusive callbacks in v0 class

### KWidgetsAddons

- Add level api from Kirigami.Heading

### KXMLGUI

- Update the "About KDE" text

### NetworkManagerQt

- Fixed a bug(error?) in ipv4 &amp; ipv6 settings
- Add ovs-bridge and ovs-interface setting
- Actualización de las preferencias de los túneles IP.
- Add proxy and user setting
- Añadir la configuración de IpTunnel.
- We can now build tun setting test all the time
- Añadir opciones para IPv6 que faltaban.
- Listen for added DBus interfaces instead of registered services (bug 400359)

### Framework de Plasma

- feature parity of Menu with the Desktop style
- Qt 5.9 is now the minimum required version
- Add back (accidentally?) deleted line in CMakeLists.txt
- 100% consistency with kirigami heading sizing
- more homogeneous look with Kirigami headings
- install the processed version of private imports
- Mobile text selection controls
- Update breeze-light and breeze-dark colorschemes
- Fixed a number of memory leaks (thanks to ASAN)

### Purpose

- phabricator plugin: use Arcanist's diff.rev. order (bug 401565)
- Proporcionar un título para JobDialog.
- Allow the JobDialog to get a nicer initial size (bug 400873)
- Hacer posible que «menudemo» pueda compartir diferentes URL.
- Use QQC2 for the JobDialog (bug 400997)

### QQC2StyleBridge

- consistent sizing of item compared to QWidgets
- Corregir el tamaño del menú.
- Asegurar que los objetos móviles están alineados al píxel.
- Permitir el uso de aplicaciones basadas en QGuiApplication (error 396287).
- touchscreen text controls
- Size according to specified icon width and height
- Respetar la propiedad «flat» de los botones.
- Fix issue where there's only one element on the menu (bug 400517)

### Solid

- Fix root disk icon change so that it doesn't erroneously change other icons

### Sonnet

- DictionaryComboBoxTest: Add stretch to avoid expanding Dump button

### Resaltado de sintaxis

- BrightScript: Allow sub to be unnamed
- Se ha añadido resaltado de sintaxis para las trazas de depuración de Wayland.
- Se ha añadido resaltado de sintaxis para TypeScript y TypeScript React.
- Rust y Yacc/Bison: Mejorar los comentarios.
- Corregir el shebang de Prolog y de Lua.
- Fix language load after including keywords from this language in another file
- Añadir la sintaxis para BrightScript.
- debchangelog: Añadir Disco Dingo.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
