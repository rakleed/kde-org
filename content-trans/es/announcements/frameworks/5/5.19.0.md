---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Simplificar la búsqueda e inicialización del complemento «attica».

### Iconos Brisa

- Muchos iconos nuevos
- Se han añadido iconos de tipos MIME que faltaban al conjunto de iconos Oxígeno.

### Módulos CMake adicionales

- ECMAddAppIcon: Usar ruta absoluta al operar sobre iconos.
- Asegurarse de que se busca el prefijo en Android.
- Añadir un módulo FindPoppler
- Usar PATH_SUFFIXES en ecm_find_package_handle_library_components()

### KActivities

- No llamar a exec() desde QML (error 357435)
- La biblioteca KActivitiesStats está ahora en un repositorio separado

### KAuth

- Realizar también «preAuthAction» para los motores con «AuthorizeFromHelperCapability».
- Se ha corregido el nombre del servicio DBus del agente «polkit».

### KCMUtils

- Corregir problemas de HiDPI en KCMUtils

### KCompletion

- El método «KLineEdit::setUrlDropsEnabled» no se puede marcar como obsoleto.

### KConfigWidgets

- Se ha añadido un esquema de color «Complementario» a «kcolorscheme».

### KCrash

- Se ha actualizado la documentación de «KCrash::initialize». Se insta a los desarrolladores de aplicaciones a llamarlo explícitamente.

### KDeclarative

- Se han limpiado las dependencias para «KDeclarative/QuickAddons».
- [KWindowSystemProxy] Se ha añadido un «setter» para «showingDesktop».
- DropArea: Corregir el evento «dragEnter» ignorado correctamente con «preventStealing».
- DragArea: Implementar el elemento delegado de arrastre.
- DragDropEvent: Se ha añadido la función «ignore()».

### KDED

- Se ha revertido la corrección de «BlockingQueuedConnection», ya que Qt 5.6 contendrá una corrección mejor.
- Hacer un registro de «kded» con los alias especificados por los módulos de «kded».

### Soporte de KDELibs 4

- kdelibs4support necesita kded (para kdedmodule.desktop)

### KFileMetaData

- Permitir la consulta de la URL de origen del archivo.

### KGlobalAccel

- Impedir un cuelgue en el caso de que dbus no esté disponible

### Complementos KDE GUI

- Se ha corregido el listado de las paletas disponibles en el diálogo de color.

### KHTML

- Se ha corregido la detección del tipo de icono del enlace (también conocido como «favicon»).

### KI18n

- Se ha reducido el uso de la API de «gettext».

### KImageFormats

- Se han añadido los complementos de «imageio» «kra» y «ora» (solo lectura).

### KInit

- Ignorar la ventana gráfica en el escritorio actual en la información de arranque inicial.
- Se ha portado «klauncher» a «xcb».
- Usar un «xcb» para interactuar con «KStartupInfo».

### KIO

- Nueva clase «FavIconRequestJob» en la nueva biblioteca «KIOGui», para la obtención de «faviconos».
- Corregir el bloqueo de «KDirListerCache» con dos listas para un directorio vacío en la caché (error 278431).
- Hacer que la implementación para Windows de «KIO::stat» para el protocolo «file:/» muestre un error si el archivo no existe.
- No dar por sentado que los archivos existentes en directorios de solo lectura no se pueden borrar en Windows.
- Se ha corregido el archivo «.pri» para «KIOWidgets»: depende de «KIOCore», no de sí mismo.
- Reparar la carga automática de «kcookiejar»; los valores se intercambiaron en 6db255388532a4.
- Hacer que «kcookiejar» sea accesible bajo el nombre de servicio DBus «org.kde.kcookiejar5».
- kssld: Instalar el archivo de servicio DBus para «org.kde.kssld5».
- Proporcionar un archivo de servicio DBus para «org.kde.kpasswdserver».
- [kio_ftp] Se ha corregido la visualización de la fecha/hora de modificación de los archivos/directorios (error 354597).
- [kio_help] Se ha corregido el envío de basura al servir archivos estáticos.
- [kio_http] Probar la autenticación «NTLMv2» si el servidor deniega «NTLMv1».
- [kio_http] Se ha corregido la adaptación de fallos que rompían la caché.
- [kio_http] Se ha corregido la creación de la respuesta «NTLMv2 stage 3».
- [kio_http] Se ha corregido la espera hasta que el limpiador de la caché escuche al «socket».
- kio_http_cache_cleaner: No salir durante el inicio si el directorio de la caché no existe todavía.
- Cambiar el nombre DBus de «kio_http_cache_cleaner» para que no salga si está en ejecución la versión de kde4.

### KItemModels

- KRecursiveFilterProxyModel::match: Se ha corregido un fallo.

### KConfigWidgets

- Se ha corregido un fallo en los diálogos de «KJob» (error 346215).

### Framework para paquetes

- Evitar que se encuentre el mismo paquete múltiples veces en distintas rutas.

### KParts

- PartManager: Dejar de rastrear un widget incluso si ya no es de nivel superior (error 355711).

### KTextEditor

- Mejor comportamiento para la funcionalidad «insertar paréntesis alrededor» de los paréntesis automáticos.
- Cambiar la clave de la opción para aplicar un nuevo valor predeterminado, nueva línea al final de archivo = true.
- Se han eliminado algunas llamadas sospechosas a «setUpdatesEnabled» (error 353088).
- Demorar la emisión de «verticalScrollPositionChanged» hasta que todo sea consistente para el plegado (fallo 342512).
- Se ha parcheado la actualización de la sustitución de etiquetas (error 330634).
- Actualizar la paleta solo una vez para el evento de cambio que pertenece a «qApp» (error 358526).
- Añadir saltos de línea por omisión al alcanzar EOF.
- Añadir el archivo de resaltado de sintaxis para NSIS

### Framework KWallet

- Duplicar el descriptor del archivo al abrirlo para leer el entorno.

### KWidgetsAddons

- Corregir el funcionamiento de los widgets acompañantes con KFontRequester.
- KNewPasswordDialog: Usar «KMessageWidget».
- Impedir un fallo al salir en «KSelectAction::~KSelectAction».

### KWindowSystem

- Cambiar la cabecera de licencia de «Library GPL 2 or later» a «Lesser GPL 2.1 or later».
- Se ha corregido un fallo si «KWindowSystem::mapViewport» se llama sin una «QCoreApplication».
- Poner «QX11Info::appRootWindow» en caché en «eventFilter» (error 356479).
- Deshacerse de la dependencia de «QApplication» (error 354811).

### KXMLGUI

- Añadir una opción para desactivar «KGlobalAccel» durante la compilación.
- Reparar la ruta al esquema de accesos rápidos de la aplicación.
- Se ha corregido el listado de archivos de accesos rápidos (uso incorrecto de «QDir»).

### NetworkManagerQt

- Volver a comprobar el estado de la conexión y otras propiedades para asegurarse de que son reales (versión 2) (fallo 352326).

### Iconos de Oxígeno

- Eliminar archivos enlazados incorrectos.
- Añadir iconos para las aplicaciones de KDE.
- Añadir iconos de lugares de Brisa a Oxígeno.
- Sincronizar los iconos de tipos MIME de Oxígeno con los de Brisa.

### Framework de Plasma

- Se ha añadido una propiedad «separatorVisible».
- Eliminación más explícita de «m_appletInterfaces» (error 358551).
- Usar «complementaryColorScheme» de «KColorScheme».
- AppletQuickItem: No tratar de definir el tamaño inicial mayor que el tamaño del padre (error 358200).
- IconItem: Se ha añadido la propiedad «usesPlasmaTheme».
- No cargar la caja de herramientas para tipos distintos de escritorio y de panel.
- IconItem: Intentar cargar los iconos de «QIcon::fromTheme» como SVG (error 353358).
- Ignorar la comprobación si solo una parte del tamaño es cero en «compactRepresentationCheck» (error 358039).
- [Unidades] Devolver al menos 1 ms para las duraciones (error 357532).
- Añadir «clearActions()» para eliminar cada acción de la interfaz de la miniaplicación.
- [plasmaquick/diálogo] No usar «KWindowEffects» para el tipo de la ventana de notificación.
- Marcar como obsoleto «Applet::loadPlasmoid()».
- [Modelo de datos de PlasmaCore] No reiniciar el modelo cuando se elimina una fuente.
- Se ha corregido la sugerencia del margen con SVG de fondo del panel opaco.
- IconItem: Se ha añadido la propiedad «animated».
- [Unity] Escalar el tamaño de los iconos del escritorio.
- El botón se compone sobre los bordes.
- «paintedWidth/paintedheight» para «IconItem».

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
