---
aliases:
- ../announce-applications-19.04-rc
date: 2019-04-05
description: KDE rilascia Applications 19.04 Release Candidate.
layout: application
release: applications-19.03.90
title: KDE rilascia la Release Candidate di KDE Applications 19.04
version_number: 19.03.90
version_text: 19.04 Release Candidate
---
5 aprile 2019. Oggi KDE ha rilasciato la Release Candidate della nuova versione di KDE Applications. Con il congelamento di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione dei bug e sull'ulteriore rifinitura dei programmi.

Controlla le <a href='https://community.kde.org/Applications/19.04_Release_Notes'>note di rilascio della comunità</a> per informazioni sugli archivi e sui problemi noti. Un annuncio più completo verrà reso disponibile in concomitanza con la versione finale.

A causa della grande quantità di cambiamenti, i rilasci della serie 19.04 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di partecipare alla squadra installando la Release Candidate <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
