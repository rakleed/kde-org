---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE rilascia KDE Applications 15.04.2
layout: application
title: KDE rilascia KDE Applications 15.04.2
version: 15.04.2
---
2 giugno 2015. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../15.04.0'>KDE Applications 15.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 30 errori corretti includono miglioramenti a Gwenview, Kate, Kdenlive, kdepim, Konsole, Marble, Kgpg, Kig, ktp-call-ui e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine (Long Term Support) di Plasma Workspaces 4.11.20, KDE Development Platform 4.14.9 e della suite Kontact 4.14.9.
