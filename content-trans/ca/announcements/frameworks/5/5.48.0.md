---
aliases:
- ../../kde-frameworks-5.48.0
date: 2018-07-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Adapta els usos romanents de «qDebug()» a «qcDebug(ATTICA)»
- Esmena la verificació d'URL no vàlid de proveïdor
- Esmena l'URL no vàlid a l'especificació de l'API

### Baloo

- Elimina l'entrada no usada «X-KDE-DBus-ModuleName» de les metadades del connector de «kded»
- [tags_kio] L'URL de consulta cal que sigui un parell clau-valor
- El senyal d'estat de potència només s'ha d'emetre quan canvia l'estat de la potència
- baloodb: Fa els canvis a la descripció dels arguments de la línia d'ordres després de reanomenar «prune» -&gt; «clean»
- Mostra clarament els noms de fitxer duplicats a les etiquetes de carpeta

### BluezQt

- Actualitza els fitxers XML de D-Bus a usar «Out*», per a assenyalar les anotacions de tipus Qt
- Afegeix un senyal per canvis d'adreça de dispositius

### Icones Brisa

- Usa també les icones «broom-style» per a «edit-clear-all»
- Usa una icona «broom-style» per a «edit-clear-history»
- Canvia la icona «view-media-artist» de 24px

### Mòduls extres del CMake

- Android: Fa possible sobreescriure un directori objectiu d'APK
- Descarta el QT_USE_FAST_OPERATOR_PLUS obsolet
- Afegeix «-Wlogical-op» «-Wzero-as-null-pointer-constant» als avisos dels KF5
- [ECMGenerateHeaders] Afegeix una opció per extensions de fitxers de capçalera diferents de «.h»
- No inclou un 64 en construir arquitectures de 64 bits al Flatpak

### KActivitiesStats

- Soluciona un error a Cache::clear (error 396175)
- Esmena en moure l'element ResultModel (error 396102)

### KCompletion

- Eliminar la inclusió «moc» innecessària
- Assegura que s'emet KLineEdit::clearButtonClicked

### KCoreAddons

- Elimina les definicions QT duplicades de KDEFrameworkCompilerSettings
- Assegura que compila amb els indicadors de compilació estrictes
- Elimina la clau no usada «X-KDE-DBus-ModuleName» de les metadades del tipus de servei de proves

### KCrash

- Redueix QT_DISABLE_DEPRECATED_BEFORE a la profunditat mínima de les Qt
- Elimina les definicions QT duplicades de KDEFrameworkCompilerSettings
- Assegura que es construeix amb els indicadors de compilació estrictes

### KDeclarative

- Comprova que el node tingui realment una textura vàlida (error 395554)

### KDED

- Definició del tipus de servei del «KDEDModule»: elimina la clau «X-KDE-DBus-ModuleName» no usada

### Compatibilitat amb les KDELibs 4

- Defineix QT_USE_FAST_OPERATOR_PLUS
- No exporta «kf5-config» al fitxer de configuració del CMake
- Elimina l'entrada no usada «X-KDE-DBus-ModuleName» de les metadades del connector de «kded»

### KDE WebKit

- Adapta KRun::runUrl() i KRun::run() a una API no obsoleta
- Adapta KIO::Job::ui() -&gt; KIO::Job::uiDelegate()

### KFileMetaData

- Evita avisos del compilador de les capçaleres de la «taglib»
- PopplerExtractor: usa directament els arguments de QByteArray() en lloc de 0 apuntadors
- taglibextractor: Restaura l'extracció de les propietats de l'àudio sense que existeixin etiquetes
- OdfExtractor: treballa amb noms de prefixos no comuns
- No afegeix «-ltag» a la interfície pública d'enllaç
- Implementa l'etiqueta «lyrics» al «taglibextractor»
- Proves automàtiques: ja no incrusta EmbeddedImageData a la biblioteca
- Afegeix la capacitat de llegir fitxers incrustats de coberta
- Implementa la lectura de l'etiqueta «rating»
- Verifica la versió necessària de libavcode, libavformat i libavutil

### KGlobalAccel

- Actualitza el fitxer XML de D-Bus a usar «Out*», per a assenyalar les anotacions de tipus Qt

### KHolidays

- holidays/plan2/holiday_jp_* - afegeix metainformació que mancava
- Actualitza els festius japonesos (en japonès i anglès) (error 365241)
- holidays/plan2/holiday_th_en-gb - actualitzat per Tailàndia (anglès UK) per al 2018 (error 393770)
- Afegeix els festius de Veneçuela (espanyol) (error 334305)

### KI18n

- Al fitxer de macros de CMake usa «CMAKE_CURRENT_LIST_DIR» conseqüentment en lloc d'un ús barrejat amb «KF5I18n_DIR»
- KF5I18NMacros: No instal·la un directori buit quan no existeix cap fitxer «po»

### KIconThemes

- Permet triar fitxers «.ico» al seleccionador de fitxers d'icones personalitzades (error 233201)
- Permet «Icon Scale» a partir de l'especificació 0.13 de noms d'icones (error 365941)

### KIO

- Usa el nou mètode «fastInsert» a tots els llocs que sigui d'aplicació
- Restaura la compatibilitat d'UDSEntry::insert, afegint un mètode «fastInsert»
- Adapta KLineEdit::setClearButtonShown -&gt; QLineEdit::setClearButtonEnabled
- Actualitza el fitxer XML de D-Bus a usar «Out*», per a assenyalar les anotacions de tipus Qt
- Elimina la definició QT duplicada de KDEFrameworkCompilerSettings
- Usa una icona d'emblema correcta per als fitxers i carpetes de només lectura (error 360980)
- Fa possible una altra vegada anar a l'arrel, al giny de fitxers
- [Diàleg de propietats] Millora diverses cadenes relacionades amb permisos (error 96714)
- [KIO] Afegeix la implementació de XDG_TEMPLATES_DIR a KNewFileMenu (error 191632)
- Actualitza el «docbook» de la paperera a la 5.48
- [Diàleg de propietats] Fa seleccionables tots els valors de camps a la pestanya general (error 105692)
- Elimina l'entrada no usada «X-KDE-DBus-ModuleName» de les metadades dels connectors del «kded»
- Activa la comparació de KFileItems per URL
- [KFileItem] Comprova la majoria d'URL locals per si són compartits
- Esmena una regressió en enganxar dades binàries des del porta-retalls

### Kirigami

- Color més coherent en passar per sobre el ratolí
- No obre el submenú per accions sense fills
- Refactoritza el concepte de barra d'eines global (error 395455)
- Gestiona la propietat activada de models senzills
- Presenta ActionToolbar
- Esmena l'estirament per actualitzar
- Elimina els documents «doxygen» de la classe interna
- No enllaça contra «Qt5::DBus» quan està definit «DISABLE_DBUS»
- Sense marge extra per a «overlaysheets» a la superposició
- Esmena el menú per a les Qt 5.9
- Verifica també la propietat de visibilitat de l'acció
- Millor aspecte/alineació en el mode compacte
- No explora els connectors en cada creació de «platformTheme»
- Elimina el paràmetre «personalitzat»
- Afegeix reiniciadors per a tots els colors personalitzats
- Adapta l'acoloriment del «toolbutton» al «colorSet» personalitzat
- Presenta el conjunt de colors personalitzats
- «buddyFor» escrivible a les etiquetes de posició pel que fa als subelements
- En usar un color de fons diferent, usa el «highlightedText» com a color de text

### KNewStuff

- [KMoreTools] Activa la instal·lació d'eines via URL d'Appstream
- Elimina el pedaç de l'apuntador «d» a KNS::Engine

### KService

- Elimina la clau no usada «X-KDE-DBus-ModuleName» de les metadades del servei de proves

### KTextEditor

- Guarda «updateConfig» per a barres d'estat desactivades
- Afegeix un menú contextual a la barra d'estat per commutar la visualització del comptador total de línies/paraules
- Implementa la visualització del total de línies al Kate (error 387362)
- Fa que els botons dels menús orientables de la barra d'eines mostrin els seus menús amb un clic normal en lloc d'un clic i mantenir (error 353747)
- CVE-2018-10361: Escalada de privilegis
- Esmenar l'amplada del cursor (error 391518)

### KWayland

- [servidor] Envia un esdeveniment de marc en lloc d'una purga en moure un apuntador relatiu (error 395815)
- Esmena la prova de missatge emergent a XDGV6
- Esmena un error estúpid de copiar i enganxar al client de XDGShellV6
- No cancel·la la selecció antiga del porta-retalls si és la mateixa que la nova (error 395366)
- Respecta BUILD_TESTING
- Esmena diversos problemes d'ortografia suggerits per la nova eina «linter»
- Afegeix el fitxer «arclint» al «kwayland»
- Esmena «@since» per ometre la commutació d'API

### KWidgetsAddons

- [KMessageWidget] Actualitza el full d'estil quan canvia la paleta
- Actualitza les «kcharselect-data» a Unicode 11.0
- [KCharSelect] Adapta «generate-datafile.py» al Python 3
- [KCharSelect] Prepara les traduccions per a l'actualització de l'Unicode 11.0

### ModemManagerQt

- Implementa el funcionament de les interfícies Voice i Call
- No defineix regles personalitzades de filtre de domini

### Icones de l'Oxygen

- Mostra una icona per als fitxers ocults al Dolphin (error 395963)

### Frameworks del Plasma

- FrameSvg: Actualitza el marc de màscara si ha canviat el camí a la imatge
- FrameSvg: No elimina els marcs compartits de màscara
- FrameSvg: Simplifica «updateSizes»
- Icona per al Keyboard Indicator T9050
- Esmena el color de la icona «media»
- FrameSvg: Torna a crear la memòria cau de «maskFrame» ha canviat «enabledBorders» (error 391659)
- FrameSvg: Dibuixa les cantonades només si s'han activat les vores en ambdues direccions
- Ensenya a ContainmentInterface::processMimeData com gestionar les caigudes del gestor de tasques
- FrameSVG: Suprimeix verificacions redundants
- FrameSVG: Esmena la inclusió QObject
- Usa QDateTime per la interfície amb el QML (error 394423)

### Purpose

- Afegeix l'acció Comparteix al menú contextual del Dolphin
- Reinicia adequadament els connectors
- Filtra els connectors duplicats

### QQC2StyleBridge

- No hi ha valors de píxels a «checkindicator»
- Usa RadioIndicator per a tot
- No sobreposar els missatges emergents
- A les Qt&lt;5.11 s'ignora completament el control de la paleta
- Fons del botó d'àncora

### Solid

- Esmena l'etiqueta de dispositiu amb una mida desconeguda

### Ressaltat de la sintaxi

- Esmena dels comentaris del Java
- Ressalta els fitxers Gradle també amb la sintaxi del Groovy
- CMake: Esmena el ressaltat després de cadenes amb un únic símbol <code>@</code>
- CoffeeScript: afegeix l'extensió «.cson»
- Rust: afegeix paraules clau i altres, esmena els identificadors, i altres millores/esmenes
- Awk: esmena l'expressió regular a una funció i actualitza la sintaxi per al «gawk»
- Pony: esmena l'escapada d'una cometa única i un possible bucle infinit amb #
- Actualitza la sintaxi del CMake per a la propera publicació 3.12

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
