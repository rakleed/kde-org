---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: soluciona l'ordre de creació de l'objecte QCoreApplication (error 378539)

### Icones Brisa

- Afegeix icones per al Hotspot (https://github.com/KDAB/hotspot)
- Icones millors dels sistemes de control de versions (error 377380)
- Afegeix icona del Plasmate (error 376780)
- Actualitza les icones del «microphone-sensitivity» (error 377012)
- Eleva les icones predeterminades per al «Panel» a 48

### Mòduls extres del CMake

- Sanejadors: no usa els indicadors similars a GCC per exemple amb MSVC
- KDEPackageAppTemplates: millores a la documentació
- KDECompilerSettings: passa -Wvla i -Wdate-time
- Admet versions antigues del «qmlplugindump»
- Presenta «ecm_generate_qmltypes»
- Permet que els projectes incloguin dues vegades un fitxer
- Soluciona els «rx» que coincideixen amb els noms del projecte fora de l'URI del «git»
- Presenta l'ordre de construcció de «fetch-translations»
- Usa més «-Wno-gnu-zero-variadic-macro-arguments»

### KActivities

- Només s'usen Frameworks de nivell 1, així que es converteix a un de nivell 2
- Elimina el KIO de les dependències

### KAuth

- Esmena de seguretat: verifica que qualsevol que ens cridi sigui realment qui diu que és

### KConfig

- Soluciona el càlcul del «relativePath» a KDesktopFile::locateLocal() (error 345100)

### KConfigWidgets

- Estableix la icona per l'acció Donatius
- Relaxa les restriccions per processar QGroupBoxes

### KDeclarative

- No estableix ItemHasContents a la DropArea
- No accepta esdeveniments de passar-hi per sobre a la «DragArea»

### KDocTools

- Solució temporal per a la càrrega del MSVC i del catàleg
- Soluciona un conflicte de visibilitat per al meinproc5 (error 379142)
- Posa entre cometes altres variables amb camí (evita problemes amb els espais)
- Posa entre cometes les variables amb camí (evita problemes amb els espais)
- Desactiva temporalment la documentació local del Windows
- FindDocBookXML4.cmake, FindDocBookXSL.cmake - cerca en les instal·lacions casolanes

### KFileMetaData

- Fa opcional el KArchive i no construeix els extractors que el necessitin
- Soluciona l'error de compilació de símbols duplicats amb el «mingw» al Windows

### KGlobalAccel

- Construcció: Elimina la dependència del KService

### KI18n

- Esmena la gestió dels noms base dels fitxers «po» (error 379116)
- Soluciona el «ki18n» durant l'arrencada

### KIconThemes

- No intenta crear icones de mida buida

### KIO

- KDirSortFilterProxyModel: retorna l'ordenació natural (error 343452)
- Omple UDS_CREATION_TIME amb el valor de «st_birthtime» al FreeBSD
- Esclau HTTP: envia una pàgina d'error després de fallar a l'autorització (error 373323)
- Kioexec: actualitza el delegat a un mòdul «kded» (error 370532)
- Soluciona l'esquema d'URL de la definició de la prova IGU del KDirlister dues vegades
- Suprimeix els mòduls «kiod» en sortir
- Genera un fitxer moc_predefs.h per al «KIOCore» (error 371721)
- Kioexec: soluciona la implementació de «--suggestedfilename»

### KNewStuff

- Permet categories múltiples amb el mateix nom
- KNewStuff: mostra la informació de la mida del fitxer en el delegat de la quadrícula
- Si es coneix la mida d'una entrada, la mostra a la vista de la llista
- Registra i declara KNSCore::EntryInternal::List com a tipus MIME
- No fracassa per la commutació. No a les entrades dobles
- Tanca sempre el fitxer baixat després de la baixada

### Framework del KPackage

- Soluciona el camí de les inclusions a KF5PackageMacros.cmake
- Ignora els avisos durant la generació de les «appdata» (error 378529)

### KRunner

- Plantilles: canvia la categoria de les plantilles de nivell superior a «Plasma»

### KTextEditor

- Integració del KAuth en desar els documents - vol. 2
- Esmena una declaració en aplicar el plegat de codi que canvia la posició del cursor
- Usa un element arrel no obsolet de la &lt;igu&gt; al fitxer «ui.rc»
- Afegeix marques de barra de desplaçament també a la cerca i substitució integrada
- Integració del KAuth en desar els documents

### KWayland

- La validació de la superfície és vàlida quan l'enviament del TextInput deixa un esdeveniment

### KWidgetsAddons

- KNewPasswordWidget: no oculta la visibilitat de l'acció en el mode de text net (error 378276)
- KPasswordDialog: no oculta la visibilitat de l'acció en el mode de text net (error 378276)
- Esmena KActionSelectorPrivate::insertionIndex()

### KXMLGUI

- El «kcm_useraccount» està mort, llarga vida a l'«user_manager»
- Construccions reproduïbles: elimina la versió de XMLGUI_COMPILING_OS
- Esmena: el nom del DOCTYPE cal que coincideixi amb el tipus d'element arrel
- Soluciona l'ús incorrecte d'«ANY» al kpartgui.dtd
- Usa un element arrel no obsolet de la &lt;igu&gt;
- Esmena la «dox» de l'API: substitueix 0 per «nullptr» o l'elimina quan no s'aplica

### NetworkManagerQt

- Esmena una fallada en recuperar la llista de connexions actives (error 373993)
- Defineix un valor predeterminat per a la negociació automàtica basat en la versió del NM que s'executa

### Icones de l'Oxygen

- Afegeix una icona per al Hotspot (https://github.com/KDAB/hotspot)
- Eleva les icones predeterminades per al «Panel» a 48

### Frameworks del Plasma

- Torna a carregar la icona quan canviï «usesPlasmaTheme»
- Instal·la els Plasma Components 3 perquè es puguin usar
- Introdueix «units.iconSizeHints.*» per proporcionar consells de mida d'icona configurables per l'usuari (error 378443)
- [TextFieldStyle] Esmena «textField» no és un error definit
- Actualitza el pedaç «ungrabMouse» per a les Qt 5.8
- Es protegeix contra la miniaplicació que no carrega AppletInterface (error 377050)
- Calendari: usa l'idioma correcte per als noms dels mesos i dels dies
- Genera els fitxers plugins.qmltypes per als connectors que instal·lem
- Si l'usuari ha definit una mida implícita, mantenir-la

### Solid

- Afegeix una inclusió que es necessita al «msys2»

### Ressaltat de la sintaxi

- Afegeix l'extensió per l'Arduino
- LaTeX: esmena la terminació incorrecta dels comentaris «iffalse» (error 378487)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
