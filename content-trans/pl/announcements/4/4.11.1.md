---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE wydało Przestrzenie Pracy Plazmy, Aplikacje i Platformę 4.11.1.
title: KDE wydało wrześniowe uaktualnienia dla Przestrzeni Roboczych Plazmy, Aplikacji
  i Platformy
---
5 wrzesień 2013. Dzisiaj KDE wydało uaktualnienia dla swoich Przestrzeni Roboczych, Aplikacji i Platformy Programistycznej. Uaktualnienia te są pierwszym w szeregu miesięcznych uaktualnień stabilizacyjnych dla serii 4.11. Jak to zostało ogłoszone w wydaniu, przestrzenie robocze będą otrzymywały uaktualnienia przed dwa następne lata. Jako iż wydanie to zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń, będzie to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 70 zarejestrowanych błędów uwzględnia ulepszenia do zarządcy okien KWin, zarządcy plików Dolphin i innych. Użytkownicy mogą się spodziewać, że Pulpit Plazmy będzie szybciej się uruchamiał, Dolphin będzie łagodniej przewijał, a rozmaite programy i narzędzia będą wykorzystywały mniej pamięci. Ulepszenia uwzględniają powrót przeciągnij-upuść z paska zadań do pagera, poprawki podświetlania i kolorów w Kate i WIELE małych błędów zmiażdżonych w grze Kmahjongg. Napisano wiele poprawek zwiększających stabilność i jak zazwyczaj dodano tłumaczenia.

Bardziej zupełny <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>wykaz</a> zmian można znaleźć w programie śledzącym wydania KDE. Szczegółowy wykaz zmian, które trafiły do wydania 4.11.1, można znaleźć przeglądając dzienniki git.

Aby pobrać kod źródłowy lub pakiety do zainstalowania przejdź do <a href='/info/4/4.11.1'>Strony informacyjnej 4.11.1</a>. Jeśli chcesz wiedzieć więcej o wersji 4.11 Przestrzeni Roboczych KDE, Aplikacji i Platformy Programistycznej, zajrzyj do <a href='/announcements/4.11/'>uwag o wydaniu 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Nowy przepływ pracy wyślij-później w Kontact` width="600px">}}

Oprogramowanie KDE, włączając w to wszystkie biblioteki i aplikacje, jest dostępne za darmo na warunkach licencji Wolnego Oprogramowania. Oprogramowanie KDE można otrzymać jako kod źródłowy i w postaci różnych formatów binarnych z <a href='http://download.kde.org/stable/4.11.1/'> download.kde.org</a> lub istniejących obecnie  <a href='/distributions'>znaczących  systemach GNU/Linuks oraz UNIX</a>.
