---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE wydało Aplikacje KDE 17.04.2
layout: application
title: KDE wydało Aplikacje KDE 17.04.2
version: 17.04.2
---
8 czerwca 2017. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../17.04.0'>Aplikacji KDE 17.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than 15 recorded bugfixes include improvements to kdepim, ark, dolphin, gwenview, kdenlive, among others.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.33.
