---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE wydało Aplikacje KDE 16.12.1
layout: application
title: KDE wydało Aplikacje KDE 16.12.1
version: 16.12.1
---
12 stycznia 2017. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../16.12.0'>Aplikacji KDE 16.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

This release fixes a DATA LOSS bug in the iCal resource which failed to create std.ics if it didn't exist.

Więcej niż 40 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, ark, gwenview, kajongg, okular, kate, kdenlive oraz innych

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.28.
