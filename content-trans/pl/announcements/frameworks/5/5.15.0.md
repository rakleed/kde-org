---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Naprawiono obsługę granicy/przesunięcia w SearchStore::exec
- Utwórz ponownie indeks baloo
- ustawienia balooctl: dodano opcję do ustawiania/widoku onlyBasicIndexing
- Przeniesiono sprawdzenie balooctl, tak aby działało z nową architekturą (błąd 353011)
- FileContentIndexer: naprawiono dwukrotne emitowanie filePath
- UnindexedFileIterator: mtime jest quint32, a nie quint64
- Transakcja: naprawiono kolejną literówkę Dbi
- Transakcja: Naprawiono documentMTime() oraz documentCTime() które wyświetlały złe Dbis.
- Transaction::checkPostingDbInTermsDb: Optymalizacja kodu
- Usunięto ostrzeżenia dbus
- Balooctl: Dodano polecenie checkDb
- ustawienia balooctl: Dodano "filtr wykluczający"
- KF5Baloo: Make sure D-Bus interfaces are generated before they are used. (bug 353308)
- Avoid using QByteArray::fromRawData
- Usunięto baloo-monitor z baloo
- TagListJob: Wyemituj błąd, gdy nie uda się otworzyć bazy danych
- Nie pomijaj podwarunków, jeśli ich nie znaleziono
- Czystszy kod dla nieudanego uruchomienia Baloo::File::load() przy niepowodzeniu przy otwieraniu bazy danych.
- balooctl używa IndexerConfig zamiast bezpośredniej manipulacji baloofilerc
- Ulepszono i18n dla balooshow
- balooshow godnie się zawiesza, gdy nie można otworzyć bazy danych.
- Zwróć błąd w Baloo::File::load() jeśli nie otwarto bazy danych. (błąd 353049)
- IndexerConfig: dodaj metodę refresh()
- inotify: Do not simulate a closedWrite event after move without cookie
- ExtractorProcess: Remove the extra n at the end of the filePath
- baloo_file_extractor: wywołaj QProcess::close przed zniszczeniem QProcess
- baloomonitorplugin/balooctl: przetłumacz na i18n stan programu indeksującego.
- BalooCtl: Dodaj opcję 'config'
- Uczyń baloosearch ładniejszym
- Usuń puste pliki EventMonitor
- BalooShow: Pokaż więcej informacji, gdy identyfikatory nie pasują do siebie
- BalooShow: Gdy wywołany ze sprawdzeniem id, to sprawdź czy id jest poprawne
- Dodano klasę FileInfo
- Dodano sprawdzenia w kilku miejscach, aby Baloo nie ulegał usterką gdy wyłączony. (błąd 352454)
- Naprawiono Baloo, który nie zważał na ustawienie "tylko podstawowe indeksowanie"
- Monitor: Fetch remaining time on startup
- Use actual method calls in MainAdaptor instead of QMetaObject::invokeMethod
- Dodano interfejs org.kde.baloo to głównego obiektu dla wstecznej zgodności
- Naprawiono wyświetlanie ciągu znaków daty na pasku adresu ze względu na przeniesienie QDate
- Dodano opóźnienie po każdym pliku zamiast po każdym wsadzie
- Usunięto zależność Qt::Widgets z baloo_file
- Usunięto nieużywany kod z baloo_file_extractor
- Dodano monitor baloo lub eksperymentalną wtyczkę qml
- Make "querying for remaining time" thread safe
- kioslaves: Dodano brakujące zastąpienie dla funkcji wirtualnych
- Extractor: Set the applicationData after constructing the app
- Query: Zaimplementowano wsparcie dla 'offset'
- Balooctl: Dodano --version oraz --help (błąd 351645)
- Usunięto obsługę KAuth, aby zwiększyć maksymalną liczbę obserwatorów inotify, jeśli ich liczba jest zbyt mała (błąd 351602)

### BluezQt

- Naprawiono usterkę fakebluez w obexmanagertest z ASAN
- Forward declare all exported classes in types.h
- ObexTransfer: zgłaszaj błąd, gdy usunięto sesję przesyłania
- Utils: Hold pointers to managers instances
- ObexTransfer: Set error when org.bluez.obex crashes

### Dodatkowe moduły CMake

- Pamięć podręczna ikon GTK będzie uaktualniana po zainstalowaniu ikon
- Usunięto ominięcie błędu polegające na opóźnieniu wykonywania na Androidzie
- ECMEnableSanitizers: The undefined sanitizer is supported by gcc 4.9
- Disable X11,XCB etc. detection on OS X
- Szukaj plików raczej w prefiksie instalacji, a nie ścieżce prefiksa
- Użyto Qt5 do określenia prefiksu instalacyjnego Qt5
- Dodano definicję ANDROID jako iż jest potrzebna w qsystemdetection.h.

### Integracja Szkieletów

- Naprawiono nie pokazywanie problemu przez okno dialogowe dla losowego pliku. (błąd 350758)

### KAktywności

- Using a custom matching function instead of sqlite's glob. (bug 352574)
- Naprawiono problem z dodawaniem nowego zasobu do modelu

### KKodeki

- Naprawiono usterkę w UnicodeGroupProber::HandleData przy używaniu krótkich ciągów znaków

### KConfig

- Mark kconfig-compiler as non-gui tool

### KCoreAddons

- KShell::splitArgs: only ASCII space is a separator, not unicode space U+3000 (bug 345140)
- KDirWatch: Naprawiono usterkę, gdy globalny statyczny destruktor używa KDirWatch::self() (błąd 353080)
- Usunięto usterkę przy używaniu KDirWatch w Q_GLOBAL_STATIC.
- KDirWatch: przywrócono bezpieczeństwo wątku
- Clarify how to set KAboutData constructor arguments.

### KCrash

- KCrash: pass cwd to kdeinit when auto-restarting the app via kdeinit. (bug 337760)
- Dodano KCrash::initialize() tak więc aplikacje i wtyczki platformy mogą jednoznacznie włączyć KCrash.
- Disable ASAN if enabled

### KDeclarative

- Small improvements in ColumnProxyModel
- Make it possible for applications to know path to homeDir
- move EventForge from the desktop containment
- Provide enabled property for QIconItem.

### KDED

- kded: simplify logic around sycoca; just call ensureCacheValid.

### Obsługa KDELibs 4

- Call newInstance from the child on first invocation
- Use kdewin defines.
- Don't try to find X11 on WIN32
- cmake: Naprawiono sprawdzenie wersji taglib w FindTaglib.cmake.

### KDesignerPlugin

- Qt moc can't handle macros (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- zaimplementowano okna przy użyciu metadanych użytkownika

### Dodatkowy interfejs użytkownika dla KDE

- Not looking for X11/XCB makes sense also for WIN32

### KHTML

- Replace std::auto_ptr with std::unique_ptr
- khtml-filter: Discard rules containing special adblock features that we do not handle yet.
- khtml-filter: Code reorder, no functional changes.
- khtml-filter: Ignore regexp with options as we do not support them.
- khtml-filter: Naprawiono wykrywanie znaku rozdzielającego w adblocku.
- khtml-filter: Clean up from trailing white spaces.
- khtml-filter: Do not discard lines starting with '&amp;' as it is not a special adblock char.

### KI18n

- usunięto surowe iteratory dla msvc, aby móc zbudować ki18n

### KIO

- KFileWidget: parent argument should default to 0 like in all widgets.
- Make sure the size of the byte array we just dumped into the struct is big enough before calculating the targetInfo, otherwise we're accessing memory that doesn't belong to us
- Naprawiono użycie Qurl przy wywoływaniu QFileDialog::getExistingDirectory()
- Refresh Solid's device list before querying in kio_trash
- Dopuszczaj zapis trash: poza trash:/ jako adres url dla listDir (wywołuje listRoot) (błąd 353181)
- KProtocolManager: naprawiono zawieszenie przy używaniu EnvVarProxy. (błąd 350890)
- Don't try to find X11 on WIN32
- KBuildSycocaProgressDialog: use Qt's builtin busy indicator. (bug 158672)
- KBuildSycocaProgressDialog: run kbuildsycoca5 with QProcess.
- KPropertiesDialog: poprawiono ~/.local które było dowiązaniem symbolicznym, porównywanie ścieżek kanonicznych
- Dodano obsługę dla udziałów sieciowych w kio_trash (błąd 177023)
- Connect to the signals of QDialogButtonBox, not QDialog (bug 352770)
- Cookies KCM: update DBus names for kded5
- Używaj plików JSON bezpośrednio zamiast kcoreaddons_desktop_to_json()

### KNotification

- Don't send notification update signal twice
- Przetwórz ustawienia powiadomień tylko gdy uległy zmianie
- Don't try to find X11 on WIN32

### KNotifyConfig

- Change method for loading defaults
- Send the appname whose config was updated along with the DBus signal
- Dodano metodę przywracającą kconfigwidget do ustawień domyślnych
- Don't sync the config n times when saving

### KService

- Use largest timestamp in subdirectory as resource directory timestamp.
- KSycoca: zachowaj mtime dla każdego katalogu źródłowego po to aby wykryć zmiany. (błąd 353036)
- KServiceTypeProfile: usunięto niepotrzebne tworzenie wytwórni. (błąd 353360)
- Simplify and speed up KServiceTest::initTestCase.
- make install name of applications.menu file a cached cmake variable
- KSycoca: ensureCacheValid() powinien utworzyć bazę danych, jeśli nie istnieje
- KSycoca: globalna baza danych powinna działać po ostatnim kodzie sprawdzającym znaczniki czasu
- KSycoca: zmiana nazwy bazy danych na taką, która uwzględnia język i sumę sha1 katalogów, z których została stworzona.
- KSycoca: ensureCacheValid() stało się częścią publicznego API.
- KSycoca: dodano wskaźnik q, aby obejść się bez wykorzystania singleton
- KSycoca: usunięto wszystkie metody self() dla wytwórni, zamiast tego przechowywane są w KSycoca.
- KBuildSycoca: usunięto zapisywanie pliku ksycoca5stamp.
- KBuildSycoca: use qCWarning rather than fprintf(stderr, ...) or qWarning
- KSycoca: przebudowa ksycoca powinna odbywać się w procesie, a nie poprzez wykonanie kbuildsycoca5
- KSycoca: przeniesiono cały kod kbuildsycoca do lib, z wykluczeniem main().
- optymalizacja KSycoca: obserwuj tylko plik, jeśli aplikacja połączy się do databaseChanged()
- Naprawiono wycieki pamięci w klasie KBuildSycoca
- KSycoca: zastąpiono powiadomienie szyny DBus obserwacją pliku przy użyciu KDirWatch.
- kbuildsycoca: deprecate option --nosignal.
- KBuildSycoca: replace dbus-based locking with a lock file.
- Do not crash when encountering invalid plugin info.
- Rename headers to _p.h in preparation for move to kservice library.
- Move checkGlobalHeader() within KBuildSycoca::recreate().
- Usunięto kod dla --checkstamps oraz --nocheckfiles.

### KTextEditor

- validate more regexp
- naprawiono wyrażenie regularne w plikach HL (błąd 352662)
- synchronizacja ocaml HL ze stanem https://code.google.com/p/vincent-hugot-projects/ zanim google code zostanie wyłączony, kilka małych poprawek błędów
- dodano podział słowa (błąd 352258)
- validate line before calling folding stuff (bug 339894)
- Naprawiono zliczanie słów w Kate, które słuchało DocumentPrivate zamiast Document (błąd 353258)
- Uaktualniono podświetlanie składni Kconfig: dodano nowe operatory z Linux 4.2
- sync w/ KDE/4.14 kate branch
- minimapa: Uchwyt paska przewijania od teraz jest rysowany, gdy określono, aby nie rysować jego strzałek na skrajach. (błąd352641)
- składnia: Dodano opcję git-user dla kdesrc-buildrc

### Szkielet Portfela

- No longer automatically close on last use

### KWidgetsAddons

- Naprawiono ostrzeżenie C4138 (MSVC): znaleziono '*/' poza komentarzem

### KWindowSystem

- Perform deep copy of QByteArray get_stringlist_reply
- Allow interacting with multiple X servers in the NETWM classes.
- [xcb] Consider mods in KKeyServer as initialized on platform != x11
- Change KKeyserver (x11) to categorized logging

### KXMLGUI

- Make it possible to import/export shortcut schemes symmetrically

### ZarządzanieSieciąQt

- Naprawiono introspekcje, LastSeen powinien być używany w  AccessPoint, a nie w ActiveConnection

### Szkielety Plazmy

- Make tooltip dialog hidden on the cursor entering the inactive ToolTipArea
- if the desktop file has Icon=/foo.svgz use that file from package
- dodano rodzaj pliku "zrzut ekranu" w pakietach
- consider devicepixelration in standalone scrollbar
- no hover effect on touchscreen+mobile
- Use lineedit svg margins in sizeHint calculation
- Don't fade animate icon in plasma tooltips
- Naprawiono pomijanie tekstu przycisku
- Context menus of applets within a panel no longer overlap the applet
- Simplify getting associated apps list in AssociatedApplicationManager

### Sonnet

- Naprawiono poprawne wczytywanie identyfikatora wtyczki hunspell 
- wsparcie dla statycznej kompilacji na windowsie, dodano ścieżkę słownika libreoffice hunspell z windowsa
- Do not assume UTF-8 encoded Hunspell dictionaries. (bug 353133)
- Naprawiono Highlighter::setCurrentLanguage() dla przypadku, w którym poprzedni język był nieprawidłowy (błąd 349151)
- support /usr/share/hunspell as dict location
- NSSpellChecker-based plugin

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
