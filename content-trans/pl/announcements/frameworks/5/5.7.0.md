---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Ogólne

- Kilka poprawek do kompilowania z nadchodzącym Qt 5.5

### KAktywności

- Naprawiono rozpoczynanie i zatrzymywanie aktywności
- Naprawiono podgląd aktywności, który wyświetlał czasami nie tę tapetę

### KArchiwum

- Pliki tymczasowe będą tworzone w katalogu tymczasowym, a nie w bieżącym katalogu

### KAuth

- Naprawiono generowanie plików usługi pomocniczej KAuth DBus 

### KCMUtils

- Naprawiono assert gdy ścieżki dbus zawierały znak '.'

### KKodeki

- Dodano obsługę CP949 do KCharsets

### KConfig

- kconf_update już nie przetwarza pliku *.upd z KDE SC 4. Dodano "Version=5" na początku pliku upd dla uaktualnień wykonywanych na aplikacjach Qt5/KF5
- Naprawiono KCoreConfigSkeleton przy przełączaniu wartości, gdy zapis następował pomiędzy przełączeniem

### KConfigWidgets

- KRecentFilesAction: naprawiono porządek wpisów menu (tak, że odpowiada on porządkowi kdelibs4)

### KCoreAddons

- KAboutData: Samoczynne wywoływanie addHelpOption oraz addVersionOption dla wygody i spójności
- KAboutData: Przywrócenie "Proszę używać http://bugs.kde.org do zgłaszania błędów." gdy nie ustawiono innego adresu pocztowego/url
- KAutoSaveFile: allStaleFiles() teraz działa zgodnie z oczekiwaniami dla plików lokalnych, staleFiles() także zostało naprawione
- KRandomSequence od teraz używa liczb całkowitych wewnętrznie i udostępnia int-api dla 64-bitowej jednoznaczności
- Definicje typów mime: pliki *.qmltypes oraz *.qmlproject także mają typ mim text/x-qml
- KShell: sprawiono, że quoteArgs cytuje adresy url przy użyciu QChar::isSpace(), niezwykłe znaki odstępu nie były obsługiwane we właściwy sposób
- KSharedDataCache: napraw tworzenie katalogów zawierających pamięć podręczną (błąd przenoszenia)

### KDBusAddons

- Dodano metodę pomocniczą KDEDModule::moduleForMessage do pisania usług podobnych do kded, takich jak np. kiod

### KDeclarative

- Dodano składnik rysowania
- Dodano metodę przeładowywania Formats::formatDuration, która przyjmuje liczby całkowite
- Dodano nowe właściwości: paintedWidth oraz paintedHeight do QPixmapItem oraz QImageItem
- Naprawiono malowanie QImageItem oraz QPixmapItem

### Kded

- Dodano obsługę wczytywania modułów kded przy użyciu metadanych JSON

### KGlobalAccel

- Teraz zawiera składniki bibliotek uruchomieniowych, czyniąc z niego Szkielet poziomu 3
- Przywrócono działanie silnika dla MS Windows
- Przywrócono działanie silnika dla Maca
- Naprawiono awarię KGlobalAccel przy zamykaniu biblioteki uruchomieniowej X11

### KI18n

- Oznaczono wyniki jako wymagane, aby ostrzec, gdy API jest nadużywane
- Dodano opcję systemu budowania BUILD_WITH_QTSCRIPT, aby umożliwić użycie ograniczonego zestawu funkcji na systemach osadzonych

### KInit

- OSX: wczytaj poprawne biblioteki współdzielone w czasie uruchomienia
- Poprawki w kompilacji Mingw

### KIO

- Naprawiono awarię w zadaniach przy dowiązywaniu do KIOWidgets, lecz tylko przy używaniu QCoreApplication
- Naprawiono edytowanie skrótów sieciowych
- Dodano opcję KIOCORE_ONLY do kompilowania tylko KIOCore oraz jego programów pomocniczych, lecz nie KIOWidgets czy KIOFileWidgets, zmniejszając przez to znacząco liczbę wymaganych zależności
- Dodano klasę KFileCopyToMenu, która dodaje Kopiuj do / Przenieś do do menu podręcznych
- Protokoły z włączonym SSL: dodano obsługę dla protokołów TLSv1.1 oraz TLSv1.2, usunięto SSLv3
- Naprawiono negotiatedSslVersion oraz negotiatedSslVersionName, tak aby zwracała wynegocjowany protokół
- Zastosuj wprowadzony adres URL do widoku przy naciśnięciu przycisku, który przełącza nawigatora adresu URL z powrotem do trybu okruchów
- Naprawiono dwa paski postępu/okna dialogowe wyświetlające się dla zadań kopiowania/przenoszenia
- KIO od teraz używa swojej własnej usługi, kiod, dla usług wykonywanych poza-procesem, które poprzednio działały z kded, celem zmniejszenia zależności; obecnie zmiana zastępuje jedynie kssld
- Naprawiono błąd "Nie można zapisać do &lt;ścieżki&gt;" przy wyzwoleniu kioexec
- Pozbyto się ostrzeżeń "QFileInfo::absolutePath: Skonstruowane z pustą nazwą" przy używaniu KFilePlacesModel

### KItemModels

- Naprawiono KRecursiveFilterProxyModel dla Qt 5.5.0+, poprzez fakt, że QSortFilterProxyModel teraz używa parametru rola da sygnału dataChanged

### KNewStuff

- Zawsze wczytuj dane xml na nowo dla zdalnych adresów url

### KNotifications

- Dokumentacja: wspomniano o wymaganiu nazw plików dla plików .notifyrc
- Naprawiono zwisający wskaźnik do KNotification
- Naprawiono wyciek w knotifyconfig
- Brakujące nagłówki knotifyconfig zostaną dograne

### KPackage

- Zmieniono nazwę kpackagetool man na kpackagetool5
- Naprawiono wgrywanie na systemach plików, gdzie ważna jest wielkość liter

### Kross

- Naprawiono Kross::MetaFunction, tak aby działał z systemem meta-obiektów Qt5

### KService

- Dołącz nieznane właściwości przy przekształcaniu KPluginInfo z KService
- KPluginInfo: naprawiono niekopiowanie właściwości z KService::Ptr
- OS X: poprawka zwiększająca wydajność dla kbuildsycoca4 (pomijanie pęków aplikacji)

### KTextEditor

- Naprawiono przewijanie na gładzikach o wysokiej czułości
- Nie emituj documentUrlChanged przy ponownym wczytywaniu
- Nie zmieniaj położenia wskaźnika w wierszach z tabulatorami przy ponownym wczytywaniu dokumentu
- Nie rozwijaj(zwijaj) pierwszego wiersza, jeśli został ręcznie (zwinięty) rozwinięty
- vimode: strzałki służą do przeglądania historii poleceń
- Przy otrzymaniu sygnału KDirWatch::deleted() nie będzie próby utworzenia digest
- Wydajność: usunięto globalne inicjacje

### KUnitConversion

- Naprawiono nieskończoną rekursywność w Unit::setUnitMultiplier

### Portfel

- Portfele ECB zostaną samoczynnie wykryte i przekształcone na portfele CBC
- Naprawiono algorytm szyfrowania CBC
- Zadbano o to, aby wykaz portfeli został uaktualniony, po usunięciu pliku portfeli z dysku
- Usunięto rozjechanie &lt;/p&gt; w tekście widocznym dla użytkownika

### KWidgetsAddons

- Użyto kstyleextensions do określania własnych elementów sterujących do wyświetlania paska kcapacity, gdy jest obsługiwany, umożliwia to nadawanie poprawnego wyglądu elementom interfejsu
- Zapewniono nazwę z dostępem dla KLed

### KWindowSystem

- Naprawiono NETRootInfo::setShowingDesktop(bool), które nie działało na Openbox
- Dodano wygodną metodę  KWindowSystem::setShowingDesktop(bool)
- Poprawki w obsłudze formatu ikon
- Dodano metodę NETWinInfo::icccmIconPixmap, która dostarcza map pikselowych ikon z własności WM_HINTS
- Dodano przeładowanie do KWindowSystem::icon, co zmniejsza wycieczki na około do X-Server
- Dodano obsługę dla _NET_WM_OPAQUE_REGION

### ZarządzanieSieciąQt

- Nie wyświetlaj wiadomości o nieobsługiwanej właściwości "PunktyDostępowe"
- Dodano obsługę dla ZarządzaniaSiecią 1.0.0 (niewymagane)
- Naprawiono obsługę danych poufnych VpnSetting
- Dodano klasę GenericSetting dla połączeń niezarządzanych przez ZarządzanieSiecią
- Dodano własność AutoconnectPriority do ConnectionSettings

#### Szkielety Plazmy

- Naprawiono błędnie otwierane uszkodzonego menu podręcznego przy naciskaniu środkowym przyciskiem myszy na oknie wysuwnym Plazmy
- Wywołaj przełączenie przycisku po obróceniu rolką myszy
- Okno dialogowe nigdy nie zmieni rozmiaru na większy niż ekran
- Panele będą przywracane gdy aplet zostanie przywrócony
- Naprawiono skróty klawiszowe
- Przywrócono obsługę hint-apply-color-scheme
- Ustawienia zostaną wczytane ponownie, gdy zmianie ulega plasmarc
- ...

### Solid

- Dodano energyFull oraz energyFullDesign do baterii

### Zmiany w systemie budowania (extra-cmake-modules)

- Nowy moduł ECMUninstallTarget do tworzenia i odinstalowywania celu
- KDECMakeSettings domyślnie importuje ECMUninstallTarget
- KDEInstallDirs: ostrzeżenie o mieszaniu względnych i bezwzględnych ścieżek instalacyjnych w wierszu poleceń
- Dodano moduł ECMAddAppIcon, aby dodać ikony do wykonywalnych plików docelowych na MS Windows oraz Mac OS X
- Naprawiono ostrzeżenie CMP0053 w CMake 3.1
- Nie usuwaj ustawienia zmiennych pamięci podręcznej w KDEInstallDirs

### Integracja Szkieletów

- Naprawiono uaktualnianie ustawienia jednokrotnego naciśnięcia klawiszem myszy w czasie uruchomienia
- Wiele poprawek w integracji z tacką systemową
- Nakładanie schematu kolorystycznego tylko na elementy interfejsu z poziomu szczytowego (naprawia QQuickWidgets)
- Uaktualniono ustawienia XCursor na platformie X11

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
