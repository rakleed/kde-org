---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE, KDE Uygulamaları 18.08.2'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 18.08.2'yi Gönderdi
version: 18.08.2
---
October 11, 2018. Today KDE released the second stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than a dozen recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, KCalc, Umbrello, among others.

İyileştirmeler şunları içerir:

- Dolphin'de bir dosyayı sürüklemek artık yanlışlıkla satır içi yeniden adlandırmayı tetikleyemez
- KCalc, ondalık sayıları girerken yine hem 'nokta' hem de 'virgül' tuşlarına izin verir
- KDE'nin kart oyunları için Paris kart destesindeki görsel bir aksaklık giderildi
