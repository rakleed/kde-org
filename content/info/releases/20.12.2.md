---
aliases:
- /info/releases-20.12.2
announcement: /announcements/releases/2021-02-apps-update
date: 2021-02-04
title: "20.12.2 Releases Source Info Page"
layout: releases
signer: Heiko Becker
signing_fingerprint: D81C0CB38EB725EF6691C385BB463350D6EF31EF
---
