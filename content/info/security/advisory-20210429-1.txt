KDE Project Security Advisory
=============================

Title:          KMail: Deleting attachments can disclose content of encrypted messages
Risk Rating:    Low
CVE:            CVE-2021-31855
Versions:       KMail, messagelib < 5.17.1
Author:         Ingo Klöcker <kloecker@kde.org>
Date:           29 April 2021

Overview
========

Deleting an attachment of a decrypted encrypted message stored on a remote server
(e.g. an IMAP server) causes KMail to upload the decrypted content of the message
to the remote server. This is not easily noticeable by the user because KMail does
not display the decrypted content.

Impact
======

With a specially crafted message a user could be tricked into decrypting an encrypted
message and then deleting an attachment attached to this message. If the attacker has
access to the messages stored on the email server, then the attacker could read the
decrypted content of the encrypted message.

Workaround
==========

Do not delete attachments of encrypted messages.

Solution
========

Update to messagelib >= 5.17.1 (when released)

Or apply this patch:
https://commits.kde.org/messagelib/3b5b171e91ce78b966c98b1292a1bcbc8d984799

Credits
=======

Thanks to Ingo Klöcker for reporting and fixing the issue.
