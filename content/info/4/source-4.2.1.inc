<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdeaccessibility-4.2.1.tar.bz2">kdeaccessibility-4.2.1</a></td><td align="right">6.3MB</td><td><tt>0ab97ffd3cbddc881b963ed4203045264132f8bc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdeadmin-4.2.1.tar.bz2">kdeadmin-4.2.1</a></td><td align="right">1.9MB</td><td><tt>888203103fe86010461b1e38d51ba9a20f3250e8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdeartwork-4.2.1.tar.bz2">kdeartwork-4.2.1</a></td><td align="right">22MB</td><td><tt>02bd99ca5cf303bdeb991b3e85b45dfc4e69e0bc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdebase-4.2.1.tar.bz2">kdebase-4.2.1</a></td><td align="right">4.1MB</td><td><tt>c500024294a7621d176d26bdabdd138d18ec827d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdebase-runtime-4.2.1.tar.bz2">kdebase-runtime-4.2.1</a></td><td align="right">67MB</td><td><tt>e80d1882d36e4c9737e80fcb5080bc683403ddb5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdebase-workspace-4.2.1.tar.bz2">kdebase-workspace-4.2.1</a></td><td align="right">49MB</td><td><tt>412b8a6778d5c71a366c054b0136edae309bbef0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdebindings-4.2.1.tar.bz2">kdebindings-4.2.1</a></td><td align="right">4.6MB</td><td><tt>96353bb3269a7ca37ff31487a0fb7a9c25958963</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdeedu-4.2.1.tar.bz2">kdeedu-4.2.1</a></td><td align="right">57MB</td><td><tt>f2381f33f6586b950e925423d135b9e66b7bf428</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdegames-4.2.1.tar.bz2">kdegames-4.2.1</a></td><td align="right">37MB</td><td><tt>dee8a0fece054bc3b6234fa088ca16b8f5f87795</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdegraphics-4.2.1.tar.bz2">kdegraphics-4.2.1</a></td><td align="right">3.5MB</td><td><tt>5c21e016c75a79a9499aac26ea1240d6024e700e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdelibs-4.2.1.tar.bz2">kdelibs-4.2.1</a></td><td align="right">9.6MB</td><td><tt>d2214b9864b64e4a8382a9f593d082c801c58571</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdemultimedia-4.2.1.tar.bz2">kdemultimedia-4.2.1</a></td><td align="right">1.5MB</td><td><tt>5382c963fae0ca6528c326b73234525e170a5c2e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdenetwork-4.2.1.tar.bz2">kdenetwork-4.2.1</a></td><td align="right">7.2MB</td><td><tt>d6d730c167cd72d43904715014b2adc8f7d5bc1e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdepim-4.2.1.tar.bz2">kdepim-4.2.1</a></td><td align="right">13MB</td><td><tt>be97f4d34eb19b08c30988e07a75c24d5ccad08c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdepimlibs-4.2.1.tar.bz2">kdepimlibs-4.2.1</a></td><td align="right">1.6MB</td><td><tt>150228037fcd740fec0a490149cd1980ddb8fb57</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdeplasma-addons-4.2.1.tar.bz2">kdeplasma-addons-4.2.1</a></td><td align="right">4.1MB</td><td><tt>8e164a8e1476862392371f765372c2e168895d55</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdesdk-4.2.1.tar.bz2">kdesdk-4.2.1</a></td><td align="right">5.2MB</td><td><tt>dca74527bcf6e5925ec58a74196e683cc68a259a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdetoys-4.2.1.tar.bz2">kdetoys-4.2.1</a></td><td align="right">1.3MB</td><td><tt>46157a10a35d37e798faa8bb988ac1c3f2a51f07</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdeutils-4.2.1.tar.bz2">kdeutils-4.2.1</a></td><td align="right">2.2MB</td><td><tt>2f875d05584b25b928b38e1da2b04c073acefd35</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/kdewebdev-4.2.1.tar.bz2">kdewebdev-4.2.1</a></td><td align="right">2.5MB</td><td><tt>438bef3bb32ce53a83c6f30f65fb49d4d4e7c76a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.1/src/phonon-4.3.1.tar.bz2">phonon-4.3.1</a></td><td align="right">564KB</td><td><tt>f7537e5280d0a4cc1348975daa7a7e45d833d45c</tt></td></tr>
</table>
