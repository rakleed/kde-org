---
aliases:
- ../4.7
- ../4.7.et
date: '2011-07-27'
custom_about: true
custom_contact: true
title: Uued KDE rakendused, töötsoonid ja arendusplatvorm pakuvad uusi võimalusi ja
  suuremat stabiilsust
---

<p>
KDE-l on rõõm teatada uusimatest väljalasetest, mis uuendavad oluliselt KDE Plasma töötsoone, KDE rakendusi ja kogu KDE tarkvarale alust moodustavat KDE platvormi. Kõigi nende väljalasete versioon 4.7 pakub rohkelt uusi võimalusi ning paranenud stabiilsust ja jõudlust.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/general-desktop.png">
	<img src="/announcements/4/4.7.0/thumbs/general-desktop.png" class="img-fluid" alt="Plasma ja rakendused 4.7">
	</a> <br/>
	<em>Plasma ja rakendused 4.7</em>
</div>
<br/>

<h3>
<a href="./plasma">
Plasma töötsoonid: KWin aitab neid kõikjal kasutada
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="KDE Plasma töötsoonid 4.7" />
</a>

Plasma töötsoonidele on kasuks tulnud ulatuslik arendustegevus KDE komposiit-aknahalduri KWin kallal ning selliste uute Qt tehnoloogiate nagu Qt Quick kasutamise võimalus. Täpsemalt kõneleb kõigest <a href="./plasma">Plasma töölaua ja Plasma Netbook 4.7 väljalasketeade</a>.
<br />

</p>

<h3>
<a href="./applications">
Uuendatud KDE rakendused pakuvad rohkelt vaimustavaid omadusi
</a>
</h3>

<p>
<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="KDE rakendused 4.7"/>
</a>

Paljud KDE rakendused läbisid uuenduskuuri. Eriti tasub välja tuua KDE grupitöölahenduse Kontact taasühinemist KDE reeglipärase väljalasketsükliga pärast seda, kui kõik selle tähtsamad komponendid porditi Akonadi peale. Samal ajal näeb ilmavalgust KDE võimalusterohke fotohalduri Digikam uus väljalase. Täpsemalt kõneleb kõigest <a href="./applications">KDE rakenduste 4.7 väljalasketeade</a>.
<br />

</p>

<h3>
<a href="./platform">
KDE platvorm: täiustatud multimeedia-, kiirsuhtlus- ja semantilised võimalused
</a>
</h3>

<p>
<a href="./platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="KDE arendusplatvorm 4.7.0"/>
</a>

Suur hulk KDE ja kolmanda poole tarkvara saab tulu ulatuslikust arendustööst Phononi kallal ning olulistest täiustustest semantilise töölaua komponentide seas, mille API on tublisti täienenud ning stabiilsus suurenenud. Uus KDE Telepathy raamistik võimaldab lõimida kiirsuhtluse otse töötsoonidesse ja rakendustesse. Jõudluse ja stabiilsuse paranemine peaaegu kõigis komponentides tagab etema kasutajakogemuse ning vähendab KDE platvormi 4.7 kasutavate rakenduste koormust süsteemile. Täpsemalt kõneleb kõigest <a href="./platform">KDE platvorm 4.7 väljalasketeade</a>.
<br />

</p>

<h3>
Kiirsuhtlus otse töölaual
</h3>
<p>
KDE-Telepathy meeskonnal on rõõm teatada KDE uue kiirsuhtluslahenduse ajaloolisest esmaväljalaskest. See on küll alles algusjärgus, aga juba praegu võib luua igat laadi kontosid, kaasa arvatud GTalk ja Facebook Chat, ning tarvitada neid täiesti igapäevaselt. Vestlusliides võimaldab valida väga erinevate välimuste vahel, pakkudes selleks Adiumi teemasid. Soovi korral võib Plasma olekuvidina seada otse paneelile ja määrata sealt oma võrguolekut. Et projekt ei ole veel piisavalt küps kuuluma KDE suurde perekonda, pakendatakse ja levitatakse seda ülejäänud KDE komponentidest eraldi. KDE-Telepathy 0.1.0 lähtekoodi leiab saidilt <a href="http://download.kde.org/download?url=unstable/telepathy-kde/0.1.0/src/">download.kde.org</a>. Paigaldamisjuhised leiab saidilt <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Installing_stable_release">community.kde.org</a>.
</p>

<h3>
Stabiilsus ja uued võimalused
</h3>
<p>
Lisaks paljudele uutele omadustele, mida on kirjeldatud väljalasketeadetes, on KDE kaastöötajad pärast KDE tarkvara viimast suuremat väljalaset suutnud sulgeda üle 12 000 veateate (sealhulgas üle 2000 unikaalse veateate). Nii võib kindlalt öelda, et meie tarkvara on täna stabiilsem kui eales varem.
</p>

<h3>
Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline
</h3>
<p>
KDE julgustab kõiki levitama sõna sotsiaalvõrgustikes. Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter, identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr, ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile silti "KDE", et kõik võiksid vajaliku materjali hõlpsamini üles leida ja KDE meeskond saaks koostada aruandeid KDE 4.7 väljalasketeate kajastamise kohta.
Kõike seda, mis toimub seoses väljalaskega sotsiaalvõrgustikes, võite jälgida KDE kogukonna otsevoos. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes sotsiaalvõrgustikes. Otsevoo leiab aadressilt <a href="http://buzz.kde.org">buzz.kde.org</a>.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.7/&amp;title=KDE%20releases%20version%204.7%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="/announcements/4.7/" data-text="#KDE releases version 4.7 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.7/ #kde47" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.7/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.7%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="/announcements/4.7/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde47"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde47"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde47"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde47"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
Väljalasketeadetest
</h3><p>
Käesolevad väljalasketeated koostasid Algot Runeman, Dennis Nienhüser, Dominik Haumann, Jos Poortvliet, Markus Slopianka, Martin Klapetek, Nick Pantazis, Sebastian K&uuml;gler, Stuart Jarvis, Vishesh Handa, Vivek Prakash, Carl Symons ja teised KDE propageerimismeeskonna ning laiemagi kogukonna liikmed. Eesti keelde tõlkis need Marek Laane. Väljalasketeated kajastavad ainult üksikuid muudatusi kogu arendustööst, mida KDE tarkvara on viimasel poolel aastal üle elanud.
</p>

<h4>KDE toetamine</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.7.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Astuge mängu"/> </a>

<p align="justify"> KDE e.V. uus <a
href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.</p>

<p>&nbsp;</p>
