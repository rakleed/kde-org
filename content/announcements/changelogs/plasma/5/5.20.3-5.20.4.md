---
aliases:
- /announcements/plasma-5.20.3-5.20.4-changelog
hidden: true
plasma: true
title: Plasma 5.20.4 complete changelog
type: fulllog
version: 5.20.4
---

{{< details title="breeze" href="https://commits.kde.org/breeze" >}}
+ Notify on color scheme changes. [Commit.](http://commits.kde.org/breeze/a632f35e49f1475618557790328a498c32bc4f90) Fixes bug [#428771](https://bugs.kde.org/428771)
{{< /details >}}

{{< details title="discover" href="https://commits.kde.org/discover" >}}
+ Fix incorrect usage of units on ApplicationDelegate. [Commit.](http://commits.kde.org/discover/6b3a6f4d89aff557e12e4a44252b46c77ff125b6) 
+ Fix sidebar header/toolbar sizing and height. [Commit.](http://commits.kde.org/discover/4c118c49a7792b15a7d7e10adcac021c6ed37053) 
+ Display title in application page. [Commit.](http://commits.kde.org/discover/d32292b9808baf57d04f37feb8c90cf5f11bafdd) 
+ Pk: Set the Daemon locale at start. [Commit.](http://commits.kde.org/discover/88cd73a57d7b068970eee885bd3ea6a9b907b0c6) See bug [#424862](https://bugs.kde.org/424862)
+ Fix installation of local packages. [Commit.](http://commits.kde.org/discover/0db81d6e944356c07bd38994afc1c5f4ec19f6e1) Fixes bug [#428125](https://bugs.kde.org/428125)
{{< /details >}}

{{< details title="kinfocenter" href="https://commits.kde.org/kinfocenter" >}}
+ Weight main categories properly. [Commit.](http://commits.kde.org/kinfocenter/1b666ba9f39d0a1c99820e4350fd90b9bab04f21) Fixes bug [#429153](https://bugs.kde.org/429153)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Use QuickControls 2 StackView. [Commit.](http://commits.kde.org/kscreenlocker/c4c2aeb7210eae08d499053e048704673e4a5910) Fixes bug [#429290](https://bugs.kde.org/429290)
+ Actually replace wallaper pages. [Commit.](http://commits.kde.org/kscreenlocker/45a2029af3d6f6f5d208fb34337272663a15dc59) 
{{< /details >}}

{{< details title="kwin" href="https://commits.kde.org/kwin" >}}
+ Xwl: No need to delete the source immediately. [Commit.](http://commits.kde.org/kwin/10e1b6fd9db57ef95ecb710335acae1c4bc0d0b2) 
+ Xwl: No need to create createX11Source twice consecutively. [Commit.](http://commits.kde.org/kwin/7bfb9add9666c56cb4c993208d9bf42a4bdae86f) 
+ Xwl: Do not refresh the x11 Clipboard while fetching. [Commit.](http://commits.kde.org/kwin/99b29195b45ecc0de97761f13e7df81dd92b53e7) Fixes bug [#424754](https://bugs.kde.org/424754). See bug [#412350](https://bugs.kde.org/412350)
+ Xwl: Include errors and warnings. [Commit.](http://commits.kde.org/kwin/9854f403332145bb36199575fbd2dbf95f7bf4d3) 
+ Screencating: query for dmabuf availability before we start streaming. [Commit.](http://commits.kde.org/kwin/a3f5d4310010dd6fb2f4d1359d0490cbc31c783f) 
+ Wayland: Fix clipped thumbnails of client-side decorated apps. [Commit.](http://commits.kde.org/kwin/038aa9d8d7effc2170d66b5d12c25d704ebcaa46) Fixes bug [#428595](https://bugs.kde.org/428595)
+ Fixed Toggle Night Color global shortcut, which used i18n in object name, leading to erratic behavior e.g. when system locale or translations changed. [Commit.](http://commits.kde.org/kwin/6acab647182158d2ebbfc313b6d030fbfaa1acc9) Fixes bug [#428024](https://bugs.kde.org/428024)
+ Set setMoveResize(true) after stopping fullscreen and quick tiling. [Commit.](http://commits.kde.org/kwin/4361892c25dbb9b94c4e5ba8f00f03861d1fde67) Fixes bug [#427848](https://bugs.kde.org/427848)
+ Screencasting: don't crash if the cursor is too big for our buffer. [Commit.](http://commits.kde.org/kwin/233df87c2cd0495f008f0773f78546f25fb6d6e8) 
+ Fix: magiclamp effect wrong direction. [Commit.](http://commits.kde.org/kwin/2a4fb12f8f2b7426a0a85314e5f67dfd2fb25d25) 
+ Kcm/decorations: Fix border size updating for thumbnails. [Commit.](http://commits.kde.org/kwin/922dc0050b7a77c60e0189cb9bea45b681610ec0) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Fix build with newer Qt. [Commit.](http://commits.kde.org/libkscreen/98508c587f5d24c6a0c480ee415b42e3baf8e5e6) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Do not insert non existing columns. [Commit.](http://commits.kde.org/libksysguard/f54298a2fa53885ba0900ab228dc887d5e508e5e) 
+ Write entries if face config changes. [Commit.](http://commits.kde.org/libksysguard/984148d39df3da1d726aec9bf8e741ec2c840541) Fixes bug [#429155](https://bugs.kde.org/429155). Fixes bug [#429167](https://bugs.kde.org/429167). Fixes bug [#429367](https://bugs.kde.org/429367)
+ Delete compactRepresentation when switching faces. [Commit.](http://commits.kde.org/libksysguard/0eb3e56e70300fe5990529045d93c0930e9d9330) Fixes bug [#424599](https://bugs.kde.org/424599)
{{< /details >}}

{{< details title="plasma-desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Remove cmake checks for breeze decoration. [Commit.](http://commits.kde.org/plasma-desktop/cf44fdd5005b614c6939110136e4d19b7ec02e3d) 
+ There is no Q_FALLTHROUGH in qml or js. [Commit.](http://commits.kde.org/plasma-desktop/980b22d518e0cfcfba376c01bc43cc7454317904) Fixes bug [#429574](https://bugs.kde.org/429574)
+ Fix translation extraction. [Commit.](http://commits.kde.org/plasma-desktop/46b8f34cd2cf94ef08eaa94679a43fef1d71e480) Fixes bug [#429466](https://bugs.kde.org/429466)
+ [kcms/users]: Fix 429313. [Commit.](http://commits.kde.org/plasma-desktop/6d94a36193944a0751f17364c19ea1d989f0b7a2) Fixes bug [#429313](https://bugs.kde.org/429313)
+ Use plasma theme icons in kickoff leave view. [Commit.](http://commits.kde.org/plasma-desktop/2da94367a93d53dd9ede58cc2e38a1b376c963a5) See bug [#429280](https://bugs.kde.org/429280)
+ Don't use visible property in procedural code to determine state. [Commit.](http://commits.kde.org/plasma-desktop/809f3787555f323de2a2f96d1e8d9f13601f4c27) Fixes bug [#408116](https://bugs.kde.org/408116)
+ [panel] Fix dragging panel to resize for top and right panels. [Commit.](http://commits.kde.org/plasma-desktop/bf7d64b37479c40ad137b9e2a38c35032c2ff66e) Fixes bug [#429063](https://bugs.kde.org/429063)
{{< /details >}}

{{< details title="plasma-pa" href="https://commits.kde.org/plasma-pa" >}}
+ Remove count property from PulseObjectFilterModel. [Commit.](http://commits.kde.org/plasma-pa/56bf58379fe507950a08240547710db5b6c7b8bf) Fixes bug [#427978](https://bugs.kde.org/427978)
{{< /details >}}

{{< details title="plasma-workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix Environment Canada weather domain. [Commit.](http://commits.kde.org/plasma-workspace/8c4e3bc68d6fa3cfcfac9f9910c2068589a4cb6c) 
+ Fix build with newer Qt. [Commit.](http://commits.kde.org/plasma-workspace/2a4a4a07dd7e0016577760e00b410a69e96c4e2b) 
+ Fix: Font files, kfontview and thumbnailer crash on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/7c994031f3bd755def1474bcd66636a52b8d5d25) Fixes bug [#401031](https://bugs.kde.org/401031)
+ Readd searching for breeze decoration. [Commit.](http://commits.kde.org/plasma-workspace/3c7c3ced762a5909c25fba5c9a18ed5e815e9dfb) Fixes bug [#429298](https://bugs.kde.org/429298)
+ [Notifications] Check pause button when job is paused. [Commit.](http://commits.kde.org/plasma-workspace/46fd949d6a02d882d5aba9676c356ecc7732ab58) 
+ The cursor previews are in a layout. [Commit.](http://commits.kde.org/plasma-workspace/320e77d90de2ff23bfb7caa4097952996c0fe2de) 
+ Revert "Use new simpler way to disable session management in services". [Commit.](http://commits.kde.org/plasma-workspace/da34fd073f6b361fde1fdcee559d60e8c0268cd6) See bug [#424408](https://bugs.kde.org/424408)
+ [Tab switcher] Fix binding loop that spams the log. [Commit.](http://commits.kde.org/plasma-workspace/8dbc1ff6b8ecb3f15571b65e526c7e739c865049) Fixes bug [#410984](https://bugs.kde.org/410984)
{{< /details >}}

{{< details title="systemsettings" href="https://commits.kde.org/systemsettings" >}}
+ Also load category when opening startup module in icon mode. [Commit.](http://commits.kde.org/systemsettings/3489e39ce5aafdf4d45bd89ffe3568cc791a9b14) Fixes bug [#429306](https://bugs.kde.org/429306)
{{< /details >}}