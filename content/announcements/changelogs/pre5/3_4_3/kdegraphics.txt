2005-07-21 06:01 +0000 [r437180]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/NEWS: make it clear
	  those features were _later_ backported

2005-07-21 06:16 +0000 [r437181]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/README,
	  branches/KDE/3.4/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  -INSTALL ref since only in backport branches; ref to
	  branches/kolourpaint/control

2005-07-21 07:05 +0000 [r437182]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  revertlast

2005-07-21 07:17 +0000 [r437185]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/kpmainwindow_view.cpp:
	  dfaure said that bug is a feature

2005-07-21 14:27 +0000 [r437317]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/splash/Splash.cc: Fix
	  crahes, refer to poppler bugs 3728 and 3750

2005-07-23 20:28 +0000 [r437990]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/kviewpart.cpp: Backport
	  fix for bug #109478. Don't keep the zoom combobox focused.

2005-07-23 20:43 +0000 [r437998]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/centeringScrollview.cpp,
	  branches/KDE/3.4/kdegraphics/kviewshell/kviewpart.cpp,
	  branches/KDE/3.4/kdegraphics/kviewshell/centeringScrollview.h:
	  Backport fix for bug #109530. Restore scrollbars when switching
	  back from fullscreenmode.

2005-07-23 20:47 +0000 [r438000]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/kviewpart.cpp: I should
	  remember to wait for the compiler to finish before I commit
	  something.

2005-07-25 20:45 +0000 [r438717]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/DCTStream.cc: Work on
	  bad jpeg data that have garbage before the start marker. Fixes
	  poppler bug #3299

2005-07-26 21:03 +0000 [r439008]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/fofi/FoFiType1.cc: Fix for
	  crash in document from poppler bug 3344

2005-08-01 19:17 +0000 [r442150]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/DCTStream.cc,
	  branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/DCTStream.h: Even
	  more backports of the fix to the 3299 patch

2005-08-02 16:42 +0000 [r442423]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/PDFDoc.cc: Backport
	  fix for bug 110034

2005-08-02 17:08 +0000 [r442431]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/PDFDoc.cc: Backport
	  fix for bug 110000

2005-08-03 16:11 +0000 [r442692]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/ui/toc.cpp: Forgot to backport
	  this crash fix from 3.5 branch, sorry about it, will be there on
	  KDE 3.4.3 and KDE 3.5.0 BUGS: 110087 BUGS: 110111

2005-08-04 13:49 +0000 [r442941]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/tools/kptooltext.cpp:
	  fix warnings [also in trunk/]

2005-08-04 13:52 +0000 [r442943]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/tools/kptooltext.cpp:
	  This is a NOP change to allow a comment to be placed in SVN
	  history: the last commit should have referred to
	  "branches/KDE/3.5/" , not "trunk/KDE/" ("trunk")

2005-08-04 14:13 +0000 [r442953]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/kolourpaintui.rc:
	  warning about XML changes affecting future KolourPaint wrapper
	  script for "version" parsing [also in branches/KDE/3.5]

2005-08-04 14:36 +0000 [r442963]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/kolourpaint.desktop,
	  branches/KDE/3.4/kdegraphics/kolourpaint/kpdefs.h,
	  branches/KDE/3.4/kdegraphics/kolourpaint/kpdocumentsaveoptions.cpp:
	  * Correct and update image format associations to all formats
	  supported by KDE 3.4.2 (kdelibs/kimgio/:r436163)

2005-08-04 14:42 +0000 [r442969]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.4/kdegraphics/kolourpaint/README,
	  branches/KDE/3.4/kdegraphics/kolourpaint/VERSION,
	  branches/KDE/3.4/kdegraphics/kolourpaint/kolourpaint.cpp:
	  1.4.3_light-pre; up-to-date docs

2005-08-04 17:11 +0000 [r443024]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/centeringScrollview.cpp:
	  Backport of revision 442905 Accept drops in the PageView widget

2005-08-05 22:19 +0000 [r443369]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/part.h,
	  branches/KDE/3.4/kdegraphics/kpdf/part.cpp: Backport the unbreak
	  back patch

2005-08-06 12:02 +0000 [r443466]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/PDFDoc.cc: improve
	  last 1024 bytes EOF seek

2005-08-07 04:42 +0000 [r443732]  thiago

	* branches/KDE/3.4/kdegraphics/kghostview/displayoptions.h,
	  branches/KDE/3.4/kdegraphics/kghostview/displayoptions.cpp:
	  Removing the templated operator<<, as suggested by Walter Meinl.
	  Thanks for pointing out. I hope to get some points with Dirk's
	  gcc4 compile police :-) BUG:110249

2005-08-07 23:54 +0000 [r443936]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/PDFDoc.cc: Fix %%EOF
	  checking BUGS: 110353

2005-08-08 12:41 +0000 [r444031]  mueller

	* branches/KDE/3.4/kdegraphics/kfile-plugins/png/kfile_png.cpp: fix
	  memory corruption on 64bit

2005-08-08 15:53 +0000 [r444068]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/pageSizeWidget.cpp,
	  branches/KDE/3.4/kdegraphics/kviewshell/sizePreview.cpp,
	  branches/KDE/3.4/kdegraphics/kviewshell/pageSizeWidget_base.ui,
	  branches/KDE/3.4/kdegraphics/kviewshell/sizePreview.h: Backport
	  of revision 444067 Make the pageSizePreview-widget work again.
	  And while i'm on it: Some rendering improvements to this widget,
	  and fixes to the layout of the page size dialog.

2005-08-10 09:57 +0000 [r444465]  mlaurent

	* branches/KDE/3.4/kdegraphics/libkscan/scanparams.cpp: Fix error
	  when so is null

2005-08-10 10:39 +0000 [r444475]  mueller

	* branches/KDE/3.4/kdegraphics/kfile-plugins/png/kfile_png.cpp: add
	  DoS protection

2005-08-12 20:48 +0000 [r446212]  mueller

	* branches/KDE/3.4/kdegraphics/kview/photobook/Makefile.am: its a
	  plugin, dude

2005-08-15 11:30 +0000 [r449386]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/kmultipage.cpp: Backport
	  of bugfix for bug #110727 Fix mousewheel scrolling in single page
	  mode.

2005-08-16 10:21 +0000 [r449660]  mlaurent

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/JPXStream.cc: Fix
	  crash on x86_64

2005-08-17 22:41 +0000 [r450335]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/Stream.cc,
	  branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/Stream.h,
	  branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/Makefile.am: Disable
	  zlib based decoder from poppler as it cause bugs BUGS: 110199

2005-08-26 13:10 +0000 [r453551]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/ui/minibar.cpp: Backport fix
	  for bug 110171

2005-09-01 14:06 +0000 [r455833]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/core/document.h,
	  branches/KDE/3.4/kdegraphics/kpdf/core/document.cpp,
	  branches/KDE/3.4/kdegraphics/kpdf/part.cpp: Backport If somebody
	  from the upper spheres (like Konqui for example) gives us the
	  mimetype use it instead trying to guess the mimetype ourselves
	  for the file we are opening, fixes problem in opening a pdf file
	  generated by php code

2005-09-04 11:01 +0000 [r456976]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/kpmainwindow.cpp: * Text
	  drops to the empty part of the scrollview will not be placed
	  outside the document [also in branches/KDE/3.5/]

2005-09-04 11:05 +0000 [r456985-456981]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/widgets/kpcolortoolbar.cpp,
	  branches/KDE/3.4/kdegraphics/kolourpaint/widgets/kpcolortoolbar.h:
	  * Prevent accidental drags in the Colour Palette from pasting
	  text containing the colour code * Cells in the bottom row and
	  cells in the rightmost column of the Colour Palette are now the
	  same size as the other cells [also in branches/KDE/3.5/]

	* branches/KDE/3.4/kdegraphics/kolourpaint/NEWS: add NEWS for last
	  2 commits

2005-09-09 13:52 +0000 [r458974]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/ui/pageview.cpp: unblock the
	  viewportupdating when we get a invalid viewport

2005-09-10 01:05 +0000 [r459143]  thiago

	* branches/KDE/3.4/kdegraphics/kgamma/kcmkgamma/kgamma.desktop:
	  Backport of 459141: readding missing Categories line that got
	  removed in r355823.

2005-09-10 12:01 +0000 [r459217]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/ui/pageview.cpp: Do not launch
	  the relayouting immediately but wait to return to the event loop
	  as that relayouting interferes with observer loading

2005-09-10 18:11 +0000 [r459399]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/ui/minibar.cpp: Backport fix
	  for bug 112332

2005-09-11 20:20 +0000 [r459780]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/ui/minibar.cpp: Ensure the
	  button is updated on enter/leave events, as some styles may not
	  do that.

2005-09-14 18:45 +0000 [r460679]  whuss

	* branches/KDE/3.4/kdegraphics/kviewshell/documentWidget.cpp:
	  Backport of commit 460677 Fix a subtle drawing bug. Previously
	  whenever the documentWidget recieved a paintEvent with a
	  nonrectangular damaged region, the overlays (textselection,
	  hyperlinks) got overwritten by the page pixmap, because the
	  bitBlt was executed for the whole rectangle containing the
	  region, not only the region itself. This bug only appeared when
	  the pageView was scrolled simultaneously in both horizontal and
	  vertical direction while an overlay was visible.

2005-09-20 14:36 +0000 [r462275]  kebekus

	* branches/KDE/3.4/kdegraphics/kdvi/dviFile.cpp,
	  branches/KDE/3.4/kdegraphics/kdvi/dviFile.h,
	  branches/KDE/3.4/kdegraphics/kdvi/dviwin_prescan.cpp: fixes bug
	  #112446

2005-09-29 20:55 +0000 [r465464-465462]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/part.h,
	  branches/KDE/3.4/kdegraphics/kpdf/ui/thumbnaillist.cpp,
	  branches/KDE/3.4/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.4/kdegraphics/kpdf/part.cpp: backport fix for bug
	  109764, it seems we are finally going to have a 3.4.3

	* branches/KDE/3.4/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.4/kdegraphics/kpdf/VERSION: update version to
	  0.4.3

2005-10-02 01:42 +0000 [r466255]  mpyne

	* branches/KDE/3.4/kdegraphics/ksnapshot/ksnapshotwidget.ui:
	  Removing useless include hints from ksnapshot 3.4's
	  ksnapshotwidget.ui file in the hopes that it will build with Qt
	  3.3.5. One of the files doesn't appear to be in use anymore
	  anyways.

2005-10-02 13:15 +0000 [r466367]  whuss

	* branches/KDE/3.4/kdegraphics/kdvi/kdvi_multipage.cpp: Fix
	  printing the current page BUG:113161

2005-10-02 13:22 +0000 [r466370-466369]  whuss

	* branches/KDE/3.4/kdegraphics/kdvi/kdvi_multipage.cpp,
	  branches/KDE/3.4/kdegraphics/kdvi/main.cpp: Bump version number

	* branches/KDE/3.4/kdegraphics/kviewshell/main.cpp,
	  branches/KDE/3.4/kdegraphics/kviewshell/kviewpart.cpp: Bump
	  version number

2005-10-02 18:30 +0000 [r466463]  coolo

	* branches/KDE/3.4/kdegraphics/kviewshell/pageSizeWidget_base.ui:
	  fix build with qt 3.3.5

2005-10-02 18:41 +0000 [r466467]  whuss

	* branches/KDE/3.4/kdegraphics/kdvi/kdvi_multipage.cpp: Don't break
	  message freeze.

2005-10-03 18:12 +0000 [r466932]  aacid

	* branches/KDE/3.4/kdegraphics/kpdf/xpdf/xpdf/GlobalParams.cc: Fix
	  for bug 113771

2005-10-04 10:43 +0000 [r467124-467123]  dang

	* branches/KDE/3.4/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.4/kdegraphics/kolourpaint/README,
	  branches/KDE/3.4/kdegraphics/kolourpaint/VERSION,
	  branches/KDE/3.4/kdegraphics/kolourpaint/kolourpaint.cpp: Release
	  "1.4.3_light"

	* branches/KDE/3.4/kdegraphics/kolourpaint/NEWS: KDE 3.4.3 not
	  3.4.2

2005-10-05 13:47 +0000 [r467518]  coolo

	* branches/KDE/3.4/kdegraphics/kdegraphics.lsm: 3.4.3

