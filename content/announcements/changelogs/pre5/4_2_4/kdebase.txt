------------------------------------------------------------------------
r961882 | mueller | 2009-04-30 21:11:38 +0000 (Thu, 30 Apr 2009) | 2 lines

update version number for 4.2.3

------------------------------------------------------------------------
r962583 | freininghaus | 2009-05-02 15:29:12 +0000 (Sat, 02 May 2009) | 8 lines

Ignore the 'HoverCloseButton' setting and remove it from konqueror.kcfg (KTabBar does not have this functionality any more, and anyway, it seems to me that the setting could actually never be controlled using the GUI).

Fixes the problem that the "Show close button on tabs" setting in the settings dialog is ignored if the 'HoverCloseButton' setting is present in konquerorrc.

Fix will be in KDE 4.2.4.

CCBUG: 190699

------------------------------------------------------------------------
r963249 | segato | 2009-05-04 10:03:45 +0000 (Mon, 04 May 2009) | 7 lines

backport of r963247

use fileName() instead of baseName() since we want the extension too
also search for jpeg file too when there is no extension
CCBUG:190345


------------------------------------------------------------------------
r963983 | ilic | 2009-05-05 19:12:39 +0000 (Tue, 05 May 2009) | 1 line

Mode string may be empty, i18n does not like that. (bport: 963982)
------------------------------------------------------------------------
r964062 | freininghaus | 2009-05-05 21:51:50 +0000 (Tue, 05 May 2009) | 6 lines

Don't show an empty window caption when browsing "/", "trash:", etc.

Fix will be in KDE 4.2.4.

CCBUG: 190783

------------------------------------------------------------------------
r964129 | jacopods | 2009-05-06 04:35:46 +0000 (Wed, 06 May 2009) | 2 lines

Revert previous commit; bugfix interacts with another bug (ARGH)

------------------------------------------------------------------------
r964158 | scripty | 2009-05-06 08:27:57 +0000 (Wed, 06 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r965329 | hindenburg | 2009-05-08 15:42:41 +0000 (Fri, 08 May 2009) | 4 lines

Expand ~ for working/initial directory; includes the command line and in profiles.

CCBUG: 183283

------------------------------------------------------------------------
r965532 | graesslin | 2009-05-09 09:19:17 +0000 (Sat, 09 May 2009) | 3 lines

Backport rev 965526
GeometryTip has to use X11BypassWindowManagerHint to see this window in compositing.
CCBUG: 174798
------------------------------------------------------------------------
r965732 | cfeck | 2009-05-09 16:18:46 +0000 (Sat, 09 May 2009) | 4 lines

Do not access uninitialized config variables (backport r965730)

CCBUG: 192129

------------------------------------------------------------------------
r965955 | scripty | 2009-05-10 07:32:07 +0000 (Sun, 10 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r966182 | orlovich | 2009-05-10 15:23:12 +0000 (Sun, 10 May 2009) | 5 lines

Fix signal connection, so triggering 'Form Completion' setting marks us 
as dirty

BUG:192247

------------------------------------------------------------------------
r966289 | darioandres | 2009-05-10 21:33:16 +0000 (Sun, 10 May 2009) | 9 lines

Backport to 4.2branch of:
SVN commit 966287 by darioandres:

Clear the timeServers list on load()
Also, do not call load() from the constructor as it will be called later
automatically.

CCBUG: 180211

------------------------------------------------------------------------
r966303 | darioandres | 2009-05-10 21:58:37 +0000 (Sun, 10 May 2009) | 11 lines

Backport to 4.2branch of:
SVN commit 966298 by darioandres:

Use "PCI_LOOKUP_DEVICE|PCI_LOOKUP_SUBSYSTEM" flags instead of
"PCI_LOOKUP_VENDOR|PCI_LOOKUP_DEVICE|PCI_LOOKUP_SUBSYSTEM".
This fixes a crash in certain situations in the PCI KCM
(the information obtained from the call using the new flags doesn't change
as documented in the pciutils header doc.)

CCBUG: 173327

------------------------------------------------------------------------
r966487 | scripty | 2009-05-11 09:05:55 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r967305 | scripty | 2009-05-13 08:04:18 +0000 (Wed, 13 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r968598 | freininghaus | 2009-05-16 11:02:08 +0000 (Sat, 16 May 2009) | 10 lines

Set the font to italic for symbolic links before calculating the size
of the area which is made available to KFileItemDelegate::paint. Fixes
the problem that Dolphin truncates the names of some symbolic links if
the italic version of the font needs more space than the non-italic
one.

Fix will be in KDE 4.2.4.

CCBUG: 183620

------------------------------------------------------------------------
r968937 | osterfeld | 2009-05-16 23:24:39 +0000 (Sat, 16 May 2009) | 2 lines

including clucene is only needed when both clucene and sopranoindex are found. (and the clucene include dir is added via include_directories() only then)

------------------------------------------------------------------------
r969267 | freininghaus | 2009-05-17 19:59:47 +0000 (Sun, 17 May 2009) | 7 lines

Update the directory of the Terminal Panel if the current URL is not a
"file:" URL, but is nevertheless local (e.g., "desktop:").

Fix will be in KDE 4.2.4.

CCBUG: 167810

------------------------------------------------------------------------
r969305 | darioandres | 2009-05-17 22:17:08 +0000 (Sun, 17 May 2009) | 9 lines

Backport to 4.2branch of:
SVN commit 969303 by darioandres:

If we are going to retrieve the selected item with currentRow()
we should set the initially selected item using setCurrentRow()
otherwise it won't work properly

CCBUG: 191868

------------------------------------------------------------------------
r969649 | dafre | 2009-05-18 16:43:14 +0000 (Mon, 18 May 2009) | 2 lines

Backporting interface fix

------------------------------------------------------------------------
r969674 | dafre | 2009-05-18 17:14:35 +0000 (Mon, 18 May 2009) | 2 lines

Backporting fix for KCM

------------------------------------------------------------------------
r969879 | scripty | 2009-05-19 07:35:12 +0000 (Tue, 19 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r969964 | gokcen | 2009-05-19 12:00:00 +0000 (Tue, 19 May 2009) | 1 line

Fix typo of kDebug, show "adding kcmodule file" debug output correctly.
------------------------------------------------------------------------
r970335 | rkcosta | 2009-05-20 04:06:52 +0000 (Wed, 20 May 2009) | 7 lines

Backport commit 913326 and the parts of 913327 that
do not do command-line processing (so that we don't
add new strings).

CCBUG: 165374
CCMAIL: johnflux@gmail.com

------------------------------------------------------------------------
r970716 | lueck | 2009-05-20 16:02:10 +0000 (Wed, 20 May 2009) | 1 line

add 2nd arg khotkeys for the catalog name, where the messages are textracted to
------------------------------------------------------------------------
r972058 | scripty | 2009-05-24 07:42:02 +0000 (Sun, 24 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r972514 | scripty | 2009-05-25 07:50:14 +0000 (Mon, 25 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r972855 | freininghaus | 2009-05-25 20:29:09 +0000 (Mon, 25 May 2009) | 15 lines

Don't crash when breaking off a tab which contains split views. This
requires two changes:

1. KonqMainWindow::slotBreakOffTab() has to break off the current tab,
not the current view (cf. a similar crash on closing split tabs, fixed
by commit 744963).

2. In KonqViewManager::breakOffTab(), loadViewProfileFromConfig()
crashes when loading the profile with the split view, it is replaced
by loadRootItem() (similar to KonqViewManager::duplicateTab()).

Fix will be in KDE 4.2.4.

CCBUG: 174292

------------------------------------------------------------------------
r973450 | scripty | 2009-05-27 08:45:37 +0000 (Wed, 27 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r974221 | aseigo | 2009-05-28 19:01:05 +0000 (Thu, 28 May 2009) | 2 lines

backport fix for potential infinite looping

------------------------------------------------------------------------
r974226 | mueller | 2009-05-28 19:17:10 +0000 (Thu, 28 May 2009) | 2 lines

4.2.4 preparations

------------------------------------------------------------------------
