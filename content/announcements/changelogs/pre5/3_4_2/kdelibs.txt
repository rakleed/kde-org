2005-05-23 12:47 +0000 [r417332]  dgp

	* kdecore/configure.in.in, kio/kio/configure.in.in: Remove warnings
	  on ./configure for FreeBSD.

2005-05-24 09:31 +0000 [r417685]  dfaure

	* kio/magic: - python bytecode magic updated (patch by
	  xavier.helluy web.de) - OASIS OpenDocument magic added

2005-05-25 11:53 +0000 [r418053]  bram

	* kate/data/pascal.xml: Backport changes to Pascal highlighting.

2005-05-27 14:05 +0000 [r418719]  dfaure

	* kdecore/netsupp.cpp: Exporting this symbol is necessary for "make
	  check" to pass, since kresolvertest uses it.

2005-05-27 14:51 +0000 [r418730]  dfaure

	* kabc/vcard/include/VCardEntity.h: Fix make check, i.e. make
	  header useable (fixed that in trunk some time ago already)

2005-05-27 15:07 +0000 [r418734]  dfaure

	* kdeui/tests/kledtest.cpp: fix compilation with gcc-3.4.3

2005-05-27 16:01 +0000 [r418753]  dfaure

	* kparts/tests/notepadpart.cpp (added), kparts/tests/Makefile.am,
	  kparts/tests/notepad.cpp: Fix "make check": don't link to a part.

2005-05-27 16:16 +0000 [r418760]  dfaure

	* kdewidgets/tests/Makefile.am: Backport: disable broken test

2005-05-27 22:23 +0000 [r418889]  dfaure

	* kspell2/tests/Makefile.am, kspell2/ui/Makefile.am:
	  hidden-visibility and unit tests for internal classes don't go
	  well together... Instead of exporting the internal classes, let's
	  put the code in a noinst lib (.a) that both the shared lib and
	  the unit tests link to.

2005-05-27 22:49 +0000 [r418896]  dfaure

	* khtml/testkhtml.cpp: This was only for memory debugging, we can
	  without it when using hidden visibility.

2005-05-29 23:19 +0000 [r419586]  carewolf

	* khtml/css/cssstyleselector.cpp: Backport of nth fix

2005-05-31 12:23 +0000 [r420133]  lunakl

	* dcop/dcopserver.cpp: Backport sending proper dcop name.

2005-05-31 13:34 +0000 [r420160]  lunakl

	* dcop/dcopserver.h, dcop/dcopserver.cpp: Backport another fix.

2005-06-01 14:28 +0000 [r420841]  mkretz

	* arts/knotify/knotify.cpp: backport to 3.4 branch CCBUG: 61438
	  apply proposed patch that removes the _node.start/stop calls
	  which seem not to be needed at this point (my aRts knowledge is
	  fading :-( )

2005-06-01 23:23 +0000 [r421013]  dfaure

	* mimetypes/application/x-debian-package.desktop (added),
	  mimetypes/application/Makefile.am: Alias for x-deb (used to be
	  provided by kpackage, so this alias is for compatibility). BUG:
	  106607

2005-06-01 23:27 +0000 [r421018]  mueller

	* kdecore/network/kresolvermanager.cpp: compile with solaris 2.6
	  BUG: 85707

2005-06-02 09:10 +0000 [r421122]  gerken

	* kio/kio/global.cpp: Add missing break (backport)

2005-06-03 09:26 +0000 [r421501]  gianni

	* kspell2/ui/Makefile.am: Fixed compiling... Thank you David
	  CCMAIL:faure@kde.org,kde-devel@kde.org

2005-06-04 18:40 +0000 [r422197]  orlovich

	* khtml/css/css_valueimpl.cpp: It's "inherit" in CSS, not
	  "inherited" BUG:106779

2005-06-06 08:24 +0000 [r422709]  mbuesch

	* kdeui/kpassdlg.cpp: fix memory leak ==7775== 4 bytes in 1 blocks
	  are definitely lost in loss record 36 of 633 ==7775== at
	  0x1B9076F1: operator new(unsigned) (vg_replace_malloc.c:132)
	  ==7775== by 0x1BCDE1A3: ourMaxLength(KPasswordEdit const*)
	  (kpassdlg.cpp:71)

2005-06-10 16:59 +0000 [r424069]  pletourn

	* kdeui/klistbox.cpp: Use contentsToViewport()

2005-06-11 08:10 +0000 [r424234]  coolo

	* khtml/khtml_ext.cpp: backport percentage fix

2005-06-11 20:29 +0000 [r424415]  staikos

	* kwallet/backend/cbc.cc: backport leak fix

2005-06-12 14:09 +0000 [r424606]  tilladam

	* kdeui/ksyntaxhighlighter.cpp: Backport of: SVN commit 424591 by
	  tilladam: Don't reparse the full KGlobal configuration (which
	  takes around 2 seconds here) each time a KDictSpellingHighlighter
	  is instantiated, which is everytime a KMail composer is opened.
	  The only key that is read there is a hidden, undocumented,
	  non-gui-accesible tuning key, so there is zero probability of it
	  changing under us by any means but direct editing of the config
	  file. In that case the user can reasonably be expected to restart
	  the application, especially if it means that starting a composer
	  KMail becomes instantaneous again, for all users. Ok'd by Waldo
	  and Zack.

2005-06-14 19:54 +0000 [r425455]  orlovich

	* kdeui/ktoolbar.cpp: Do not let the idle buttons list hold dead
	  buttons when calling QToolBar::clear, as that will delete them,
	  causing a mess. I would appreciate if someone could forward-port
	  to 3.5 Fixes #105164, might also fix #101188, but I can't
	  reproduce that well enough to be sure. BUG:105164 CCBUG:101188

2005-06-15 15:33 +0000 [r425786]  sgotti

	* kio/misc/kntlm/kntlm.cpp: Backport fix for wrong auth string sent
	  to server for NTLMv2, patch from Szombathelyi Gyrgy.

2005-06-16 19:33 +0000 [r426249]  waba

	* kdecore/klockfile.cpp: Fix locking on filesystems that don't
	  support link count BUG:105336

2005-06-16 23:37 +0000 [r426310]  dfaure

	* kio/kpasswdserver/kpasswdserver.cpp,
	  kio/kpasswdserver/kpasswdserver.h: Keep the Wallet opened (but
	  without leaks this time), so that the user has to open it only
	  once, and after that it can fill any password dialogs for you.
	  BUG: 103028

2005-06-17 17:29 +0000 [r426558]  dfaure

	* kio/kio/ktar.cpp: Fix KTar crash, e.g. when displaying the
	  tooltip for a .deb file. tmpFile wasn't initialized to 0 in both
	  constructors(!) CCBUG: 104454 In fact this fixes the initial
	  report in 104454, but the valgrind trace there is very
	  interesting for debugging #96405...

2005-06-17 19:52 +0000 [r426600]  dfaure

	* kio/kio/job.cpp: Fix a, hmm, very old bug... "Bug 37607: progress
	  bar wrong while copying files..." It's certainly most visible now
	  that there is a "keep dialog open" checkbox. The speed
	  optimization of not emitting processedFiles/processedDirs at
	  every change and using a timer instead, forgot to simply emit
	  those at the end again. Also fixed "122/121 files" bug due to the
	  initial +1. BUG: 37607

2005-06-19 21:24 +0000 [r427175]  carewolf

	* khtml/html/htmltokenizer.cpp: Backport of fix for #103935 and
	  #107363 CCBUG: 103935, 107363

2005-06-20 16:36 +0000 [r427428]  grossard

	* kdoctools/customization/fr/user.entities: added translator entity
	  and fixed emails

2005-06-21 20:09 +0000 [r427780]  dfaure

	* kio/kfile/kopenwith.cpp: Keep the full Exec line from the
	  existing service, when checking "remember application association
	  for this type of file". BUG:107860 BUG:107335

2005-06-22 15:42 +0000 [r427991]  staikos

	* kio/kssl/kssl/GeoTrust_Universal_CA.pem (added),
	  kio/kssl/kssl/caroot/ca-bundle.crt,
	  kio/kssl/kssl/GeoTrust_Universal_CA2.pem (added),
	  kio/kssl/kssl/localcerts, kio/kssl/kssl/ksslcalist: backport root
	  db update

2005-06-26 19:55 +0000 [r429165]  fwang

	* kdoctools/customization/zh-CN/lang.entities,
	  kdoctools/customization/zh-CN/entities/fdl-notice.docbook,
	  kdoctools/customization/zh-CN/catalog: Corrected some doc tags.

2005-06-27 15:12 +0000 [r429400]  binner

	* kdecore/kdeversion.h: Bump to 3.4.2

2005-06-27 15:18 +0000 [r429401]  binner

	* README, kdelibs.lsm: 3.4.2 preparations

2005-06-29 22:02 +0000 [r430034]  dfaure

	* kinit/kinit.cpp: Backport better error message than "klauncher
	  said: Error loading 'kio_foobar'"

2005-07-01 17:18 +0000 [r430523]  pletourn

	* kdeui/kedittoolbar.h, kdeui/kedittoolbar.cpp: Refactor code from
	  slot{Up,Down}Button() in a new method moveActive() Use this
	  method to handle correctly DnD of dynamic actions BUG:108343

2005-07-03 22:14 +0000 [r431325]  binner

	* kdeui/ktabwidget.cpp: Bug 106954: tooltips of tabs inside
	  konqueror window are parsed for HTML tags

2005-07-04 23:25 +0000 [r431681]  bero

	* khtml/java/org/kde/kjas/server/Main.java,
	  khtml/java/org/kde/kjas/server/KJASAppletStub.java: gcj Applet
	  support

2005-07-07 10:38 +0000 [r432435]  binner

	* kdeui/kkeydialog.cpp: An always disabled button does not make
	  sense

2005-07-07 21:38 +0000 [r432580]  pletourn

	* kdeui/klistview.cpp: Give back the focus to the view after
	  canceling a rename BUG:98120

2005-07-11 19:16 +0000 [r433757]  mueller

	* kinit/lnusertemp.c: use $TMPDIR for the KDE related /tmp files if
	  available.

2005-07-11 20:20 +0000 [r433774]  dfaure

	* kio/kio/job.cpp: No progress dialog for the subjob BUG: 95641

2005-07-11 22:37 +0000 [r433822]  mueller

	* kinit/lnusertemp.c: ossi is right, switch priorities of KDETMP vs
	  TMPDIR

2005-07-11 23:48 +0000 [r433838]  mueller

	* kinit/lnusertemp.c: grrr... lets hope this time the script works
	  alright and doesn't mismerge.

2005-07-12 13:36 +0000 [r433990]  staniek

	* kdeui/kpushbutton.cpp, kdeui/klineedit.cpp: backported: let
	  completion box inherit font settings let push button reuse a
	  tooltip from gui item

2005-07-14 12:12 +0000 [r434507]  binner

	* kdeui/kcolordialog.cpp, kio/kfile/kicondialog.cpp: Killing more
	  non-functional "Help" buttons

2005-07-17 15:57 +0000 [r435598]  staikos

	* kwallet/client/kwallet.cc: backport 429530: fix for 105365: don't
	  allow empty wallet names

2005-07-17 16:03 +0000 [r435600]  staikos

	* kio/kssl/ksslcertificatehome.cc: make hostnames case insensitive

2005-07-18 23:01 +0000 [r436066]  grossard

	* kdoctools/customization/fr/user.entities: copied from trunk

2005-07-19 14:30 +0000 [r436339]  dfaure

	* kdeui/klistview.cpp: backport Thomas' double-click fix

2005-07-19 21:37 +0000 [r436515]  orlovich

	* khtml/misc/decoder.cpp: Backport fix to 105496. Thanks to dfaure
	  for telling me of svnbackport! CCBUG:105496

2005-07-20 09:13 +0000 [r436795]  coolo

	* kdeui/kdepackages.h: new packages

