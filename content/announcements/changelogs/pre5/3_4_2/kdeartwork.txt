2005-06-10 22:51 +0000 [r424154]  howells

	* kscreensaver/configure.in.in: Correct arts fix, see bug 102398

2005-06-11 21:32 +0000 [r424439]  howells

	* kscreensaver/configure.in.in: Backport fix to 3.4 branch CCBUG:
	  89387

2005-06-27 15:18 +0000 [r429401]  binner

	* kdeartwork.lsm: 3.4.2 preparations

