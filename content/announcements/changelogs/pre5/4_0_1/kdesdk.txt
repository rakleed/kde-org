2008-01-05 15:24 +0000 [r757633]  mlaurent

	* branches/KDE/4.0/kdesdk/kbugbuster/backend/mailsender.cpp:
	  Backport : fix dbus adress

2008-01-05 20:32 +0000 [r757758]  woebbe

	* branches/KDE/4.0/kdesdk/scripts/kdesvn-build: backport 757301 by
	  toma: Make 'branch 4.0' work for pimlibs as well.

2008-01-05 20:40 +0000 [r757760]  woebbe

	* branches/KDE/4.0/kdesdk/scripts/svnforwardport: if you're on 4.0
	  you probably want to forwardport to 4.1

2008-01-05 21:00 +0000 [r757764]  woebbe

	* branches/KDE/4.0/kdesdk/scripts/svnforwardport: bah, of course
	  you always want to forwardport to trunk, but from 4.0 and not 3.5

2008-01-09 13:31 +0000 [r758889]  mueller

	* branches/KDE/4.0/kdesdk/kstartperf/libkstartperf.c,
	  branches/KDE/4.0/kdesdk/kstartperf,
	  branches/KDE/4.0/kdesdk/kstartperf/kstartperf.cpp: RELICENSE:
	  Artistic -> MIT

2008-01-10 20:27 +0000 [r759535]  ereslibre

	* branches/KDE/4.0/kdesdk/kate/app/kateviewmanager.h,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/kateviewmanager.cpp: Remove
	  unused code as we talked on #kate. Making the gui to be
	  regenerated after the config dialog is closed, making the plugin
	  bug to be fixed. Backport.

2008-01-10 20:43 +0000 [r759547]  ereslibre

	* branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp: kmultiple ->
	  document-multiple. backport

2008-01-12 10:28 +0000 [r760232]  reitelbach

	* branches/KDE/4.0/kdesdk/scripts/extractattr: fix minor typo
	  SVN_SILENT

2008-01-13 20:19 +0000 [r760965]  woebbe

	* branches/KDE/4.0/kdesdk/cervisia/version.h: bump version

2008-01-15 11:16 +0000 [r761751]  woebbe

	* branches/KDE/4.0/kdesdk/scripts/CMakeLists.txt: install
	  svnforwardport

2008-01-16 17:49 +0000 [r762268]  woebbe

	* branches/KDE/4.0/kdesdk/scripts/svnforwardport: don't use kdialog
	  (copied coolo's patch to svnbackport)

2008-01-20 17:57 +0000 [r763967]  kkofler

	* branches/KDE/4.0/kdesdk/kompare/komparepart/komparesplitter.cpp,
	  branches/KDE/4.0/kdesdk/kompare/komparepart/komparesplitter.h:
	  Fix #156174: [Regression] Mouse wheel does not cause viewport to
	  scroll CCBUG: 156714 (backport rev 763966 from trunk)

2008-01-22 10:20 +0000 [r764670]  dhaumann

	* branches/KDE/4.0/kdesdk/kate/plugins/tabbarextension/ktinytabbutton.cpp:
	  backport: fix 100 % cpu usage when highlighting tabs. CCBUG:
	  156171

2008-01-22 10:28 +0000 [r764675]  dhaumann

	* branches/KDE/4.0/kdesdk/kate/plugins/tabbarextension/ktinytabbar.cpp,
	  branches/KDE/4.0/kdesdk/kate/plugins/tabbarextension/plugin_katetabbarextension.cpp,
	  branches/KDE/4.0/kdesdk/kate/plugins/tabbarextension/ktinytabbar.h,
	  branches/KDE/4.0/kdesdk/kate/plugins/tabbarextension/plugin_katetabbarextension.h:
	  make save&load work again, thanks for the patch CCBUG: 156177

2008-01-25 00:36 +0000 [r765995]  ereslibre

	* branches/KDE/4.0/kdesdk/umbrello/umbrello/uml.cpp: Backport of
	  fix for bug 155035

2008-01-30 23:44 +0000 [r768939]  dfaure

	* branches/KDE/4.0/kdesdk/scripts/svnbackport,
	  branches/KDE/4.0/kdesdk/scripts/svnforwardport: exit immediately
	  if svnlastchange isn't in $PATH (to confuse richmoore a bit less)
	  Too bad the code changed so I couldn't use "svnbackport
	  svnbackport svnforwardport", would have been a funny command :)

