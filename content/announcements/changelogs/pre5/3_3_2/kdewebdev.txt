Dir: kdewebdev
----------------------------
new version numbers



Dir: kdewebdev/debian
----------------------------
Another round of changes for kdewebdev packaging.
----------------------------
Updates for 3.3.1.
----------------------------
Fix source overrides (see #261435).
----------------------------
Final documentation updates for now.
----------------------------
More webdev updates, mainly manpages this time.
----------------------------
More documentation.
----------------------------
Fixes for kimagemapeditor.
----------------------------
Don't ship separate libkommander packages.



Dir: kdewebdev/kfilereplace/Attic
----------------------------
This files are not even compiled...



Dir: kdewebdev/kfilereplace
----------------------------
Hide columns that are not useful (but confusing) when doing search only.



Dir: kdewebdev/kimagemapeditor
----------------------------
Fix kimagemapeditor so it correctly opens images from the command-line.
Previously it tried to open images as HTML (though opening images from
within the UI worked fine).
----------------------------
Fixed bug 93351: output is not XML compliant

BUG: 93351



Dir: kdewebdev/klinkstatus/src/ui
----------------------------
Backport of
Fix non translated strings.



Dir: kdewebdev/kommander
----------------------------
Backport some fixes from HEAD into Function Browser:
* support for 4-parameter functions
* DCOP is now default group
* don't add brackets for parameter-less functions.



Dir: kdewebdev/kommander/editor
----------------------------
Backport some fixes from HEAD into Function Browser:
* support for 4-parameter functions
* DCOP is now default group
* don't add brackets for parameter-less functions.
----------------------------
fixing name used for bug reporting



Dir: kdewebdev/kommander/widget
----------------------------
Backport: fix a nasty bug in && evaluation.
----------------------------
Backport: recognize floating-point numbers in @String.isNumber.
----------------------------
Backport a few changes related with external process handling. Fixes some execbutton-related crashes.



Dir: kdewebdev/kommander/widgets
----------------------------
Backport a few changes related with external process handling. Fixes some execbutton-related crashes.
----------------------------
Backport: Grant addUniqueItem for Combos.
----------------------------
Backport fix: use "\n" as file separator in multiselection mode of FileSelector
----------------------------
Backport: allow setting maximum value of Slider.



Dir: kdewebdev/quanta
----------------------------
Bump version.
----------------------------
Backport: don't crash on exist if the user removed an action.



Dir: kdewebdev/quanta/components/tableeditor
----------------------------
missing i18n()



Dir: kdewebdev/quanta/data
----------------------------
Fixed incorrect code for non-breaking space.



Dir: kdewebdev/quanta/data/dtep/html
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/data/dtep/html-strict
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/data/dtep/php
----------------------------
Backport: autocomplete PHP functions after @



Dir: kdewebdev/quanta/data/dtep/xhtml
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/data/dtep/xhtml-basic
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/data/dtep/xhtml-frameset
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/data/dtep/xhtml-strict
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/data/dtep/xhtml11
----------------------------
Backport: <option disabled="true"> is not valid, it should be <option disabled="disabled">. Same for selected attribute.



Dir: kdewebdev/quanta/dialogs
----------------------------
Backport: don't try to add a newly created action to a non-existent All toolbar.



Dir: kdewebdev/quanta/parsers
----------------------------
Backport table editor bugfixes.
----------------------------
Backport use isEmpty()
----------------------------
Backport: parse PHP statements ending with a one-line comment ( //comment ?>) correctly.
----------------------------
Backport fix for 91508.



Dir: kdewebdev/quanta/parts/kafka
----------------------------
* Partially fix #89976



Dir: kdewebdev/quanta/project
----------------------------
Disable Proceed button in the upload dialog once the upload is started. Fixes
various problems like non-responding Quanta after upload and possibly the
bug described in #88892.

BUG: 88892
----------------------------
Some fixes to the New Project wizard by Jens.
----------------------------
Backport: don't show directories that were removed from the project in the upload dialog [#87186].



Dir: kdewebdev/quanta/scripts
----------------------------
Support more character encoding in the Quick Start dialog.



Dir: kdewebdev/quanta/src
----------------------------
Bump version.
----------------------------
Backport: reset the MDI mode to IDEAl when the resetlayout switch or the GUI option is used.
----------------------------
Backport: don't show the Upload File item in the tab context menu if the current item is not part of a project.
----------------------------
Use KStdGuiItems.
----------------------------
Backport: always save the last project entry under the [Projects] group in the config file.



Dir: kdewebdev/quanta/treeviews
----------------------------
backport bugfix for problems with not remembering all open folders



Dir: kdewebdev/quanta/utility
----------------------------
Backport use isEmpty()
