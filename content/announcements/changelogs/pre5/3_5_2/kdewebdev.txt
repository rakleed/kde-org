2006-01-26 18:59 +0000 [r502619]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/debugger/pathmapperdialog.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/copyto.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewlocal.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/basetreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/utility/quantacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantadoc.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/examples/tutorial/strings.kmdr,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/pathmapper.cpp,
	  branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/structtreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/lib/qextfileinfo.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/templatestreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/debuggermanager.cpp:
	  Sometimes it is enough to check if a file exists and it is
	  readable, there is no need to see if it is writable as well.
	  Alloww opening of read-only remote files, like from http:// .
	  BUG: 120632

2006-01-26 20:46 +0000 [r502648]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: I
	  have no idea how we missed it and since when it is in this
	  state... Fixes insertion of files to the project using the
	  Project menu. BUG: 120629

2006-01-28 13:54 +0000 [r503231]  deller

	* branches/KDE/3.5/kdewebdev/kfilereplace/kfilereplacepart.cpp,
	  branches/KDE/3.5/kdewebdev/kfilereplace/knewprojectdlg.cpp,
	  branches/KDE/3.5/kdewebdev/kfilereplace/configurationclasses.h:
	  do not misuse a QString as a QStringList replacement. This
	  cleanup was necessary to be able to use writePathList() at all.

2006-01-29 16:29 +0000 [r503581]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.rc,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/global.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.h: Fix
	  link order to work with IRIX. Patch by "The Written Word" :)
	  CCMAIL: The Written Word <bugzilla-kde@thewrittenword.com> BUG:
	  120898

2006-01-29 16:57 +0000 [r503591]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.rc,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/global.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.h: Ups.
	  Revert previous commit.

2006-01-31 17:27 +0000 [r504297]  thiago

	* branches/KDE/3.5/kdewebdev/kommander/editor/Makefile.am: Changing
	  the linking order. BUG:120899

2006-01-31 17:32 +0000 [r504302]  thiago

	* branches/KDE/3.5/kdewebdev/kommander/widget/function.h: Name your
	  parameters correctly. BUG:120901

2006-01-31 20:40 +0000 [r504341-504340]  rdieter

	* branches/KDE/3.5/kdewebdev/kommander/plugin/Makefile.am,
	  branches/KDE/3.5/kdewebdev/kommander/widget/Makefile.am,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/Makefile.am,
	  branches/KDE/3.5/kdewebdev/kommander/factory/Makefile.am: fix bug
	  #119629 * adds -no-undefined * adds additional libraries to
	  resolve missing symbols.

	* branches/KDE/3.5/kdewebdev/kommander/pluginmanager/mainwindow.cpp:
	  drop hard-coded .la reference (related to bug #119629)

2006-02-02 17:50 +0000 [r504975]  thiago

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/xsldbgmsg.cpp:
	  Applying attachment 14406 from Bugzilla. It is far less intrusive
	  than the alternative patch (attachment 14485). BUG:120895

2006-02-03 19:22 +0000 [r505390]  jriddell

	* branches/KDE/3.5/kdewebdev/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdewebdev
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdewebdev

2006-02-08 10:41 +0000 [r507039]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/actionconfigdialog.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix detection of
	  existing shortcuts.

2006-02-08 11:20 +0000 [r507048]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp:
	  Forgot to backport this fix to 3.5

2006-02-13 18:32 +0000 [r509118]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Ignore special areas
	  inside comments. Fixes issues with IE workarounds, like: <!--[if
	  lt IE 7]> <link rel="stylesheet" href="css/schrott.css"
	  type="text/css"> <![endif]--> CCMAIL: quanta@kde.org

2006-02-15 22:44 +0000 [r509963]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed
	  (added): Syncronize.

2006-02-15 22:49 +0000 [r509965]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/configure.in.in
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/NEWS
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/README
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/klinkstatus.kdevelop
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/AUTHORS
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/TODO
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/INSTALL
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/ChangeLog
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/src
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/COPYING
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/Makefile.am
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed/data
	  (removed): Delete.

2006-02-15 23:18 +0000 [r509974]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/ChangeLog,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/htmlparser.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_shell.rc,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultssearchbar.cpp
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidgetbase.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultview.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager_impl.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/tabwidgetsession.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/styles/results_stylesheet.xsl,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/cfg/klinkstatus.kcfg,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus.desktop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus_proceed
	  (removed),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/utils/xsl.cpp (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkfilter.cpp
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/actionmanager.cpp
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus.kdevelop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/htmlparser.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configresultsdialog.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/styles (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/main.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.rc,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/utils/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.desktop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/tabwidgetsession.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/styles/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultssearchbar.h
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node_impl.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/utils/xsl.h (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/actionmanager.h
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkfilter.h
	  (added): Syncronize with klinkstatus_proceed.

2006-02-16 09:06 +0000 [r510063]  bram

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configresultsdialog.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidgetbase.ui:
	  fixuifiles

2006-02-17 15:21 +0000 [r510614]  deller

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/Makefile.am: fix make
	  with builddir != srcdir

2006-02-19 17:37 +0000 [r511378]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp: At least
	  don't crash. I have no idea why it happens (it is undo/redo
	  code), the code will be removed in KDE4 and the bug is very hard
	  to reproduce on my working machine (but I can see it on another
	  one). So until I find time to debug it deeply, at list try to
	  avoid the crash. Hopefully it will not cause more trouble in
	  other places. CCBUG: 121280

2006-02-19 18:00 +0000 [r511385]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/utility/tagaction.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix a bug that went
	  unnoticed for some years: set the tooltip for the user actions
	  correctly.

2006-02-19 19:42 +0000 [r511410]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/actionconfigdialog.cpp:
	  Don't show the newly added action twice in the action edit dialog
	  if: - the previously selected action was the last one - Apply was
	  pressed.

2006-02-21 16:55 +0000 [r512036]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp:
	  Try to crash less.

2006-02-23 08:59 +0000 [r512657]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/copyto.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/wkafkapart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewlocal.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/wkafkapart.h:
	  Connect to the right slot (the worst problem with Qt I believe,
	  you don't get compilation errors for connecting to non-existent
	  slots). Don't include "." and ".." in the result of a copy. Use a
	  guarded pointer to hopefully avoid random crash on exit.

2006-02-23 09:41 +0000 [r512667]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/utility/tagaction.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkahtmlpart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp: Disable
	  toggle actions. The reason: - they do not work and are
	  missleading in source view - they do not work as they should in
	  VPL as well, especially if VPL&Source editing is mixed.

2006-02-28 21:37 +0000 [r514630]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/document.h,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parsercommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp: This should
	  finnally fix a grave crash bug in Quanta, which appeared due to a
	  node tree corruption.The patch does other things as well: - stop
	  background detailed parsing if the text was changed (this caused
	  the corruption) - parse only once after the user stopped typing,
	  for real. The intention was there since several versions but DID
	  NOT work! Now this should really make typing faster. I will mark
	  all related bugs as fixed. Reporters, in case you still see the
	  crash or slow typing with Quanta 3.5.2 (or a recent - after this
	  commit - snapshot/svn version), reopen the report or open a new
	  one. BUG: 121280, 122475, 122252, 120983

2006-02-28 21:41 +0000 [r514631]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Remove wrong
	  comment; update ChangeLog.

2006-03-01 15:05 +0000 [r514820]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/widget/kommanderwidget.cpp:
	  Accept DCOP methods without parantheses, so @dcop(@dcopid,
	  MainApplication-Interface, quit) works now. Before you had to use
	  @dcop(@dcopid, MainApplication-Interface, "quit()") CCMAIL:
	  kommander-devel@mail.kdewebdev.org

2006-03-03 10:46 +0000 [r515285]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/scripttreeview.cpp:
	  Use %scriptdir/ in path when assigning an action to a script from
	  the treeview.

2006-03-03 15:15 +0000 [r515381]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/preview/whtmlpart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkahtmlpart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/utility/toolbartabwidget.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/basetreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/kqapp.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/actionconfigdialog.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/basetreeview.h:
	  Accept URL drops (opens the files). It does not accept on the
	  tabs (for some reason I don't know), but in many other places on
	  the main window. Also remove many outdated code and #if checks.
	  BUG: 102605

2006-03-03 15:26 +0000 [r515385]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't try to remove
	  an empty, unmodified buffer, if it is the last opened one. BUG:
	  111599

2006-03-03 16:39 +0000 [r515400]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantadoc.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp:
	  Remember cursor position for project documents. Make highlighting
	  workaround work again (was broken by previous commit). BUG:
	  101966

2006-03-03 17:11 +0000 [r515409]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/data/config/quantaui.rc: Add
	  the standard show/hide menubar action. BUG: 113064

2006-03-03 17:25 +0000 [r515414]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: Add
	  possibility to limit the number of recent files/projects. No GUI,
	  use "Recent Files Limit" in the [General Options] section. BUG:
	  113309 CCMAIL: quanta@kde.org

2006-03-03 18:21 +0000 [r515431]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/tagattributeitems.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: - fix a crash when
	  using the attribute tree - fix insertion of relative URLs from
	  the attribute tree - fix resolving of relative URLs when there
	  are symlinks around

2006-03-03 19:21 +0000 [r515443-515442]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: Do not loose the
	  trailing slash (broken by rev 515431).

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/uploadprofilespage.ui.h,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/dcopwindowmanagerif.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectupload.h:
	  WindowManagerIf::uploadURL(url, profile, markOnly) DCOP method to
	  upload an url to a profile (or mark it as uploaded).

2006-03-03 20:10 +0000 [r515460]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: Add trailing
	  slash for local directories as well. Fixes addition of local
	  directories to the project (creates weird project error like
	  #122419). BUG: 122419

2006-03-03 20:13 +0000 [r515461]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Update changelog.

2006-03-03 20:19 +0000 [r515463]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.h: It deserves a
	  change in the version.

2006-03-04 12:50 +0000 [r515623]  jens

	* branches/KDE/3.5/kdewebdev/quanta/src/main.cpp: update year to
	  2006

2006-03-04 13:32 +0000 [r515649]  jens

	* branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml11/body.tag,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: fix in data for
	  #122272

2006-03-04 14:23 +0000 [r515660]  jens

	* branches/KDE/3.5/kdewebdev/quanta/src/main.cpp: undo my last
	  commit, I forgot that it is in a translated string

2006-03-04 19:58 +0000 [r515766]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: Regression fix:
	  don't include the "." and ".." in the list of files when listing
	  a local directory.

2006-03-04 20:49 +0000 [r515783]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: Trying to get
	  the canonical path for a relative URL is not good.

2006-03-04 21:14 +0000 [r515790]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: The
	  first part of trying to fix the new project wizard (again).

2006-03-04 22:13 +0000 [r515802]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectnewweb.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewlocal.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewweb.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewlocal.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: The
	  new project wizard should work again in most cases.

2006-03-04 22:21 +0000 [r515803]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectnewweb.cpp: Hm,
	  wait with commit until compilation finishes.

2006-03-05 10:28 +0000 [r515900]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: Fix
	  another problem related to symlinked directories.

2006-03-05 10:38 +0000 [r515901]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectnewgeneral.cpp:
	  Warn if the user has selected a templates/toolbars folder outside
	  of the project base. The text is reused, thus not 100% correct,
	  but better than not doing something without any feedback.

2006-03-07 15:51 +0000 [r516557-516555]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/lib/qextfileinfo.h: Write an own
	  version of canonical path, as the QDir::canonicalPath: - does not
	  work if the path does not exist - eats the ending slash

	* branches/KDE/3.5/kdewebdev/quanta/project/projectnewgeneral.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewgeneral.cpp:
	  Do not accept template and toolbar dirs outside of the project.
	  The implementation is not perfect (you get the warning when the
	  dialog is canceled as well), but I find it OK for the current
	  branch. Quanta4 will have (has) a new wizard anyway. BUG: 118815

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: Use
	  QExtFileInfo::canonicalPath.

2006-03-08 10:07 +0000 [r516716]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: Don't try to
	  find the canonical path for relative paths (QDir::cdUp does not
	  work in this case and we get an infinite recursion).

2006-03-08 15:19 +0000 [r516785]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/dcopquantaif.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/dcopquanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/src/dcopquanta.cpp: DCOP
	  interface QuantaIf::groupElements(groupName). Described in
	  ChangeLog.

2006-03-12 17:04 +0000 [r517935]  jens

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: always reload a file
	  if the user wants it [related to #121329]

2006-03-13 13:46 +0000 [r518227]  mueller

	* branches/KDE/3.5/kdewebdev/quanta/project/project.h: the usual
	  "daily unbreak compilation"

2006-03-13 15:46 +0000 [r518270]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp: Don't leak
	  the iterator.

2006-03-13 15:57 +0000 [r518276]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/qtag.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp: Fix another
	  memory leak + micro optimization.

2006-03-13 16:12 +0000 [r518282]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/doctreeview.cpp:
	  Another non-critical memory leak fix.

2006-03-13 16:48 +0000 [r518297]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/tag.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/doctreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp: A new set of
	  leak fixes.

2006-03-13 20:19 +0000 [r518366]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/sagroupparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/undoredo.cpp:
	  Debugging code: - count the number of allocated/deleted node
	  objects - display this number in some places - comment out some
	  debug outputs I don't need now

2006-03-14 17:19 +0000 [r518611]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/preview/whtmlpart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/preview/whtmlpart.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: - fix previewing of
	  noframes area - fix previewing of read-only files (patch by Jens
	  Herden)

2006-03-14 17:32 +0000 [r518612]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp:
	  Store some of the urls saved in the quantarc file without the
	  password as it is stored as cleartext. Actually I think the
	  solution is not that good, but as a band aid, this should be OK.
	  For KDE/Quanta4 I will try to find a better one. CCBUG: 123618

2006-03-14 18:35 +0000 [r518629]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/structtreeview.cpp:
	  Don't clear the problems view when not needed.

2006-03-14 19:16 +0000 [r518640]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantadoc.cpp: Show the
	  project name in the window titlebar. Set version number to 3.5.2.

2006-03-14 19:46 +0000 [r518647]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp:
	  Don't try to open the last open project over and over if it was
	  removed from the disk. The rest of the bug is either a wish or
	  won't be implemented (see my previous comment there). BUG: 117194

2006-03-14 20:21 +0000 [r518657]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Mark uploaded files
	  as uploaded even if upload fails later. Proceed basicly acts like
	  a "retry" in this case, resumes the upload from the last
	  not-uploaded file. BUG: 111857

2006-03-16 21:14 +0000 [r519320]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/README,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/TODO,
	  branches/KDE/3.5/kdewebdev/quanta/VERSION: Preparing for 3.5.2
	  and try to fix a bug with remote projects.

2006-03-16 21:17 +0000 [r519322]  amantia

	* branches/KDE/3.5/kdewebdev/PACKAGING,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdewebdev/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdewebdev/README: Preparing for 3.5.2.

2006-03-17 02:37 +0000 [r519407]  jens

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: reload project tree
	  for remote projects after rescanning the project folder

