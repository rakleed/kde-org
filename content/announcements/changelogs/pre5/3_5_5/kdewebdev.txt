2006-07-28 13:43 +0000 [r567289]  kling

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/structtreeview.cpp:
	  Fixed a crash when navigating PHP documents via "Document
	  Structure"/"Functions" Bug introduced in r415463 CCMAIL:
	  quanta-devel@kde.org

2006-07-30 22:19 +0000 [r568027]  mrudolf

	* branches/KDE/3.5/kdewebdev/kommander/widgets/wizard.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/wizard.h: Add
	  initialization/destroy for Wizard. Use setEnabled() to
	  enable/disable Finish button.

2006-08-21 15:05 +0000 [r575439]  jriddell

	* branches/KDE/3.5/kdewebdev/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-08-31 17:39 +0000 [r579276]  dannya

	* branches/KDE/3.5/kdewebdev/doc/klinkstatus/index.docbook: fix EBN
	  issues

2006-08-31 17:47 +0000 [r579279]  dannya

	* branches/KDE/3.5/kdewebdev/doc/kfilereplace/index.docbook: fix
	  EBN issues

2006-08-31 17:54 +0000 [r579280]  dannya

	* branches/KDE/3.5/kdewebdev/doc/kommander/index.docbook: fix EBN
	  issues

2006-08-31 18:09 +0000 [r579284]  dannya

	* branches/KDE/3.5/kdewebdev/doc/xsldbg/index.docbook: fix EBN
	  issues

2006-08-31 18:19 +0000 [r579289]  dannya

	* branches/KDE/3.5/kdewebdev/doc/quanta/index.docbook: update
	  docbook version declaration

2006-08-31 18:22 +0000 [r579290]  dannya

	* branches/KDE/3.5/kdewebdev/doc/kxsldbg/index.docbook: fix EBN
	  issues

2006-09-01 19:54 +0000 [r579844]  amantia

	* branches/KDE/3.5/kdewebdev/doc/klinkstatus/index.docbook,
	  branches/KDE/3.5/kdewebdev/doc/kxsldbg/index.docbook,
	  branches/KDE/3.5/kdewebdev/doc/xsldbg/index.docbook,
	  branches/KDE/3.5/kdewebdev/doc/kfilereplace/index.docbook: Revert
	  some of the EBN fixes, as they were not compiling on older KDE
	  releases (we support 3.4.x and 3.5.x).

2006-09-01 20:04 +0000 [r579846]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/filestreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/treeviews/basetreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/gubed/quantadebuggergubed.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/utility/quantacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.h,
	  branches/KDE/3.5/kdewebdev/quanta/plugins/quantaplugininterface.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/utility/quantacommon.h,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/dtepeditdlg.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/dtds.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h: Fix a
	  long-standing bug that I could finally somewhat reproduce, which
	  caused growing of quantarc until it was too large to handle,
	  caused by the automatic backup system. Fix also other bugs in the
	  auto-backup. Change symlink handling, so paths are resolved after
	  reading from config files. Hopefully it doesn't introduce new
	  bugs. CCBUG: 111049

2006-09-01 20:37 +0000 [r579862]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/filestreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop: Regression fix
	  in the files view due to the symlink handling change.

2006-09-01 21:38 +0000 [r579876]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Save As asked for
	  confirmation of "save before close" for modified files due to the
	  setEncoding() call. This was confusing and could result in data
	  loss as well. For now, remove the possibility to change the
	  encoding in the save dialog (users should change the encoding and
	  save later or save and change later). Possiby fixes #131728 (I
	  cannot reproduce it with this version). BUG: 131728

2006-09-01 21:47 +0000 [r579881]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/data/chars: Add some more
	  special characters to the list. BUGS: 130513, 124628

2006-09-01 22:01 +0000 [r579883]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/csseditor/cssselector.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Not setting a
	  pointer to 0 after deletion is asking for trouble. Fix crash in
	  CSS editor. BUG: 131849

2006-09-01 23:04 +0000 [r579903]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't crash when
	  closing a document in VPL mode. Should fix the following bugs:
	  BUGS: 133082, 126585, 125153

2006-09-02 07:32 +0000 [r579962]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/lib/qextfileinfo.h: Commit the missing
	  changes from lib...

2006-09-02 09:09 +0000 [r579974]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/filestreeview.cpp:
	  Avoid duplicatet entries for top folders. Can happen if the user
	  user both the old and current quanta in parallel.

2006-09-02 09:22 +0000 [r579975]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix crash when
	  copying to clipboard inside VPL. BUG: 130212

2006-09-02 09:46 +0000 [r579980]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Show the correct
	  column number if tabs are used in the document BUG: 133313

2006-09-02 10:29 +0000 [r579990]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectupload.h: Only
	  one upload dialog can be shown at any time... BUG: 132535

2006-09-02 10:43 +0000 [r579993]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Do not show Find in
	  Files menu if KFileReplace is not installed. BUG: 132530

2006-09-02 12:41 +0000 [r580040]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Should fix a crash
	  when using close all I saw.

2006-09-02 14:05 +0000 [r580055]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/utility/qpevents.cpp: - allow
	  logging to files outside of project directory - don't send
	  closing events for untitled, unmodified documents BUG: 131782

2006-09-04 09:04 +0000 [r580691]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/tagdialogs/tagwidget.cpp:
	  Backport fix CID 2676, 2677, 2678 (memory leak).

2006-09-04 09:31 +0000 [r580696]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp: Backport some
	  CID fixes.

2006-09-04 15:12 +0000 [r580821]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/utility/quantacommon.cpp:
	  Resolve symlinks only for local files and keep the url notation.

2006-09-05 13:27 +0000 [r581148]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditor.ui,
	  branches/KDE/3.5/kdewebdev/kommander/editor/mainwindow.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditorimpl.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/Makefile.am,
	  branches/KDE/3.5/kdewebdev/kommander/editor/connectioneditorimpl.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/factory/kommanderversion.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditorimpl.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/kommander.xml
	  (added),
	  branches/KDE/3.5/kdewebdev/kommander/editor/mainwindow.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/mainwindowactions.cpp:
	  Backport usage of KTextEditor instead of plain QTextEdit for
	  Kommander Editor. The feature was developed and tested in a
	  separate branch. It introduces 2 new strings, but those are
	  already in kdelibs, so hopefuly it doesn't matter for
	  translators. Fix one menu entry capitalization.

2006-09-06 12:29 +0000 [r581451]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop: Backport:
	  remove dead code (CID 2627).

2006-09-06 12:52 +0000 [r581462]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp: Backport
	  safety null checks (CID 2643).

2006-09-07 16:39 +0000 [r581821]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/editor/kommander.xml: Fix
	  highlighting.

2006-09-07 17:03 +0000 [r581829]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/factory/kommanderfactory.cpp:
	  Fix crash on exit in the editor. Do not use static objects in
	  libraries. CCMAIL: kommander-devel@mail.kdewebdev.org

2006-09-07 17:11 +0000 [r581834]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/factory/kommanderfactory.cpp:
	  Oops...

2006-09-07 17:18 +0000 [r581836]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/docfolder.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Respect the order of
	  items in the .docrc. Patch from the bugreport. BUG: 133704

2006-09-07 20:42 +0000 [r581882]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix crash when
	  creating project through slow links.

2006-09-07 21:02 +0000 [r581888]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Really abort if a
	  remote directory cannot be created. BUG: 117032

2006-09-07 21:23 +0000 [r581901]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/factory/kommanderfactory.cpp:
	  Don't crash in executor.

2006-09-08 12:49 +0000 [r582161]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/docfolder.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp: Complete the
	  fix for #133704 as requested (plus some cleanup). BUG: 133704

2006-09-08 13:21 +0000 [r582175]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix automatic
	  updating of closing tags (don't search for the closing tag for
	  single tags). BUG: 132357

2006-09-08 14:37 +0000 [r582192]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/csseditor/specialsb.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/csseditor/specialsb.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Accept float numbers
	  for length values (use lineedit instead of spinbox). BUG: 130295

2006-09-09 10:21 +0000 [r582414]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Make CSS completion
	  work inside style attributes. BUG: 80605

2006-09-09 10:58 +0000 [r582429]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/dirtydlg.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Improve usability of
	  the File Changed dialog. BUG: 126058 CCMAIL: kde-i18n-doc@kde.org

2006-09-09 14:29 +0000 [r582484]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/utility/quantacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Handle correctly the
	  escaped backslashes inside strings. BUG: 128819

2006-09-09 17:10 +0000 [r582528]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/dtds.h,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/docbook-4.2/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/kde-docbook-4.1.2/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/kde-docbook-4.2/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/php/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xslt/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/wml-1-2/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/tagxml/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/empty/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml11/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml-frameset/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/schema/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml-strict/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/html/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/html-frameset/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/html-strict/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml-basic/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/src/dtds.cpp: Improve mimetype
	  and extension based searching for a DTEP that can handle the
	  currently opened file. Related to bug #129808.

2006-09-09 18:02 +0000 [r582538]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/wkafkapart.cpp: I
	  got a crash here...

2006-09-09 19:08 +0000 [r582556]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantaview.h: Regression
	  fix (switching between documents was broken in VPL mode). Caused
	  by part of the commit in rev. 580040. + some code cleanup.

2006-09-13 21:35 +0000 [r583978]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/ChangeLog,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/tests/data/entities/bull&bladder.jpg,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/documentrootdialog.cpp
	  (added), branches/KDE/3.5/kdewebdev/klinkstatus/src/tests
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configidentificationdialog.h
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidgetbase.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/tests/data,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultview.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager_impl.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus_impl.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/url.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/tests/data/entities/link_with_html_entities.html,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/cfg/klinkstatus.kcfg,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configidentificationdialogui.ui
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/tests/data/entities/bull_bladder.jpg,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus.kdevelop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configidentificationdialog.cpp
	  (added),
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/htmlparser.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configresultsdialog.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configsearchdialog.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/documentrootdialog.h
	  (added), branches/KDE/3.5/kdewebdev/klinkstatus/src/main.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/tabwidgetsession.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/url.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/Makefile.am,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/tests/data/entities:
	  svn merge -r 472392:532677 branches/work/klinkstatus_proceed

2006-09-16 15:09 +0000 [r585214]  bram

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configresultsdialog.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configsearchdialog.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/settings/configidentificationdialogui.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidgetbase.ui:
	  fixuifiles

2006-09-20 08:23 +0000 [r586656-586655]  amantia

	* branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdewebdev/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/VERSION,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/VERSION: Increase version
	  numbers.

	* branches/KDE/3.5/kdewebdev/kdewebdev.lsm: Increase version
	  numbers.

2006-09-21 22:46 +0000 [r587218]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/http.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/htmlparser.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/cfg/klinkstatus.kcfg,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node_impl.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidgetbase.ui,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/http.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/htmlparser.cpp:
	  BUG: 134331 - respect the charset of the documents. Look at the
	  server response and, if not specified, try to look in the meta
	  tag, like defined by the spec. - remove some evil processEvents()
	  calls - don't ignore the current session properties in favor of
	  the default ones when starting a check - add unlimited recursion
	  by default

2006-09-21 23:01 +0000 [r587221]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp:
	  Safer

2006-09-23 15:39 +0000 [r587660]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp:
	  BUG: 134329 Accept fragment identifiers like "#" and "#top".

2006-09-24 18:59 +0000 [r588052]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/klinkstatus.kdevelop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkstatus_impl.h:
	  BUG: 134373 - Each link now has an associated status:
	  UNDETERMINED, SUCCESSFULL, BROKEN, HTTP_REDIRECTION,
	  HTTP_CLIENT_ERROR, HTTP_SERVER_ERROR, TIMEOUT, NOT_SUPPORTED,
	  MALFORMED that is clearly set after it being checked, which
	  didn't happen before. This fixes bug #134373 which was caused by
	  a typo btw. Also fix some failed assertions; there were some code
	  paths in LinkChecker that weren't properly managed.

2006-09-24 22:15 +0000 [r588087]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/actionmanager.cpp: Fix
	  icon

2006-09-26 18:50 +0000 [r588744]  amantia

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/Makefile.am: Fix
	  build.
