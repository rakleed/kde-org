2007-01-24 13:56 +0000 [r626734]  mueller

	* branches/KDE/3.5/kdeaccessibility/kmag/kmagselrect.h: don't
	  define a min function

2007-03-12 18:36 +0000 [r641842]  mueller

	* branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/pluginconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kmousetool/kmousetool/mtstroke.h:
	  the usual daily unbreak compilation

2007-04-05 13:05 +0000 [r650803]  scripty

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/tool_brush_selection.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/tool_polygonal_selection.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/systemtray.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/superimpose.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/tool_picker_selection.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/redeyes.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/tool_wizard_selection.svgz,
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/tool_eraser_selection.svgz:
	  Remove svn:executable (as it is a graphic file) (goutte)

2007-04-12 21:43 +0000 [r653216]  scripty

	* branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayit.desktop: A
	  .desktop file should not be executable (goutte)

2007-04-13 11:15 +0000 [r653454]  scripty

	* branches/KDE/3.5/kdeaccessibility/ksayit/KTTSD_Lib/kttsdlib.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayitsystemtray.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/KTTSD_Lib/kttsdlibsetupimpl.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayit_ttsplugin.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/voicesetupdlg.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/fxsetupimpl.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayit_fxplugin.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayitviewimpl.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/docbookgenerator.cpp,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/version.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/docbookgenerator.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/Freeverb_plugin/freeverbsetupimpl.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/fxpluginhandler.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/Freeverb_plugin/ksayitfreeverblib.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/Types.h,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayit.h: C++ files
	  should not be executable (goutte)

2007-04-16 16:32 +0000 [r654616-654615]  dannya

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/kftpgrabber.svgz
	  (added): sync local icons with repo

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rsibreak2.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/scheme_advanced_powersave.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rsibreak3.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rsibreak4.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rsibreakx.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/suspend_to_disk.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/podcast.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/scheme_acoustic.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/scheme_presentation.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/scheme_powersave.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/stand_by.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/podcast_new.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/timings.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/lan.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/laptoppower.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/processor.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/scheme_power.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/duringbreaks.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rsibreak0.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/suspend_to_ram.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rsibreak1.svgz
	  (added): sync local icons with repo

2007-04-16 16:36 +0000 [r654621-654619]  dannya

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/kig_numbers.svgz
	  (added): sync local icons with repo

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/kig_numbers.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/kig_numbers.svgz
	  (removed): sync local icons with repo

2007-04-16 18:31 +0000 [r654665]  dannya

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/svn_merge.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/svn_switch.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/power-manager.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/webcamsend.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/apport.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/svn_status.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/wineconfig.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/svn_add.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/hibernate.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/suspend.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/webcamreceive.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/rss_tag.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/apps/komparator.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/svn_branch.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/svn_remove.svgz
	  (added): filling in iconset gaps

2007-04-16 18:47 +0000 [r654671]  dannya

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_commit.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/kopeteeditstatusmessage.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_sourceseditor.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_distupgrade.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_remove.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_purge.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_update.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_install.svgz
	  (added): filling in iconset gaps

2007-04-17 12:29 +0000 [r654959]  dannya

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/show_offliners.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/adept_notifier_warning.svgz
	  (added),
	  branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/actions/voicecall.svgz
	  (added): filling in iconset gaps

2007-04-17 12:43 +0000 [r654967]  dannya

	* branches/KDE/3.5/kdeaccessibility/IconThemes/mono/scalable/mimetypes/torrent.svgz
	  (added): filling in iconset gaps

2007-04-21 19:59 +0000 [r656596]  scripty

	* branches/KDE/3.5/kdeaccessibility/ksayit/Freeverb_plugin/freeverb_setup.ui,
	  branches/KDE/3.5/kdeaccessibility/ksayit/KTTSD_Lib/KTTSDlibSetup.ui,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayitui.rc,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/fx_setup.ui,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/KSayItView.ui: .ui
	  and .rc files should not be executable (goutte)

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

