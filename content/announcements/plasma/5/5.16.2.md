---
aliases:
- ../../plasma-5.16.2
changelog: 5.16.1-5.16.2
date: 2019-06-25
layout: plasma
youtube: T-29hJUxoFQ
figure:
  src: /announcements/plasma/5/5.16.0/plasma-5.16.png
  class: text-center mt-4
asBugfix: true
---

- Klipper: Always restore the last clipboard item. <a href="https://commits.kde.org/plasma-workspace/3bd6ac34ed74e3b86bfb6b29818b726baf505f20">Commit.</a>
- Discover: Improved notification identification for Snap applications. <a href="https://commits.kde.org/plasma-workspace/951551bc6d1ab61e4d06fe48830459f23879097c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21881">D21881</a>
- Notifications: Don't keep non-configurable notifications in history. <a href="https://commits.kde.org/plasma-workspace/1f6050b1740cf800cb031e98fd89bc00ca20c55a">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D22048">D22048</a>
